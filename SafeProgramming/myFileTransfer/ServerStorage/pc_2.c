#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>

#define NUM 3	//临界区
int queue[NUM];
sem_t blank_number, product_number;

void *producer ( void * arg )		//生产者
{
	static int p = 0;

	for ( ;; ) {
		sem_wait( &blank_number );		//信号量等待
		queue[p] = rand() % 1000;		//产生随机数，作为生产者生产的产品
		printf("Product %d \n", queue[p]);		//打印出产品数目
		p = (p+1) % NUM;	
		sleep ( rand() % 3);		//线程挂起
		sem_post( &product_number );	//信号量释放
	}
}
void *consumer ( void * arg )		//消费者
{

	static int c = 0;
	for( ;; ) {
		sem_wait( &product_number );		//信号量等待
		printf("Consume %d\n", queue[c]);	//消费者消费临界区内的产品
		c = (c+1) % NUM;	
		sleep( rand() % 3 );			//线程挂起
		sem_post( &blank_number );		//信号量释放
	}
}

int main(int argc, char *argv[] )
{
	pthread_t pid, cid_1,cid_2,cid_3,cid_4;
    
	sem_init( &blank_number, 0, NUM );		//blank_number信号量初始化
	sem_init( &product_number, 0, 0);		//product_number信号量初始化
	pthread_create( &pid, NULL, producer, NULL);	//创建线程生产者
    

	pthread_create( &cid_1, NULL, consumer, NULL);	//创建线程消费者
    pthread_create( &cid_2, NULL, consumer, NULL);	//创建线程消费者
    pthread_create( &cid_3, NULL, producer, NULL);	//创建线程生产者
    pthread_create( &cid_4, NULL, producer, NULL);	//创建线程生产者

    pthread_join( pid, NULL );				
	pthread_join( cid_1, NULL );
    pthread_join( cid_2, NULL );
    pthread_join( cid_3, NULL );
    pthread_join( cid_4, NULL );
	sem_destroy( &blank_number );		//释放信号量blank_number
	sem_destroy( &product_number );		//释放信号量product_number
	return 0;
}
