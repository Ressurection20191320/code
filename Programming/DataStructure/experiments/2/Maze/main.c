#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXSIZE 100  //迷宫最大长宽
#define MAXSTACK 100 //栈最大容量

typedef struct Offsets
{
    int v;
    int h;
} offsets; //定义每次移动的下一步偏移量结构体变量

typedef struct MazePosition
{
    int R;
    int C;
    int Dir;
} MazePosision; //定义每次走动的位置和对应偏移量

typedef MazePosision ElementType;//迷宫位置对应点作为栈的元素
typedef int Position;
struct SNode
{
    ElementType *Data;
    Position Top;
    int MaxSize;
};
typedef struct SNode *PtrToSNode;
typedef PtrToSNode Stack;//定义好栈的结构体变量

Stack CreateStack(int MaxSize)//创建Maxsize大小的栈
{
    Stack S = (Stack)malloc(sizeof(struct SNode));
    S->Data = (ElementType *)malloc(MaxSize * sizeof(ElementType));
    S->Top = -1;
    S->MaxSize = MaxSize;
    return S;
}

int Push(Stack S, ElementType x)//压栈函数
{
    if (S->Top == S->MaxSize - 1)
    {
        printf("Stack is Full!!\n");
        return 0;
    }
    else
    {
        S->Data[++(S->Top)] = x;
        return 1;
    }
}

ElementType Pop(Stack S)//出栈函数
{
    if (S->Top == -1)
        printf("Stack is Empty!!\n");
    else
        return (S->Data[(S->Top)--]);
}

void Path(int Maze[][MAXSIZE], int ExitR, int ExitC, int size) //迷宫主要函数，默认迷宫入口(1,1)
{
    offsets Move[4] = {{0, 1}, {1, 0}, {-1, 0}, {0, -1}};//定义上下左右四个偏移量
    int Mark[MAXSIZE][MAXSIZE];//Mark数组存放已经走过的迷宫格，若已走过则置为1
    Stack S;
    MazePosision P;//当前迷宫中走到的位置，包含位置，下一步应走的方向
    int R, C, NextR, NextC, Dir;
    int Found = 0;//找到才置为1

    S = CreateStack(MAXSIZE);

    Mark[1][1] = 1;//第一步已走
    P.R = 1;
    P.C = 1;
    P.Dir = 0;//初始化第一步尝试
    Push(S, P);

    while (S->Top != -1 && !Found)//主要迷宫循环,若栈空了，那就是没有解
    {
        P = Pop(S);
        R = P.R;
        C = P.C;
        Dir = P.Dir;
        while (Dir < 4 && !Found)//若当前位置4个方向都走完，没有找到可走的下一步，就弹栈，回退上一步继续找出口
        {
            NextR = R + Move[Dir].v;
            NextC = C + Move[Dir].h;//找到下一次要尝试的格子
            if (NextR == ExitR && NextC == ExitC && Maze[ExitR][ExitC] == 0)
                Found = 1;//找到出口
            else if (!Maze[NextR][NextC] && !Mark[NextR][NextC])//如果下一个尝试的格子没有走过，且不是墙，就继续走
            {
                Mark[NextR][NextC] = 1;//将将要走的这一格标记为走过
                P.R = R;
                P.C = C;//设置好当前位置
                P.Dir = Dir + 1;//此方向已走过，方向为若失败后下次应尝试的方向
                Push(S, P);//将当前位置和方向压入栈中
                R = NextR;
                C = NextC;
                Dir = 0;//初始化将走下一格的路
            }
            else//若不可走但4个方向未尝试完，尝试下一方向
                Dir++;
        }
    }
    if (Found)//若找到，则输出
    {
        Maze[NextR][NextC] = 8;
        Maze[R][C] = 8;//用8标志走过的路径
        while (!(S->Top == -1))
        {
            P = Pop(S);
            Maze[P.R][P.C] = 8;
        }
        printf("Found solve below(the route is marked by \"X\"):\n");
        for (int i1 = 1; i1 <= size; i1++)
        {
            for (int i2 = 1; i2 <= size; i2++)
            {
                if (Maze[i1][i2] == 8)
                    printf("X ");
                else
                    printf("%d ", Maze[i1][i2]);
            }
            printf("\n");
        }//输出以X标记行走路径的迷宫
    }
    else
        printf("Can't solve.\n");
}

int main()
{
    int size;
    int Maze[MAXSIZE][MAXSIZE];
    int ExitR, ExitC;
    printf("Please input the size of the maze:");
    scanf("%d", &size);
    for (int i = 0; i < size + 2; i++)
        Maze[i][0] = Maze[0][i] = Maze[size + 1][i] = Maze[i][size + 1] = 1; //给迷宫周围加一圈以'1'围成的墙
    printf("Please input the maze(default entrance is (1,1)):\n");
    for (int i1 = 1; i1 < size + 1; i1++)
        for (int i2 = 1; i2 < size + 1; i2++)
            scanf("%d", &Maze[i1][i2]);
    printf("Please input the Exit if the Maze \"R C\":");
    scanf("%d%d", &ExitR, &ExitC);
    Path(Maze, ExitR, ExitC, size);
    return 0;
}