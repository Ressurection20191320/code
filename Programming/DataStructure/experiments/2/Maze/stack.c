typedef int Position;
typedef struct SNode *PtrToSNode;
struct SNode{
    ElementType *Data;
    Position Top;
    int MaxSize;
};
typedef PtrToSNode Stack;

Stack CreateStack(int MaxSize)
{
    Stack S=(Stack)malloc(sizeof(struct SNode));
    S->Data=(ElementType*)malloc(MaxSize*sizeof(ElementType));
    S->Top=-1;
    S->MaxSize=MaxSize;
    return S;
}

int Push(Stack S,ElementType x)
{
    if(S->Top==S->MaxSize-1){
        printf("Stack is Full!!\n");
        return 0;
    }
    else{
        S->Data[++(S->Top)]=x;
        return 1;
    }
}

ElementType Pop(Stack S)
{
    if(S->Top==-1){
        printf("Stack is Empty!!\n");
        return 0;
    }
    else
        return(S->Data[(S->Top)--]);
}