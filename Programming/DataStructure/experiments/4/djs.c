#include<stdio.h>
#include<stdlib.h>
typedef struct node{
    int data;
    struct node *next;
}linklist;

linklist* createlist(linklist *L,int n){//头插法创建链表
    linklist *s,*l;
    int t;
    L=(linklist *)malloc(sizeof(linklist));//创建头结点
    L->next=NULL;//将头节点next置空
    l=L;
    for(int i=0;i<n;i++){
        s=(linklist*)malloc(sizeof(linklist));
        scanf("%d",&t);
        s->data=t;
        s->next=l->next;
        l->next=s;
    }
    return l;
}
void insertsort(linklist *L)
{
    linklist *p,*q,*pre;
    p = L->next->next;
    L->next->next = NULL;
    while(p)
    {
        q = p->next;
        pre = L;
        while(pre->next != NULL && pre->next->data < p->data)
            pre = pre->next;
        p->next = pre->next;
        pre->next = p;
        p = q;
    }
}
void putlist(linklist *l){
    linklist *p=l->next;
    while(p!=NULL){
        printf(" %d",p->data);
        p=p->next;
    }
}
int main(){
    int n;
    linklist *l;
    scanf("%d",&n);
    l=createlist(l,n);
    insertsort(l);
    putlist(l);
    return 0;
}