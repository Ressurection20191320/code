#include <stdio.h>
#include <stdlib.h>

typedef int KeyType;

typedef struct RecordNode
{
    KeyType key; //存储要排序的值
    int seq;     //存储原来输入时的序号
    struct RecordNode *next;
} RecordNode, *PNode, *Linklist; //定义结构中每个节点的结构体类型

typedef struct List
{
    Linklist elements;
    int len;
} * List; //保存存储待排序元素和长度的结构体变量

void insertion_sort(List list) //插入排序的实现
{
    Linklist head = list->elements;
    PNode p = head->next->next; //当前进行操作被插入的元素
    PNode tmp, pre, r;
    head->next->next = NULL; //将原表初始化进行插入，就在原表上进行操作，降低空间复杂度
    while (p != NULL)
    {
        pre = head;
        r = head->next;
        while (r != NULL && p->key >= r->key)
        {
            pre = pre->next;
            r = r->next;
        } //同时移动指针，直到找到要插入的位置再停止
        tmp = p;
        p = p->next;
        pre->next = tmp;
        tmp->next = r; //进行插入操作
    }
}

List create_list()
{
    List list;
    list = (List)malloc(sizeof(struct List));
    list->elements = (Linklist)malloc(sizeof(struct RecordNode));
    PNode p, q;
    printf("请输入需要对多少数进行排序：");
    scanf("%d", &list->len);
    while (list->len < 1)
    {
        printf("输入错误，重新输入:");
        scanf("%d", &list->len);
    }
    q = list->elements;
    printf("请开始输入元素：");
    for (int i = 0; i < list->len; i++)
    {
        p = (PNode)malloc(sizeof(struct RecordNode));
        p->seq = i + 1;
        p->next = NULL;
        scanf("%d", &p->key);
        q->next = p;
        q = q->next;
    }
    return list; //通过使用返回值的形式建表
}

void output(List list)
{
    PNode p = list->elements->next;
    printf("以下以从小到大，上排为值，下排为序号的方式输出：\n值：  ");
    while (p != NULL)
    {
        printf("\t%d", p->key);
        p = p->next;
    }
    printf("\n序号：");
    p = list->elements->next;
    while (p != NULL)
    {
        printf("\t%d", p->seq);
        p = p->next;
    } //输出表中元素和对应序号
}

int main()
{
    List list = create_list();
    insertion_sort(list);
    output(list);
    return 0;
}