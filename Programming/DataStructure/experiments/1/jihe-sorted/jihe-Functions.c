typedef int DataType;

typedef struct Node
{
	DataType info;
	struct Node *link;
} * PNode, *LinkList;

int flag = -1; //定义输入结束标志flag，即输入的值为-1时，就不再输入了

LinkList CreatNullList_link(void);
int IsNullLink_link(LinkList llist);
PNode Locate_link(LinkList llist, DataType x);
int InsertPost_link(LinkList llist, PNode p, DataType x);
PNode LocatePre_link(LinkList llist, PNode p);
LinkList creatLinkList_R();
void printlist(LinkList llist);
int AddToList(LinkList llist, DataType x);
void QuickSort(PNode Head,PNode Tail);
void QS(PNode p);

LinkList CreatNullList_link(void)
{
	LinkList llist;
	llist = (LinkList)malloc(sizeof(struct Node));
	llist->link = NULL;
}

int IsNullLink_link(LinkList llist)
{
	return (llist->link == NULL);
}

PNode Locate_link(LinkList llist, DataType x)
{
	PNode p;
	p = llist->link;
	while (p->info != x && p->link != NULL)
		p = p->link;
	if (p->link == NULL)
	{
		printf("x not exist!!\n");
		return NULL;
	}
	else
		return p;
}

int InsertPost_link(LinkList llist, PNode p, DataType x)
{
	PNode q = (PNode)malloc(sizeof(struct Node));
	if (q == NULL)
	{
		printf("Out of space!!\n");
		return 0;
	}
	else
	{
		q->info = x;
		q->link = p->link;
		p->link = q;
	}
	return 1;
}

PNode LocatePre_link(LinkList llist, PNode p)
{
	PNode q = llist;
	while (q->link != p && q->link != NULL)
		q = q->link;
	if (q->link == NULL)
	{
		printf("p not exist!\n");
		return NULL;
	}
	else
	{
		return q;
	}
}

LinkList creatLinkList_R()
{
	LinkList L;
	PNode r, p;
	DataType x;
	L = (LinkList)malloc(sizeof(struct Node)); //头结点
	L->link = NULL;
	r = L;
	scanf("%d", &x);
	while (x != flag) //建立链表,flag结束标志
	{
		p = (PNode)malloc(sizeof(struct Node));
		p->info = x;
		p->link = r->link;
		r->link = p;
		r = p;
		scanf("%d", &x);
	}
	return L;
}

void printlist(LinkList llist) //按照顺序将链表中所有元素输出
{
	if (llist->link != NULL)
	{
		PNode p = llist->link;
		while (p != NULL)
		{
			printf("%d ", p->info);
			p = p->link;
		}
		printf("\n");
	}
	else
		printf("空集！\n");
}

int AddToList(LinkList llist, DataType x) //将数据x插入链表中去
{
	PNode node = (PNode)malloc(sizeof(struct Node));
	if (node == NULL)
		return 0;
	node->info = x;
	node->link = llist->link;
	llist->link = node;
	return 1;
}

void QuickSort(PNode Head,PNode Tail)//Head为需要排序的段前一个节点指针，Tail为需排序段后一个节点指针
{
	if(Head->link!=Tail&&Head->link->link!=Tail)
	{
		PNode KeyP=Head->link;      //指向存key值内存单元的指针
		PNode LP=Head;              //比key值小的游标指针,KeyP的左边
		PNode RP=KeyP;              //比key值大的游标指针,KeyP的右边
		PNode IncP=KeyP->link;      //遍历单链表的游标指针,每次移动一位
		while(IncP!=Tail)
		{
			if(IncP->info<KeyP->info)
				LP=LP->link=IncP;
			else
				RP=RP->link=IncP;
			IncP=IncP->link;
		}
		LP->link=KeyP;    //将小于Key值的子链表衔接
		RP->link=Tail;    //将大于Key值的子链表衔接
		//递归
		QuickSort(Head,KeyP); //比Key小的子链表排序
		QuickSort(KeyP,Tail); //比Key大的子链表排序
		//注Head和Tail是不参与排序的，只起表头和表尾标识作用,故原链表的Tail值为NULL
	}
}

void QS(PNode p)
{
	QuickSort(p,NULL);
}