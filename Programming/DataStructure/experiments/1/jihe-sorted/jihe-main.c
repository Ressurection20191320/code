#include <stdio.h>
#include <stdlib.h>
#include "jihe-Functions.c" //导入链表的基本操作

void menu();
LinkList set_union(LinkList la, LinkList lb);
LinkList set_Intersection(LinkList la, LinkList lb);
LinkList set_Difference(LinkList la, LinkList lb);
void AddDiffrernce(LinkList la, LinkList lb, LinkList lc);

int main()
{
    LinkList la, lb;
    menu(); //输出程序提示内容
    printf("请输入A中元素，正整数，以-1结尾：\n");
    la = creatLinkList_R(); //按头插法建立集合A，后面也相同。
    printf("请输入B中元素，正整数，以-1结尾：\n");
    lb = creatLinkList_R();
    printf("从小到大排序后，A集合的元素是：\n");
    QS(la);//将存储集合的链表进行排序
    printlist(la);
    printf("从小到大排序后，B集合的元素是：\n");
    QS(lb);
    printlist(lb);
    printf("A并B，结果为：\n");
    LinkList l1 = set_union(la, lb); //分别将交，并，差的结果存入l1,l2,l3中，并输出。
    printlist(l1);
    printf("A交B，结果为：\n");
    LinkList l2 = set_Intersection(la, lb);
    printlist(l2);
    printf("A差B，结果为：\n");
    LinkList l3 = set_Difference(la, lb);
    printlist(l3);
    return 0;
}

void menu() //提示列表
{
    printf("单链表集合运算(快排)-已经实现内容：\n");
    printf("\t\t集合数据输入\n");
    printf("\t\t集合数据显示\n");
    printf("\t\t集合A和集合B的并集\n");
    printf("\t\t集合A和集合B的交集\n");
    printf("\t\t集合A和集合B的差集\n");
}

LinkList set_union(LinkList la, LinkList lb) //实现集合的并
{
    LinkList lc = CreatNullList_link(); //创建lc存储结果
    PNode p = la->link, q = lb->link, r;
    while (p != NULL)
    {
        AddToList(lc, p->info);
        p = p->link;
    }//先把la中的所有元素放入lc中
    QS(lc);
    r = lc->link;
    while (q != NULL)
    {
        while (r != NULL && r->info < q->info)
            r = r->link;                        //将lc的指针向右移动，直到所指元素大于等于当前q所指元素
        if (r == NULL || q->info != r->info)
            AddToList(lc, q->info);             //若无重复，才加入lc中
        q = q->link;
    }
    QS(lc);
    return lc;
}

LinkList set_Intersection(LinkList la, LinkList lb) //实现集合的交
{
    LinkList lc = CreatNullList_link();
    PNode p = la->link, q = lb->link;
    while (p != NULL && q != NULL)
    {
        while (q != NULL && q->info < p->info)
            q = q->link;//一直移动q的指针，直到q大于或等于p
        if (q != NULL && q->info == p->info)
            AddToList(lc, p->info);//若有相等，加入结果链表
        p = p->link;//移动p的指针
    }
    QS(lc);
    return lc;
}

LinkList set_Difference(LinkList la, LinkList lb)//调用以上两个函数，实现集合的差运算
{
    LinkList l1 = set_union(la, lb);
    LinkList l2 = set_Intersection(la, lb);
    LinkList lc = CreatNullList_link();
    PNode p = l1->link, q = l2->link;
    while (p != NULL)
    {
        if (q == NULL)
        {
            while (p != NULL)
            {
                AddToList(lc, p->info);
                p = p->link;
            }
        }
        else
        {
            if (p->info < q->info)
            {
                AddToList(lc, p->info);
                p = p->link;
            }
            if (q != NULL && q->info == p->info)
            {
                p = p->link;
                q = q->link;
            }
        }
    }
    QS(lc);
    return lc;
}