#include <stdio.h>
#include <stdlib.h>
#include "jihe-Functions.c" //导入链表的基本操作

void menu();
LinkList set_union(LinkList la, LinkList lb);
LinkList set_Intersection(LinkList la, LinkList lb);
LinkList set_Difference(LinkList la, LinkList lb);
void AddDiffrernce(LinkList la, LinkList lb, LinkList lc);

int main()
{
    LinkList la, lb;
    menu(); //输出程序提示内容
    printf("请输入A中元素，int形，以-1结尾：\n");
    la = creatLinkList_R(); //按头插法建立集合A，后面也相同。
    printf("请输入B中元素，int形，以-1结尾：\n");
    lb = creatLinkList_R();
    printf("A集合的元素是：\n");
    printlist(la);
    printf("B集合的元素是：\n");
    printlist(lb);
    printf("A并B，结果为：\n");
    LinkList l1 = set_union(la, lb); //分别将交，并，差的结果存入l1,l2,l3中，并输出。
    printlist(l1);
    printf("A交B，结果为：\n");
    LinkList l2 = set_Intersection(la, lb);
    printlist(l2);
    printf("A差B，结果为：\n");
    LinkList l3 = set_Difference(la, lb);
    printlist(l3);
    return 0;
}

void menu() //提示列表
{
    printf("单链表集合运算-已经实现内容：\n");
    printf("\t\t集合数据输入\n");
    printf("\t\t集合数据显示\n");
    printf("\t\t集合A和集合B的并集\n");
    printf("\t\t集合A和集合B的交集\n");
    printf("\t\t集合A和集合B的差集\n");
}

LinkList set_union(LinkList la, LinkList lb) //实现集合的并
{
    LinkList lc = CreatNullList_link(); //创建lc存储结果
    PNode p = la, q = lb, r;
    int flag;
    while (p->link != NULL)
    {
        p = p->link;
        AddToList(lc, p->info);
    } //将la中所有元素复制到lc中
    while (q->link != NULL)
    {
        q = q->link;
        flag = 1; //flag的值为1，代表q中没有r中现有的元素
        r = lc -> link;
        while (r != NULL)
        {
            if (r->info == q->info)
            {
                flag = 0;
                break;
            }
            r = r->link;
        } //对于每个lc中的元素，检查lb中是否已存在，若有，就把flag置为1，不再加入lc表中。
        if (flag)
            AddToList(lc, q->info);
    }
    return lc;
}

LinkList set_Intersection(LinkList la, LinkList lb) //实现集合的交
{
    LinkList lc = CreatNullList_link();
    PNode q = la, p;
    int flag;
    while (q->link != NULL)
    {
        q = q->link;
        p=lb;
        while (p->link != NULL)
        {
            p = p->link;
            if (p->info == q->info)
            {
                AddToList(lc, q->info);
                break;
            } //对于每一个lb中的元素，遍历la中所有元素检查是否有相同元素，若有才加入lc。
        }
    }
    return lc;
}

void AddDiffrernce(LinkList la, LinkList lb, LinkList lc) //此函数将la中有，但lb中没有的元素加入到lc中，被实现差运算的函数调用。
{
    PNode p = la, q;
    int flag = 1;
    while (p->link != NULL)
    {
        p = p->link;
        q = lb;
        while (q->link != NULL)                           //对于每个p，检查是否有lb中与之相同的元素，若有则把flag置为0，不再加入链表。
        {
            q = q->link;
            flag = 1;
            if (p->info == q->info)
            {
                flag = 0;
                break;
            }
        }
        if (flag)
        {
            AddToList(lc, p->info);                     //将la中有，但lb中没有的元素加入lc表中。
        }
    }
}

LinkList set_Difference(LinkList la, LinkList lb)
{
    LinkList lc = CreatNullList_link();
    AddDiffrernce(la, lb, lc);
    AddDiffrernce(lb, la, lc);                           //完成这两步后，lc中元素为la，lb中不共有的元素，即完成了差运算。
    return lc;
}

// LinkList set_Difference(LinkList la, LinkList lb)
// {
//     LinkList lc = CreatNullList_link();
//     PNode p = la, q = lb;
//     int flag;
//     while (q->link != NULL)
//     {
//         q = q->link;
//         flag = 1;
//         p = la ;
//         while (p->link != NULL)
//         {
//             p = p->link;
//             if (p->info == q->info)
//             {
//                 flag = 0;
//                 break;
//             }
//         }
//         if (flag)
//             AddToList(lc, q->info);
//     }
//     p = lb, q = la;
//     while (q->link != NULL)
//     {
//         q = q->link;
//         flag = 1;
//         p = lb ;
//         while (p->link != NULL)
//         {
//             p = p->link;
//             if (p->info == q->info)
//             {
//                 flag = 0;
//                 break;
//             }
//         }
//         if (flag)
//             AddToList(lc, q->info);
//     }
//     return lc;
// }