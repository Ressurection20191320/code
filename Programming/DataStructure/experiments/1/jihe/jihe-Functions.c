typedef int DataType;

typedef struct Node
{
	DataType info;
	struct Node *link;
} * PNode, *LinkList;

int flag = -1; //定义输入结束标志flag，即输入的值为-1时，就不再输入了

LinkList CreatNullList_link(void);
int IsNullLink_link(LinkList llist);
PNode Locate_link(LinkList llist, DataType x);
int InsertPost_link(LinkList llist, PNode p, DataType x);
PNode LocatePre_link(LinkList llist, PNode p);
LinkList creatLinkList_R();
void printlist(LinkList llist);
int AddToList(LinkList llist, DataType x);

LinkList CreatNullList_link(void)
{
	LinkList llist;
	llist = (LinkList)malloc(sizeof(struct Node));
	llist->link = NULL;
}

int IsNullLink_link(LinkList llist)
{
	return (llist->link == NULL);
}

PNode Locate_link(LinkList llist, DataType x)
{
	PNode p;
	p = llist->link;
	while (p->info != x && p->link != NULL)
		p = p->link;
	if (p->link == NULL)
	{
		printf("x not exist!!\n");
		return NULL;
	}
	else
		return p;
}

int InsertPost_link(LinkList llist, PNode p, DataType x)
{
	PNode q = (PNode)malloc(sizeof(struct Node));
	if (q == NULL)
	{
		printf("Out of space!!\n");
		return 0;
	}
	else
	{
		q->info = x;
		q->link = p->link;
		p->link = q;
	}
	return 1;
}

PNode LocatePre_link(LinkList llist, PNode p)
{
	PNode q = llist;
	while (q->link != p && q->link != NULL)
		q = q->link;
	if (q->link == NULL)
	{
		printf("p not exist!\n");
		return NULL;
	}
	else
	{
		return q;
	}
}

LinkList creatLinkList_R()
{
	LinkList L;
	PNode r, p;
	DataType x;
	L = (LinkList)malloc(sizeof(struct Node)); //头结点
	L->link = NULL;
	r = L;
	scanf("%d", &x);
	while (x != flag) //建立链表,flag结束标志
	{
		p = (PNode)malloc(sizeof(struct Node));
		p->info = x;
		p->link = r->link;
		r->link = p;
		r = p;
		scanf("%d", &x);
	}
	return L;
}

void printlist(LinkList llist) //按照顺序将链表中所有元素输出
{
	if (llist->link != NULL)
	{
		PNode p = llist->link;
		while (p != NULL)
		{
			printf("%d ", p->info);
			p = p->link;
		}
		printf("\n");
	}
	else
		printf("空集！\n");
}

int AddToList(LinkList llist, DataType x) //将数据x插入链表中去
{
	PNode node = (PNode)malloc(sizeof(struct Node));
	if (node == NULL)
		return 0;
	node->info = x;
	node->link = llist->link;
	llist->link = node;
	return 1;
}