#include <stdio.h>
#include <stdlib.h>
#define MaxVertexNum 100
typedef char VexType;//顶点类型为字符型
typedef int AdjType;//边的类型为整型，有联系为1，无联系为0

int Visited[MaxVertexNum] = {0};//全局数组，先初始化为0，用于保存相应节点是否被访问过

typedef struct Graph
{
    VexType vex[MaxVertexNum];                //顶点向量
    AdjType arcs[MaxVertexNum][MaxVertexNum]; //邻接矩阵
    int n, e;                                 //图的当前顶点数和边数
} MGraph;
typedef MGraph *Graph;

void CreateGraph(Graph G)
{
    int i, j, k;
    printf("输入顶点数、边数：");
    scanf("%d%d", &G->n, &G->e); //输入顶点数，边数
    printf("输入所有顶点值(中间不带空格):\n");
    getchar();
    for (i = 0; i < G->n; i++)
        scanf("%c", &G->vex[i]); //输入顶点
    for (i = 0; i < G->n; i++)
        for (j = 0; j < G->n; j++)
            G->arcs[i][j] = 0; //初始化邻接矩阵为0
    printf("输入边的序号数值对(两个序号对应的输入序列中相应的顶点是相互连接的)：\n");
    for (k = 0; k < G->e; k++)
    { //输入邻接矩阵
        printf("输入第%d对数值:", k + 1);
        scanf("%d%d", &i, &j);                             //输入一条边依附的顶点对应的序号
        G->arcs[j - 1][i - 1] = G->arcs[i - 1][j - 1] = 1; //(v1, v2)的对称边(v2, v1)相同
    }
} 

VexType FirstAdjV(Graph G, VexType v)
{
    int i, j;
    for (i = 0; i < G->n; i++) //查找顶点v
        if (G->vex[i] == v)
            break;
    for (j = 0; j < G->n; j++) //查找矩阵中第一个非0值
        if (G->arcs[i][j] == 1)
            break;
    if (j < G->n)
        return (G->vex[j]); //返回第一邻接点
    else
        return '\0';//若无下一个顶点，返回标志'\0'，不再继续
}

VexType NextAdjV(Graph G, VexType v, VexType v1)
{
    int i, j, k;
    for (i = 0; i < G->n; i++) //查找顶点v所在行
        if (G->vex[i] == v)
            break;
    for (j = 0; j < G->n; j++) //查找顶点v1所在列
        if (G->vex[j] == v1)
            break;
    for (k = j + 1; k < G->n; k++) //查找v1下一个非0值
        if (G->arcs[i][k] == 1)
            break;
    if (k == G->n)
        return '\0';//若无法找到，即返回'\0'作为标志
    return (G->vex[k]); //返回下一顶点
}

int find(Graph G, char v)
{
    int i = 0;
    while (1)
    {
        if (G->vex[i] == v)
            return i;
        i++;
    }
}//此函数寻找v在顶点数组中的下标值

void DFS(Graph G, VexType v)
{ //以v为出发点对图G进行深度优先遍历
    VexType v1;
    Visited[find(G, v)] = 1; //标记v为已访问
    printf("%c", v);
    for (v1 = FirstAdjV(G, v); v1 != '\0'; v1 = NextAdjV(G, v, v1))
    {
        if (!Visited[find(G, v1)]) //若v1尚未被访问
            DFS(G, v1);            //则继续纵深搜索. 递归
    }
} //DFS

int max_not_visited(int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        if (Visited[i] != 1)
            break;
    }
    return i;
}//找到n范围内最大的未被访问到的结点对应下标

int main()
{
    Graph G;
    G = (Graph)malloc(sizeof(struct Graph));
    char tmp;
    int MaxNotVisited;
    CreateGraph(G);
    printf("想要从哪一个值开始遍历?请输入：");
    scanf(" %c", &tmp);
    while (1)
    {
        int i;
        for (i = 0; i < G->n; i++)
            if (tmp == G->vex[i])
                break;
        if (i < G->n)
            break;
        printf("输入错误，输入值不在所有顶点内，请重新输入一个起始值：");
        scanf(" %c", &tmp);
    }//判断是否为有效输入，若输入的开始值不在顶点数组内，提示错误并重新输入
    printf("图的深度优先遍历，结果为：\n");
    while (MaxNotVisited < G->n)
    {
        DFS(G, tmp);
        MaxNotVisited = max_not_visited(G->n);
        tmp = G->vex[MaxNotVisited];
    }//将没有相互连接的节点强制重新开始访问一遍，确保所有顶点都被访问
    return 0;
}