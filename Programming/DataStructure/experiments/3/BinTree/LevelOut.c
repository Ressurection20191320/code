#include <stdio.h>
#include <stdlib.h>
#define MAXSIZE 100

typedef struct BinTreeNode
{
    char data;
    struct BinTreeNode *Left;
    struct BinTreeNode *Right;
} * BinTree;    //二叉树结构定义，数据类型是字符型

typedef struct SeqQueue
{
    int max;
    int f, r;
    BinTree *q;
} * Queue;      //队列结构定义

Queue CreateQueue(int m)
{
    Queue pQ;
    pQ = (Queue)malloc(sizeof(struct SeqQueue));
    pQ->q = (BinTree *)malloc(sizeof(BinTree) * m);
    pQ->f = pQ->r = 0;
    pQ->max = m;
    return pQ;
}//创建队列

void AddQueue(Queue paqu, BinTree x)
{
    if ((paqu->r + 1) % paqu->max == paqu->f)
        printf("Queue is full!\n");
    else
    {
        paqu->q[paqu->r] = x;
        paqu->r++;
        paqu->r %= paqu->max;
    }
}//将一个二叉树指针压入队列

int IsEmptyQueue(Queue paqu)
{
    return (paqu->r == paqu->f);
}//判断队列是否为空

BinTree DeleteQueue(Queue paqu)
{
    int t = paqu->f;
    paqu->f++;
    paqu->f %= paqu->max;
    return (paqu->q[t]);
}//从队尾删除并取出元素

BinTree CreateBinTree() //树的建立（依照前序遍历）
{
    char data;
    BinTree T;
    scanf("%c", &data); //输入二叉树数据
    if (data == '#')    //二叉树为空，即为'#'
        T = NULL;
    else
    {
        T = (BinTree)malloc(sizeof(struct BinTreeNode)); 
        T->data = data;
        T->Left = CreateBinTree();
        T->Right = CreateBinTree();
    }//继续递归创建子树
    return T;
}

void LevelOrder(BinTree t)
{
    Queue q;
    BinTree bt;
    if (t == NULL)
        return;               // 若是空树直接返回
    q = CreateQueue(MAXSIZE); // 创建空队列
    AddQueue(q, t);//将根节点放入队列
    while (!IsEmptyQueue(q))
    {
        bt = DeleteQueue(q);
        printf("%c", bt->data); // 访问取出队列的结点
        if (bt->Left)
            AddQueue(q, bt->Left);
        if (bt->Right)
            AddQueue(q, bt->Right);//将左右子树放入队列
    }
}

int main()
{
    BinTree T;
    printf("请以先序,字符型(不加空格,空用\"#\"表示)输入二叉树:\n");
    T = CreateBinTree();
    printf("以先序输出二叉树结果如下:\n");
    PreOrder(T);
    printf("\n以中序输出二叉树结果如下:\n");
    InOrder(T);
    printf("\n以后序输出二叉树结果如下:\n");
    PostOrder(T);
    printf("\n以层序输出二叉树结果如下:\n");
    LevelOrder(T);
    return 0;
}