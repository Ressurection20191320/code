#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>
#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
#define KEYLENGTH  0x00800000

void HandleError(char *s);

//--------------------------------------------------------------------
//  These additional #define statements are required.
#define ENCRYPT_ALGORITHM CALG_RC4 
#define ENCRYPT_BLOCK_SIZE 8 

//   Declare the function EncryptFile. The function definition
//   follows main.

static BOOL EncryptFile(
				 PCHAR szSource, 
				 PCHAR szDestination, 
				 PCHAR szPassword); 

//--------------------------------------------------------------------
//   Begin main.

void main(void) 
{ 
    CHAR szSource[100]; 
    CHAR szDestination[100]; 
    CHAR szPassword[100]; 
	
	
	printf("Encrypt a file. \n\n");
	printf("Enter the name of the file to be encrypted: ");
	scanf("%s",szSource);
	printf("Enter the name of the output file: ");
	scanf("%s",szDestination);
	printf("Enter the password:");
	scanf("%s",szPassword);
	
	//--------------------------------------------------------------------
	// Call EncryptFile to do the actual encryption.
	
	if(EncryptFile(szSource, szDestination, szPassword))
	{
		printf("Encryption of the file %s was a success. \n", szSource);
		printf("The encrypted data is in file %s.\n",szDestination);
	}
	else
	{
		HandleError("Error encrypting file!"); 
	} 
} // End of main

//--------------------------------------------------------------------
//   Code for the function EncryptFile called by main.

static BOOL EncryptFile(
						PCHAR szSource, 
						PCHAR szDestination, 
						PCHAR szPassword)
						//--------------------------------------------------------------------
						//   Parameters passed are:
						//     szSource, the name of the input, a plaintext file.
						//     szDestination, the name of the output, an encrypted file to be 
						//         created.
						//     szPassword, the password.
{ 
	//--------------------------------------------------------------------
	//   Declare and initialize local variables.
	
	FILE *hSource; 
	FILE *hDestination; 
	
	HCRYPTPROV hCryptProv; 
	HCRYPTKEY hKey; 
	HCRYPTHASH hHash; 
		
	PBYTE pbBuffer; 
	DWORD dwBlockLen; 
	DWORD dwBufferLen; 
	DWORD dwCount; 
	
	//--------------------------------------------------------------------
	// Open source file. 
	if(hSource = fopen(szSource,"rb"))
	{
		printf("The source plaintext file, %s, is open. \n", szSource);
	}
	else
	{ 
		HandleError("Error opening source plaintext file!");
	} 

	//--------------------------------------------------------------------
	// Open destination file. 
	if(hDestination = fopen(szDestination,"wb"))
	{
		printf("Destination file %s is open. \n", szDestination);
	}
	else
	{
		HandleError("Error opening destination ciphertext file!"); 
	}

	//以下获得一个CSP句柄
	if(CryptAcquireContext(
		&hCryptProv, 
		NULL,				//NULL表示使用默认密钥容器，默认密钥容器名
//为用户登陆名
		NULL, 
		PROV_RSA_FULL, 
		0))
	{
		printf("A cryptographic provider has been acquired. \n");
	}
	else
	{
		if(CryptAcquireContext(
			&hCryptProv, 
			NULL, 
			NULL, 
			PROV_RSA_FULL, 
			CRYPT_NEWKEYSET))//创建密钥容器
		{
			//创建密钥容器成功，并得到CSP句柄
			printf("A new key container has been created.\n");
		}
		else
		{
			HandleError("Could not create a new key container.\n");
		}
		
	}

	//--------------------------------------------------------------------
	// 创建一个会话密钥（session key）
	// 会话密钥也叫对称密钥，用于对称加密算法。
	// （注: 一个Session是指从调用函数CryptAcquireContext到调用函数
	//   CryptReleaseContext 期间的阶段。会话密钥只能存在于一个会话过程）

	//--------------------------------------------------------------------
	// Create a hash object. 
	if(CryptCreateHash(
		hCryptProv, 
		CALG_MD5, 
		0, 
		0, 
		&hHash))
    {
        printf("A hash object has been created. \n");
    }
    else
    { 
		HandleError("Error during CryptCreateHash!\n");
    }  

	//--------------------------------------------------------------------
	// 用输入的密码产生一个散列
	if(CryptHashData(
		hHash, 
		(BYTE *)szPassword, 
		strlen(szPassword), 
		0))
	{
		printf("The password has been added to the hash. \n");
	}
	else
	{
		HandleError("Error during CryptHashData. \n"); 
	}

	//--------------------------------------------------------------------
	// 通过散列生成会话密钥
	if(CryptDeriveKey(
		hCryptProv, 
		ENCRYPT_ALGORITHM, 
		hHash, 
		KEYLENGTH, 
		&hKey))
	{
		printf("An encryption key is derived from the password hash. \n"); 
	}
	else
	{
		HandleError("Error during CryptDeriveKey!\n"); 
	}
	//--------------------------------------------------------------------
	// Destroy the hash object. 
	
	CryptDestroyHash(hHash); 
	hHash = NULL; 
	
	//--------------------------------------------------------------------
	//  The session key is now ready. 
	
	//--------------------------------------------------------------------
	// 因为加密算法是按ENCRYPT_BLOCK_SIZE 大小的块加密的，所以被加密的
// 数据长度必须是ENCRYPT_BLOCK_SIZE 的整数倍。下面计算一次加密的
// 数据长度。

	dwBlockLen = 1000 - 1000 % ENCRYPT_BLOCK_SIZE; 
	
	//--------------------------------------------------------------------
	// Determine the block size. If a block cipher is used, 
	// it must have room for an extra block. 
	
	if(ENCRYPT_BLOCK_SIZE > 1) 
		dwBufferLen = dwBlockLen + ENCRYPT_BLOCK_SIZE; 
	else 
		dwBufferLen = dwBlockLen; 
	
	//--------------------------------------------------------------------
	// Allocate memory. 
	if(pbBuffer = (BYTE *)malloc(dwBufferLen))
	{
		printf("Memory has been allocated for the buffer. \n");
	}
	else
	{ 
		HandleError("Out of memory. \n"); 
	}
	//--------------------------------------------------------------------
	// In a do loop, encrypt the source file and write to the source file. 
	
	do 
	{ 
		
		//--------------------------------------------------------------------
		// Read up to dwBlockLen bytes from the source file. 
		dwCount = fread(pbBuffer, 1, dwBlockLen, hSource); 
		if(ferror(hSource))
		{ 
			HandleError("Error reading plaintext!\n");
		}
		
		//--------------------------------------------------------------------
		// 加密数据
		if(!CryptEncrypt(
			hKey,			//密钥
			0,				//如果数据同时进行散列和加密，这里传入一个
//散列对象
			feof(hSource),	//如果是最后一个被加密的块，输入TRUE.如果不是输.
							//入FALSE这里通过判断是否到文件尾来决定是否为
//最后一块。
			0,				//保留
			pbBuffer,		//输入被加密数据，输出加密后的数据
			&dwCount,		//输入被加密数据实际长度，输出加密后数据长度
			dwBufferLen))	//pbBuffer的大小。
		{ 
			HandleError("Error during CryptEncrypt. \n"); 
		} 
		
		//--------------------------------------------------------------------
		// Write data to the destination file. 
		
		fwrite(pbBuffer, 1, dwCount, hDestination); 
		if(ferror(hDestination))
		{ 
			HandleError("Error writing ciphertext.");
		}
		
	} 
	while(!feof(hSource)); 
	//--------------------------------------------------------------------
	//  End the do loop when the last block of the source file has been
	//  read, encrypted, and written to the destination file.
	
	//--------------------------------------------------------------------
	// Close files.
	
	if(hSource) 
		fclose(hSource); 
	if(hDestination) 
		fclose(hDestination); 
	
	//--------------------------------------------------------------------
	// Free memory. 
	
	if(pbBuffer) 
		free(pbBuffer); 
	
	//--------------------------------------------------------------------
	// Destroy session key. 
	
	if(hKey) 
		CryptDestroyKey(hKey); 
	
	//--------------------------------------------------------------------
	// Destroy hash object. 
	
	if(hHash) 
		CryptDestroyHash(hHash); 
	
	//--------------------------------------------------------------------
	// Release provider handle. 
	
	if(hCryptProv) 
		CryptReleaseContext(hCryptProv, 0);
	return(TRUE); 
} // End of Encryptfile

//--------------------------------------------------------------------
//  This example uses the function HandleError, a simple error
//  handling function, to print an error message to the standard error 
//  (stderr) file and exit the program. 
//  For most applications, replace this function with one 
//  that does more extensive error reporting.

void HandleError(char *s)
{
    fprintf(stderr,"An error occurred in running the program. \n");
    fprintf(stderr,"%s\n",s);
    fprintf(stderr, "Error number %x.\n", GetLastError());
    fprintf(stderr, "Program terminating. \n");
    exit(1);
} // End of HandleError


#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>
#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
#define KEYLENGTH  0x00800000
void HandleError(char *s);

//--------------------------------------------------------------------
//   The following #define statements are also required.

#define ENCRYPT_ALGORITHM CALG_RC4 
#define ENCRYPT_BLOCK_SIZE 8 

//--------------------------------------------------------------------
//    Declare the function DecryptFile. Code for the function follows
//    main.

BOOL DecryptFile(
				 PCHAR szSource, 
				 PCHAR szDestination, 
				 PCHAR szPassword); 

void main(void) 
{ 
	//--------------------------------------------------------------------
	//    Declare and initialize variables.
	
	CHAR szSource[100]; 
	CHAR szDestination[100]; 
	CHAR szPassword[100]; 
	
	printf("Decrypt a file. \n\n");
	printf("Enter the name of the file to be decrypted: ");
	scanf("%s",szSource);
	printf("Enter the name of the output file: ");
	scanf("%s",szDestination);
	printf("Enter the password:");
	scanf("%s",szPassword);
	
	if(!DecryptFile(szSource, szDestination, szPassword))
	{
		printf("\nError decrypting file. \n"); 
	}
	else
	{ 
		printf("\nDecryption of file %s succeeded. \n", szSource);
		printf("The decrypted file is %s .\n",szDestination);
	}
} // End of main

//--------------------------------------------------------------------
//    Define the function Decryptfile.

static BOOL DecryptFile(
						PCHAR szSource, 
						PCHAR szDestination, 
						PCHAR szPassword) 
{ 
	//--------------------------------------------------------------------
	//   Declare and initialize local variables.
	
	FILE *hSource; 
	FILE *hDestination; 
	
	HCRYPTPROV hCryptProv; 
	HCRYPTKEY hKey; 
	HCRYPTHASH hHash; 
	
	PBYTE pbBuffer; 
	DWORD dwBlockLen; 
	DWORD dwBufferLen; 
	DWORD dwCount; 
	
	BOOL status = FALSE; 
	
	//--------------------------------------------------------------------
	// Open source file. 
	if(!(hSource = fopen(szSource,"rb"))) 
	{
		HandleError("Error opening ciphertext file!");
	}
	//--------------------------------------------------------------------
	// Open destination file. 
	
	if(!(hDestination = fopen(szDestination,"wb")))
	{
		HandleError("Error opening plaintext file!");
	} 
	//--------------------------------------------------------------------
	// Get a handle to the default provider. 
	if(!CryptAcquireContext(
		&hCryptProv, 
		NULL, 
		NULL, 
		PROV_RSA_FULL, 
		0))
	{
		HandleError("Error during CryptAcquireContext!"); 
	}
	
	//--------------------------------------------------------------------
	// Decrypt the file with a session key derived from a password. 
	
	//--------------------------------------------------------------------
	// Create a hash object. 
	if(!CryptCreateHash(
		hCryptProv, 
		CALG_MD5, 
		0, 
		0, 
		&hHash))
	{
		HandleError("Error during CryptCreateHash!");
	}
	//--------------------------------------------------------------------
	// Hash in the password data. 
	
	if(!CryptHashData(
		hHash, 
		(BYTE *)szPassword, 
		strlen(szPassword), 
		0)) 
	{
		HandleError("Error during CryptHashData!"); 
	}
	//--------------------------------------------------------------------
	// Derive a session key from the hash object. 
	
	if(!CryptDeriveKey(
		hCryptProv, 
		ENCRYPT_ALGORITHM, 
		hHash, 
		KEYLENGTH, 
		&hKey))
	{ 
		HandleError("Error during CryptDeriveKey!"); 
	}
	//--------------------------------------------------------------------
	// Destroy the hash object. 
	
	CryptDestroyHash(hHash); 
	hHash = 0; 
	
	//--------------------------------------------------------------------
	//--------------------------------------------------------------------
	// Determine the number of bytes to decrypt at a time. 
	// This must be a multiple of ENCRYPT_BLOCK_SIZE. 
	
	dwBlockLen = 1000 - 1000 % ENCRYPT_BLOCK_SIZE; 
	dwBufferLen = dwBlockLen; 
	
	//--------------------------------------------------------------------
	// Allocate memory. 
	
	if(!(pbBuffer = (BYTE *)malloc(dwBufferLen)))
	{
		HandleError("Out of memory!\n"); 
	}
	//--------------------------------------------------------------------
	// Decrypt source file, and write to destination file. 
	
	do { 
		//--------------------------------------------------------------------
		// Read up to dwBlockLen bytes from source file. 
		
		dwCount = fread(
			pbBuffer, 
			1, 
			dwBlockLen, 
			hSource); 
		if(ferror(hSource))
		{
			HandleError("Error reading ciphertext!");
		}
		//--------------------------------------------------------------------
		// Decrypt data. 
		if(!CryptDecrypt(
			hKey, 
			0, 
			feof(hSource), 
			0, 
			pbBuffer, 
			&dwCount))
		{
			HandleError("Error during CryptDecrypt!"); 
		}
		//--------------------------------------------------------------------
		// Write data to destination file. 
		
		fwrite(
			pbBuffer, 
			1, 
			dwCount, 
			hDestination); 
		if(ferror(hDestination))
		{
			HandleError("Error writing plaintext!"); 
		}
	} while(!feof(hSource)); 
	status = TRUE; 
	
	//--------------------------------------------------------------------
	// Close files. 
	if(hSource) 
		fclose(hSource); 
	if(hDestination) 
		fclose(hDestination); 
	
	//--------------------------------------------------------------------
	// Free memory. 
	
	if(pbBuffer) 
		free(pbBuffer); 
	
	//--------------------------------------------------------------------
	// Destroy session key. 
	
	if(hKey) 
		CryptDestroyKey(hKey); 
	
	//--------------------------------------------------------------------
	// Destroy hash object. 
	if(hHash) 
		CryptDestroyHash(hHash); 
	
	//--------------------------------------------------------------------
	// Release provider handle. 
	
	if(hCryptProv) 
		CryptReleaseContext(hCryptProv, 0); 
	
	return status;
} // End of Decryptfile

//--------------------------------------------------------------------
//  This example uses the function HandleError, a simple error
//  handling function, to print an error message to the standard error 
//  (stderr) file and exit the program. 
//  For most applications, replace this function with one 
//  that does more extensive error reporting.

void HandleError(char *s)
{
    fprintf(stderr,"An error occurred in running the program. \n");
    fprintf(stderr,"%s\n",s);
    fprintf(stderr, "Error number %x.\n", GetLastError());
    fprintf(stderr, "Program terminating. \n");
    exit(1);
} // End of HandleError


#include <stdio.h>
#include <windows.h>
#include <Wincrypt.h>
 
#define CHECK_NULL_RET(bCondition) if (!bCondition) goto Exit0
#define BUFSIZE 1024
#define MD5LEN  16
 
BOOL GetContentMD5(
    BYTE *pszFilePath, 
    BOOL bFile, 
    BOOL bUpperCase, 
    TCHAR *pszResult,
    DWORD &dwStatus)
{
    BOOL bResult = FALSE;
    HCRYPTPROV hProv = 0;
    HCRYPTHASH hHash = 0;
    HANDLE hFile = NULL;
    BYTE rgbFile[BUFSIZE];
    DWORD cbRead = 0;
    BYTE rgbHash[MD5LEN];
    DWORD cbHash = 0;
    CHAR rgbDigitsL[] = "0123456789abcdef";
    CHAR rgbDigitsU[] = "0123456789ABCDEF";
    CHAR *rgbDigits = bUpperCase ? rgbDigitsU : rgbDigitsL;
    TCHAR szResult[MD5LEN*2+1] = {0};
 
    dwStatus = 0;
    bResult = CryptAcquireContext(&hProv,
        NULL,
        NULL,
        PROV_RSA_FULL,
        CRYPT_VERIFYCONTEXT);
    CHECK_NULL_RET(bResult);
 
    bResult = CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash);
    CHECK_NULL_RET(bResult);
 
    if (bFile)
    {
        hFile = CreateFile((TCHAR *)pszFilePath,
            GENERIC_READ,
            FILE_SHARE_READ,
            NULL,
            OPEN_EXISTING,
            FILE_FLAG_SEQUENTIAL_SCAN,
            NULL);
        CHECK_NULL_RET(!(INVALID_HANDLE_VALUE == hFile));
 
        while (bResult = ReadFile(hFile, rgbFile, BUFSIZE, 
            &cbRead, NULL))
        {
            if (0 == cbRead)
            {
                break;
            }
 
            bResult = CryptHashData(hHash, rgbFile, cbRead, 0);
            CHECK_NULL_RET(bResult);
        }
    }
    else
    {
        bResult = CryptHashData(hHash, pszFilePath, strlen((CHAR *)pszFilePath), 0);
        CHECK_NULL_RET(bResult);
    }
 
    cbHash = MD5LEN;
    if (bResult = CryptGetHashParam(hHash, HP_HASHVAL, rgbHash, &cbHash, 0))
    {
        TCHAR szTmpBuff[3] ;
        for (DWORD i = 0; i < cbHash; i++)
        {
            sprintf  (szTmpBuff, TEXT("%c%c"), rgbDigits[rgbHash[i]>>4],
                rgbDigits[rgbHash[i] & 0xf]);
            lstrcat(szResult, szTmpBuff);
        }
        bResult = TRUE;
    }
 
Exit0:
    dwStatus = GetLastError();
    CryptDestroyHash(hHash);
    CryptReleaseContext(hProv, 0);
    CloseHandle(hFile);
 
    lstrcpy(pszResult, szResult);
 
    return bResult; 
}
 
int main(int argc, char* argv[])
{
    DWORD dwStatus = 0;
    TCHAR szResult[MD5LEN*2+1] = {0};
    TCHAR szFilePath[] = TEXT("C:\\123.dll");
    CHAR szContent[] = "explorer.exe";
 
    GetContentMD5((BYTE *)szFilePath, 
        TRUE, TRUE, szResult, dwStatus);
    MessageBox(NULL, szResult, TEXT("该文件的MD5"), MB_OK); 
    return 0;
}
