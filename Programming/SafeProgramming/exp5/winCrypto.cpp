// ConsoleApplication7.cpp : 定义控制台应用程序的入口点。
//
#pragma comment(lib, "crypt32.lib")
#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>
#define _CRT_SECURE_NO_DEPRECATE
#define MY_ENCODING_TYPE (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
#define KEYLENGTH 0x00800000


void HandleError(char *s);

//--------------------------------------------------------------------
// These additional #define statements are required.
#define ENCRYPT_ALGORITHM CALG_RC4
#define ENCRYPT_BLOCK_SIZE 8
#define MAX_FILE_SIZE 4000000
#define SIGNATURE_SIZE 500

BYTE *pbKeyBlob; // 用来保存导出的公钥
DWORD dwBlobLen;

//   Declare the functions. The function definition
//   follows main.
BOOL CryptVerifyFile(
	PCHAR szSource,
	PCHAR szDestination);
BOOL CryptSignFile(
	PCHAR szSource,
	PCHAR szDestination);
BOOL CryptDecryptFile(
	PCHAR szSource,
	PCHAR szDestination,
	PCHAR szPassword);

BOOL CryptEncryptFile(
	PCHAR szSource,
	PCHAR szDestination,
	PCHAR szPassword);

//--------------------------------------------------------------------
//   Begin main.

int main(int argc, char* argv[])
{
	CHAR szSource[100];
	CHAR szDestination[100];
	CHAR szPassword[100];


	//--------------------------------------------------------------------
	// Call EncryptFile to do the actual encryption.   加密文件
	printf("/n------------------------------------------------------------/n");
	printf("/n/n1.Encrypt a file. /n/n");
	printf("/nEnter the name of the file to be encrypted: ");
	scanf("%s", szSource);
	printf("/nEnter the name of the output file: ");
	scanf("%s", szDestination);
	printf("/nEnter the password:");
	scanf("%s", szPassword);

	if (CryptEncryptFile(szSource, szDestination, szPassword))
	{
		printf("/nEncryption of the file %s was a success. /n", szSource);
		printf("/nThe encrypted data is in file %s./n", szDestination);
	}
	else
	{
		HandleError("/nError encrypting file!");
	}


	//--------------------------------------------------------------------
	// Call Decryptfile to do the actual decryption.   解密文件
	printf("/n------------------------------------------------------------/n");
	printf("/n/n2.Decrypt a file. /n/n");
	printf("/nEnter the name of the file to be decrypted: ");
	scanf("%s", szSource);
	printf("/nEnter the name of the output file: ");
	scanf("%s", szDestination);
	printf("/nEnter the password:");
	scanf("%s", szPassword);

	if (CryptDecryptFile(szSource, szDestination, szPassword))
	{
		printf("/nDecryption of the file %s was a success. /n", szSource);
		printf("/nThe decrypted data is in file %s./n", szDestination);
	}
	else
	{
		HandleError("/nError decrypting file!");
	}

	//--------------------------------------------------------------------
	// Call SignFile to do the actual signature             签名文件
	printf("/n------------------------------------------------------------/n");
	printf("/n/n3.Sign a file. /n/n");
	printf("/nEnter the name of the file to be signed: ");
	scanf("%s", szSource);
	printf("/nEnter the name of the signature file: ");
	scanf("%s", szDestination);

	if (CryptSignFile(szSource, szDestination))
	{
		printf("/nSignature of the file %s was a success. /n", szSource);
		printf("/nThe signature data is in file %s./n", szDestination);
	}
	else
	{
		HandleError("/nError while signing the file!");
	}

	//---------------------------------------------------------------------
	// Call VerifyFile to do the actual verification   验证签名
	printf("/n------------------------------------------------------------/n");
	printf("/n/n4.Verify a file and its signature. /n/n");
	printf("/nEnter the name of the file to be verified: ");
	scanf("%s", szSource);
	printf("/nEnter the name of the signature file: ");
	scanf("%s", szDestination);
	//printf("/nEnter the name of the public key file: ");
	//scanf("%s",szDestination);

	if (CryptVerifyFile(szSource, szDestination))
	{
		printf("/nVerification of the file %s was a success. /n", szSource);
	}
	else
	{
		HandleError("/nVerification failed. Error file!");
	}
	return 0;
} // End of main