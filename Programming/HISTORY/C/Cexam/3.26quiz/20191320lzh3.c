#include <stdio.h>

int main()
{
    int n, x, result;
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &x);
        result = x & (x - 1);
        if (result == 0)
            printf("yes\n");
        else
            printf("no\n");
    }
}