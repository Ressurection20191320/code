#include <stdio.h>

int check(int n)
{
    for (int i = 2; i < n; i++)
    {
        if (n % i == 0)
            return 0;
    }
    return 1;
}

int main()
{
    int n, x, count;
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        count=0;
        scanf("%d", &x);
        for (int m = 2; m < x; m++)
        {
            if (check(m))
                count++;
        }
        printf("%d\n", count);
    }
    return 0;
}