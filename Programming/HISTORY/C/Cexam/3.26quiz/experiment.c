#include <stdio.h>

int mod(int a, int n, int p)
{
    int result = 1, i = 1, j = 0;
    while (n > 32)
    {
        if (n % 2 == 0)
        {
            n /= 2;
            i *= 2;
        }

        else if (n % 2 != 0)
        {
            n = n >> 1;
            j += i;
            i *= 2;
        }
    }
    for (int i1 = 0; i1 < n; i1++)
    {
        result *= a;
        result %= p;
        if (result % p == 0)
        {
            result = 0;
            return 0;
        }
    }
    int tmp = result;
    for (int i2 = 0; i2 < i - 1; i2++)
    {
        result *= tmp;
        result %= p;
    }
    for (int i3 = 0; i3 < j; i3++)
    {
        result *= a;
        result %= p;
    }
    return result;
}

int main()
{
    int a, n, p;
    scanf("%d%d%d", &a, &n, &p);
    printf("%d", mod(a, n, p));
    return 0;
}