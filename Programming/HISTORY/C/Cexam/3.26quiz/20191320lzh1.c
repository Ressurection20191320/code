#include <stdio.h>
int reverse(int n)
{
    int a, sum = 0;
    while (n)
    {
        a = n % 10;
        sum = sum * 10 + a;
        n /= 10;
    }
    return sum;
}

int main()
{
    int n,x;
    scanf("%d",&n);
    for(int i=0;i<n;i++)
    {
        scanf("%d",&x);
        printf("%d\n",reverse(x));
    }
    return 0;
}