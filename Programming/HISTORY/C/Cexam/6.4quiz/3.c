#include <stdio.h>
void count_common(void);

void count_common(void)
{
    int A, B;
    scanf("%d %d", &A, &B);
    int counter = 0;
    // your code - start
    if (B > A)
    {
        int tmp = A;
        A = B;
        B = tmp;
    }
    while (A != B && A)
        A /= 2;
    if (A == B)
    {
        for (; A % 2 == 0; counter++, A /= 2)
            ;
        counter++;
    }
    // your code - end
    printf("%d\n", counter);
}

int main()
{
    count_common();
    return 0;
}