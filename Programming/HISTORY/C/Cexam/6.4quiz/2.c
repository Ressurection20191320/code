#include <stdio.h>
#include <string.h>

#define MAX_LEN 50
char s[MAX_LEN + 1];
char t[MAX_LEN + 1];
enum bool
{
    false,
    true
};
typedef enum bool Bool;
void check_convertable()
{
    scanf("%s %s", s, t);
    Bool flag = true;
    // your code - start
    int i, j;
    for (i = 0, j = 0; i < strlen(s); i++)
    {
        if (s[i] == t[j])
            j++;
    }
    if (strlen(s) != strlen(t) + 1 || j != strlen(t))
        flag--;
    // your code - end
    if (flag)
        printf("%s", "Possible");
    else
        printf("%s", "Impossible");
}

int main()
{
    check_convertable();
}