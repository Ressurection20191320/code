#include <stdio.h>
#include <math.h>

void get_num_apples()
{
    int K, T;
    int n = -1;
    scanf("%d %d", &K, &T);
    // your code - start
    int sum,i1;
    for (i1 = 1; sum != T; i1++)
    {
        sum = 0;
        for (int i2 = 1; i2 <= i1; i2++)
        {
            sum += pow(2, i2 - 1) * K;
        }
    }
    // your code - end
    printf("%d", i1-1);
}

int main()
{
    get_num_apples();
}