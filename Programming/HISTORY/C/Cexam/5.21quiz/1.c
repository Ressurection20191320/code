#include <stdio.h>
#include <string.h>

#define MAX_LEN 50
#define N 50

void check_similar(char s[][MAX_LEN + 1], int n);

void check_similar(char s[][MAX_LEN + 1], int n)
{
    char *messages[] = {
        "Similar handles found",
        "Similar handles not found",
    };
    int flag = 1;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < strlen(s[i]); j++)
        {
            s[i][j] = s[i][j] == '0' ? 'O' : s[i][j];
            s[i][j] = s[i][j] == 'l' ? '1' : s[i][j];
            s[i][j] = s[i][j] == 'I' ? '1' : s[i][j];
        }
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            if (strcmp(s[i], s[j]) == 0)
                flag = 0;
        }
    }
    printf("%s\n", messages[flag]);
}

int main()
{
    char accounts[N][MAX_LEN + 1];
    int n = 0;
    while (gets(accounts[n]) != NULL)
    {
        n++;
    }
    check_similar(accounts, n);
    return 0;
}
