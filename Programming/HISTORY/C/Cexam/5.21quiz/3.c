#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_LEN 50

int get_depth(char *s)
{
    int depth = 0, len_s = strlen(s);
    for (int i = 0, stk = 0; i < len_s; i++)
    {
        if (s[i] == '(')
            stk++;
        else if (s[i] == ')')
            stk--;
        depth=stk>depth?stk:depth;
    }
    printf("%d\n", depth);
}

int main()
{
    char s[MAX_LEN + 1];
    gets(s);
    get_depth(s);
    return 0;
}
