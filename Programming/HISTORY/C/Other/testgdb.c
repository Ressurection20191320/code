/*
	Name:testgdb.c
	Copyright:  rocedu
	Author: rocedu
	Date: 03/02/20 10:23
	Description:
*/

#include <stdio.h>
void display(int i) {
	printf("%d \n", i);
}

int main(int argc,char *argv[]) {

	int i, j;
	i = 10;
	j = 0;

	for(i=0; i<1000; i++) {
		j+=5;
		display(j);
	}

}


