#include <stdio.h>
#include <math.h>
int main()
{
    int x, a, b, c, y;
    fprintf(stdout, "Input x:\n");
    fscanf(stdin, "%d", &x);
    x = fabs(x);
    a = x / 100;
    b = x % 100 / 10;
    c = x % 10;
    y = a + 10 * b + 100 * c;
    fprintf(stdout, "y=%d\n", y);
    return 0;
}