#include<stdio.h>
#include<math.h>

int main()
{
    printf("Input rate, year, capital:");
    double rate,capital,deposit;
    int year;
    char ch;
    scanf("%lf,%d,%lf",&rate,&year,&capital);
    printf("Compound interest (Y/N)?");
    scanf(" %c",&ch);
    if(ch=='Y'||ch=='y')
        deposit=capital*pow(1+rate,year);
    else if(ch=='N'||ch=='n')
        deposit=capital*(1+rate*year);
    printf("deposit = %.4f\n",deposit);
    return 0;
}