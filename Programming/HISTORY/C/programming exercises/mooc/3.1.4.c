#include <stdio.h>
#include <math.h>
int main()
{
    int x, y, x1, x2, x3, x4, a, b, result;
    printf("Input x:\n");
    scanf("%d", &x);
    x = fabs(x);
    x1 = x / 1000;
    x2 = (x - x / 1000 * 1000) / 100;
    x3 = (x - x % 10) % 100 / 10;
    x4 = x % 10;
    y = x1 + x2 * 10 + x3 * 100 + x4 * 1000;
    a = x4 * 10 + x3;
    b = x2 * 10 + x1;
    result = pow(a, 2) + pow(b, 2);
    printf("y=%d\na=%d,b=%d\nresult=%d\n", y, a, b, result);
    return 0;
}