#include <stdio.h>
#include <math.h>

int main()
{
    printf("Please enter the coefficients a,b,c:\n");
    float a, b, c, j, derta, x1, x2;
    int suc = scanf("%f,%f,%f", &a, &b, &c);
    j = b * b - 4 * a * c;
    if (j < 0 || suc < 3)
        printf("error!\n");
    else
    {
        derta = sqrt(j);
        x1 = (-b + derta) / (2 * a);
        x2 = (-b - derta) / (2 * a);
        printf("x1=%7.4f, x2=%7.4f\n", x1, x2); 
    }
    return 0;
}