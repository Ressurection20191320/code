#include <stdio.h>

int main()
{
    int area;
    float weight, price;
    scanf("%d,%f", &area, &weight);
    weight = weight / 1 <= 0.00001F ? weight : (int)weight + 1.0F;
    switch (area)
    {
    case 0:
        price = 10 + (weight - 1) * 3;
        break;
    case 1:
        price = 10 + (weight - 1) * 4;
        break;
    case 2:
        price = 15 + (weight - 1) * 5;
        break;
    case 3:
        price = 15 + (weight - 1) * 6.5;
        break;
    case 4:
        price = 15 + (weight - 1) * 10;
        break;
    default:
        printf("Error in Area\n");
    }
    printf("Price: %5.2f\n", price);
    return 0;
}