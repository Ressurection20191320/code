#include <stdio.h>
#include <math.h>

int main()
{
    printf("Input capital, year:");
    double rate, capital, deposit;
    int year, flag = 0;
    char ch;
    scanf("%lf,%d", &capital, &year);
    printf("Compound interest (Y/N)?");
    getchar();
    scanf("%c", &ch);
    switch (year)
    {
    case 1:
        rate = 0.0225;
        break;
    case 2:
        rate = 0.0243;
        break;
    case 3:
        rate = 0.0270;
        break;
    case 5:
        rate = 0.0288;
        break;
    case 8:
        rate = 0.0300;
        break;
    default:
        flag = 1;
    }
    if (flag == 0)
    {
        if (ch == 'Y' || ch == 'y')
            deposit = capital * pow(1 + rate, year);
        else if (ch == 'N' || ch == 'n')
            deposit = capital * (1 + rate * year);
        printf("rate = %.4f, deposit = %.4f\n", rate, deposit);
    }
    else
        printf("Error year!\n");
    return 0;
}