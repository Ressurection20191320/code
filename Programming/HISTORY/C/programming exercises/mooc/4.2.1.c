#include <stdio.h>

int main()
{
    printf("Input a/b, c/d:");
    int a, b, c, d, x, y;
    scanf("%d/%d,%d/%d", &a, &b, &c, &d);
    x = a * d;
    y = c * b;
    if (x < y)
        printf("%d/%d<%d/%d\n", a, b, c, d);

    else if (x > y)
        printf("%d/%d>%d/%d\n", a, b, c, d);
    else
        printf("%d/%d=%d/%d\n", a, b, c, d);
    return 0;
}