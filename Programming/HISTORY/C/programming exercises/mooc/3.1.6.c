#include <stdio.h>

int main()
{
    int w, dh;
    float fh, t;
    printf("Input weight, height:\n");
    scanf("%d,%d", &w, &dh);
    fh = (float)dh;
    fh /= 100;
    t = w / (fh * fh);
    printf("weight=%d\n", 2 * w);
    printf("height=%.2f\n", fh);
    printf("t=%.2f\n", t);
    return 0;
}