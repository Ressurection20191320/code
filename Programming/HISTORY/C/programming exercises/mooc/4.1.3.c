#include <stdio.h>
int main()
{
    int score, flag = 0;
    char grade;
    printf("Please input score:\n");
    int flag0 = scanf("%d", &score);
    if (score < 0 || score > 100 || flag0 == 0)
    {
        printf("Input error!\n");
        flag = 1;
    }
    else if (score >= 90)
        grade = 'A';
    else if (score >= 80)
        grade = 'B';
    else if (score >= 70)
        grade = 'C';
    else if (score >= 60)
        grade = 'D';
    else
        grade = 'E';
    if (flag == 0)
        printf("grade: %c\n", grade);
    return 0;
}