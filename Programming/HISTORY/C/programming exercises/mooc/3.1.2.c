#include <stdio.h>
int main()
{
    int n, m, d, y;
    float p;
    printf("Enter item number:\n");
    scanf("%d", &n);
    printf("Enter unit price:\n");
    scanf("%f", &p);
    printf("Enter purchase date (mm/dd/yy):\n");
    scanf("%d/%d/%d", &m, &d, &y);
    printf("Item      Unit     Purchase\n%-9d$ %-9.2f%02d%02d%04d\n", n, p, m, d, y);
    return 0;
}