#include <stdio.h>

int main()
{
    int year;
    if (scanf("%d", &year) && year >= 0)
        if (year % 4 == 0 && !(year % 100 == 0) || year % 400 == 0)
            printf("Yes\n");
        else
            printf("No\n");
    else
        printf("Input error!");
    return 0;
}