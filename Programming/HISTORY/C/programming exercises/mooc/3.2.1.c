#include <stdio.h>
int main()
{
    int math1, English, math2;
    float F;
    printf("Input math1, English and math2:");
    scanf("%d,%d,%d", &math1, &English, &math2);
    F = (math1 * 5 + English * 1.5 + math2 * 3.5) / 10;
    printf("Final score = %.2f\n", F);
    return 0;
}