#include <stdio.h>
#include <math.h>
int main()
{
    float x, y, sum;
    printf("Please input x and y:\n");
    scanf("%f,%f", &x, &y);
    sum = pow(x, 2) + pow(y, 2);
    printf("Result=%.2f\n", sum);
    return 0;
}