#include <stdio.h>
int main()
{
    float length;
    int days;
    printf("Input length and days:");
    scanf("%f,%d", &length, &days);
    for (int i = 0; i < days; i++)
        length /= 2;
    printf("length=%.5f\n", length);
    return 0;
}