#include <stdio.h>
#include <math.h>
int main()
{
    int line[100];
    int line2[100];
    int i, j, k = 0;
    int num, temp1, temp2, temp3;
    while (1)
    {
        scanf("%d", &line[k]);
        if (line[k] == -1)
            break;
        k++;

    for (num = 0; num < k; num++)
    {
        int tmp = 0, linetmp;
        linetmp = line[num];
        while (linetmp)
        {
            tmp += (linetmp % 10) * (linetmp % 10) * (linetmp % 10);
            linetmp /= 10;
        }
        line2[num] = tmp; 
    }

    for (i = 0; i < k; i++)
    {
        for (j = i + 1; j < k; j++)
            if (line2[j] > line2[i])
            {
                temp1 = line2[i]; 
                line2[i] = line2[j];
                line2[j] = temp1;

                temp2 = line[i]; 
                line[i] = line[j];
                line[j] = temp2;
            }
    }

    for (i = 0; i < k; i++)
    {
        if (line2[i] == line2[i + 1]) 
        {
            if (line[i] < line[i + 1])
            {
                temp3 = line[i];
                line[i] = line[i + 1];
                line[i + 1] = temp3;
            }
        }
    }

    for (i = 0; i < k; i++)
    {
        printf("%d\n", line[i]);
    }
    return 0;
}