#include <stdio.h>
#include <string.h>
#include <stdlib.h>
void BubbleSort(char num[1000][1000], int n)
{
    int i, j;
    char tmp[1000];
    for (i = 0; i < n - 1; i++)
        for (j = 1; j < n - 1; j++)
        {
            if (strcmp(num[j], num[j - 1]) < 0)
            {
                strncpy(tmp, num[j], 1000);
                strncpy(num[j], num[j - 1], 1000);
                strncpy(num[j - 1], tmp, 1000);
            }
        }
}
int main()
{
    char str[1000];
    scanf("%s", str);
    int len_str = strlen(str);
    char s[1000][1000];
    memset(s, 0, sizeof(s));
    int i, j, k = 0;
    for (i = 0; i < len_str; i++)
        for (j = 1; i + j <= len_str; j++)
            strncpy(s[k++], str + i, j);
    for (int m = 0; m < 1000; m++)
        for (int n = m + 1; n <= 1000; n++)
            if (strcmp(s[m], s[n]) == 0)
                s[n][0] = 0;
    BubbleSort(s, 1000);
    for (int o = 0; o < 1000; o++)
    {
        if (s[o][0] == '\0')
            continue;
        printf("%s\n", s[o]);
    }
    //system("pause");
    return 0;
}