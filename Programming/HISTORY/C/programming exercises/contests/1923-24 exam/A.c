#include <stdio.h>
int cnt(int x)
{
    int i;
    int cnt = 0;
    for (i = 1; i <= x; i++)
    {
        if (x % i == 0)
            cnt++;
    }
    return cnt;
}

int main()
{
    int n;
    int max = 0;
    do
    {
        scanf("%d", &n);
        if (cnt(n) > max)
            max = cnt(n);
    } while (n != 0);
    printf("%d\n", max);
    return 0;
}