#include <stdio.h>
#include <string.h>
int main()
{
    char str[81];
    int cnt_n = 0, cnt_ch = 0, cnt_other;
    gets(str);
    int str_len = strlen(str);
    for (int i = 0; i < str_len; i++)
    {
        if (str[i] >= '0' && str[i] <= '9')
            cnt_n++;
        else if (str[i] >= 'a' && str[i] <= 'z' || str[i] >= 'A' && str[i] <= 'Z')
            cnt_ch++;
    }
    float r_d = (float)cnt_n / str_len;
    float r_l = (float)cnt_ch / str_len;
    float r_o = 1.0f - r_d - r_l;
    printf("letters: %.2f, digits: %.2f, others: %.2f\n",r_l,r_d,r_o);
    return 0;
}