#include <stdio.h>

int a(int n)
{
    return n * n + 1;
}

int main()
{
    int n;
    int sum = 0;
    scanf("%d", &n);
    for (int i = 1; i <= n; i++)
        sum += a(i);
    printf("%d\n", sum);
}