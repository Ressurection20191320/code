#include <stdio.h>
#include <math.h>

int sum(int x)
{
    int sum = 0;
    while (x)
    {
        sum += x % 10;
        x /= 10;
    }
    return sum;
}

int is_prime(int x)
{
    if (x < 2)
        return 0;
    for (int i = 2; i < x; i++)
        if (x % i == 0)
            return 0;
    return 1;
}

int main()
{
    int n;
    int tn = 0;
    scanf("%d", &n);
    for (int i = 2; i <= n; i++)
    {
        if (is_prime(sum(i)))
        {
            printf("%d\n", i);
            tn++;
        }
    }
    printf("total number = %d\n", tn);
    return 0;
}