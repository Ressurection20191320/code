#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 4
#define MAX_LEN 200
enum occupied { no, yes };
typedef enum occupied Occupied;
struct info {
  float n[N];
  char s[N][MAX_LEN + 1];
  Occupied flag[N];
};
typedef struct info Info;
int global_flag = 0;

int get_24(Info* x) {
  int i, j, counter = 0, k;
  for (i = 0; i < N; i++)
    if (x->flag[i]) {
      counter++;
      k = i;
    }//到此为止counter是flag为yes的个数，k是最大的那个flag对应的下标。
  if (counter == 1) {
    //// printf("%f\n", x->n[k]);
    if (fabs(x->n[k] - 24) < 0.01) {//注意对应的是n[k]的值
      printf("%s", x->s[k]);        //此时的k正好对应的是唯一留下flag是yes的那个i
      global_flag = 1;
      return 1;
    } else
      return 0;
  }

  for (i = 0; i < N; i++)
    for (j = i + 1; j < N; j++)
      if (x->flag[i] && x->flag[j]) {//到此为止找到了两个flag为yes的，j大于i，若找不到，如flag[0]是yes，flag[3]是no,但循环后此时i已经大于0了，就到最后return 0; 会引起下一次轮的restore
        float n_i = x->n[i], n_j = x->n[j];

        char s_i[MAX_LEN + 1], s_j[MAX_LEN + 1];
        char s_t[MAX_LEN + 1];
        strcpy(s_i, x->s[i]);
        strcpy(s_j, x->s[j]);
        // printf("x->s[i]: %s\n", x->s[i]);
        // printf("x->s[j]: %s\n", x->s[j]);
        // getchar();
                                    //这几个变量是如其名，就是把x里面的对应量考出来
        char* s = "+-*/";
        for (k = 0; k < 4; k++) {   //k的0，1，2，3对应的是加减乘除
          int y, z;                 //y的作用：方向的作用。0是正运算，1是交换后再算
          int flag_break = 0;
          for (y = 0; y < 2; y++) {
            s_t[0] = s[k];
            s_t[1] = '\0';//为78行附近的strcat提供条件
            switch (s[k]) {
              case '+':
                x->n[i] = n_i + n_j;
                break;
              case '-':
                x->n[i] = y == 0 ? n_i - n_j : n_j - n_i;//
                break;
              case '*':
                x->n[i] = n_i * n_j;
                break;
              case '/':
                z = y == 0 ? fabs(n_j) < 0.01 : fabs(n_i) < 0.01;
                if (z) {
                  flag_break = 1;//分别判断正的，逆的的分母是否为0
                  break;//别混了，这个break属于switch
                }
                x->n[i] = y == 0 ? n_i * 1.0 / n_j : n_j * 1.0 / n_i;
                break;
              default:
                break;
            }
            if (flag_break) break;
            if (y == 0) {
              strcat(strcat(x->s[i], s_t), x->s[j]);
            } else {
              strcat(strcat(x->s[j], s_t), x->s[i]);
              strcpy(x->s[i], x->s[j]);
            }
            // printf("cat: %s\n", x->s[i]);

            s_t[0] = '(';
            strcpy(s_t + 1, x->s[i]);
            int s_t_len = strlen(s_t);
            s_t[s_t_len] = ')';
            s_t[s_t_len + 1] = '\0';                 //这一段的作用就是加括号
            // printf("%s\n", s_t);
            // printf("x->s[i]: %s\n", x->s[i]);
            // getchar();
            strcpy(x->s[i], s_t);
            x->flag[i] = yes;                       //保留flag[i]为yes，把其他刚与之运算的算式flag置为no，相当于是看还有那些数没有参与运算，下一次的循环就会加入他们
            x->flag[j] = no;

            if (get_24(x))
              return 1;//一旦if判断为真（当且仅当算式值为24，即第30行返回1，就一层层返回1，结束递归，否则继续循环）
            else {
              // restore info before
              x->flag[i] = yes;
              x->flag[j] = yes;
              x->n[i] = n_i;
              x->n[j] = n_j;
              strcpy(x->s[i], s_i);
              strcpy(x->s[j], s_j);
            }
          }
        }
      }
  return 0;
}
int main() {
  Info* x = (Info*)malloc(sizeof(Info));
  int k;
  char s[MAX_LEN + 1];
  for (k = 0; k < N; k++) {
    int u;
    scanf("%d", &u);
    x->n[k] = u;
    x->flag[k] = yes;
    itoa(u, s, 10);
    strcpy(x->s[k], s);
    printf("%s ", x->s[k]);
  }
  printf("\n");
  // exit(1);
  get_24(x);
  if (!global_flag) printf("cannot get 24");
}