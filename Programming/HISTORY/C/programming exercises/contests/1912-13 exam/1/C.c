#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define N 50
int scores[N];

int maxchange(int x)
{
    int d[2];
    d[0] = x / 10, d[1] = x % 10;
    if (d[0] != 0 && d[0] != 9)
        d[0] = 9;
    else
        d[1] = 9;
    return d[0] * 10 + d[1];
}

void modify_scores(int scores[], int n)
{
    int highest = 0;
    int max = 0, d;
    // your code - start
    for (int i = 0; i < n; i++)
    {
        if (maxchange(scores[i]) - scores[i] > max)
        {
            max = maxchange(scores[i]) - scores[i];
            d = i;
        }
    }
    for (int i = 0; i < n; i++)
    {
        if (i == d)
            highest += (maxchange(scores[i]));
        else
            highest += scores[i];
    }
    // your code - end
    printf("%d", highest);
}

int main()
{
    int n = 0, k;
    /* input format for {51, 47, 93} is:
    51 47 93 END */
    while (scanf("%d", &k) == 1)
    {
        scores[n++] = k;
    }
    modify_scores(scores, n);
}