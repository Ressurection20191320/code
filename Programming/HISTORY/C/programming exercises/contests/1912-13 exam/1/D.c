#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define N 50
char s[N + 1];

void smallest_first_letter(char s[])
{
    int n = strlen(s);
    char smallest = 'z';
    // your code - start
    for (int i = 0; i < n; i++)
    {
        int flag[27] = {0};
        if (s[i] < smallest)
        {
            int j;
            for (j = i; j >= 0; j--)
            {
                if (flag[s[j]-'a'] < 1)
                    flag[s[j]-'a']++;
                else
                    break;
            }
            if(j==-1)
            smallest=s[i];
        }
    }
    printf("%c", smallest);
}

int main()
{
    fgets(s, N + 1, stdin);
    int n = strlen(s);
    if (s[n - 1] == '\n')
        s[n - 1] = '\0';
    smallest_first_letter(s);
    return 0;
}