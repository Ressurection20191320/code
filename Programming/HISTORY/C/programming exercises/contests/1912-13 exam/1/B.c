#include <stdio.h>
#include <string.h>

#define N 50
char state[N + 1];

void count_interesting(char s[])
{
    int n = strlen(s), k;
    int counter = 0;
    // your code - start
    if (s[0] != s[1])
        counter++;
    if (s[n - 1] != s[n - 2])
        counter++;
    for (int i = 1; i < n - 1; i++)
    {
        if (s[i - 1] != s[i] || s[i] != s[i + 1])
            counter++;
    }
    if (n == 1)
        counter = 0;
    // your code - end
    printf("%d", counter);
}

int main()
{
    scanf("%s", state);
    count_interesting(state);
}