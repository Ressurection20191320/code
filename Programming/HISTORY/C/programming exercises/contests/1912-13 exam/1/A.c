#include <stdio.h>

int f(int x)
{
    int sum = 0;
    for (int i = 1; i < x; i++)
    {
        if (x % i == 0)
            sum += i;
    }
    return sum;
}

void check_factors_sum(int m, int n)
{
    int flag = 0;
    // your code - start
    if (f(m) == n && f(n) == m)
        flag = 1;
    // your code - end
    if (flag)
        printf("Yes!");
    else
        printf("No!");
}

int main()
{
    int m, n;
    scanf("%d %d", &m, &n);
    check_factors_sum(m, n);
    return 0;
}