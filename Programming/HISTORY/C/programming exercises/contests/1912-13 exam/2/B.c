#include <stdio.h>
#include <string.h>

#define N 50
char s[N + 1];

void str_val(char s[])
{
    int n = strlen(s), k;
    if (s[n - 1] == '\n') {
        s[n - 1] = '\0';
        n--;
    }
    int val = 0;
    // your code - start
    int cnt[26]={0};
    for(int i=0;i<n;i++)
    {
        cnt[s[i]-'a']++;
    }
    for(int i=0;i<26;i++)
    {
        val+=cnt[i]*cnt[i];
    }
    // your code - end
    printf("%d", val);
}

int main()
{
    fgets(s, N + 1, stdin);
    str_val(s);
}