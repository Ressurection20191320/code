#include <stdio.h>

int f(int n)
{
    int rlt=1;
    for(int i=2;i<=n;i++)
    {
        rlt*=i;
    }
    return rlt;
}

void sum_fact(int n)
{
    int sum = 0;

    // your code - start
    while(n)
    {
        sum+=f(n%10);
        n/=10;
    }
    // your code - end
    printf("%d", sum);
}
int main()
{
    int n;
    scanf("%d", &n);
    sum_fact(n);

    return 0;
}