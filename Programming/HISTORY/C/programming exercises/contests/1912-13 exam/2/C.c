#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define N 50
char board[N][N + 1];

void board_symmetric(char board[][N + 1], int n)
{
    int m = strlen(board[0]);
    int vertical_flag = 0, horizontal_flag = 0;
    // your code - start
    int i, j, k;
    for (i = 0; i < n; i++)
    {
        for (j = 0, k = m - 1; j < k; j++, k--)
        {
            if (board[i][j] != board[i][k])
                break;
        }
        if (j >= k)
            horizontal_flag = 1;
        else
        {
            horizontal_flag = 0;
            break;
        }
    }
    for (i = 0; i < m; i++)
    {
        for (j = 0, k = n - 1; j < k; j++, k--)
        {
            if (board[j][i] != board[k][i])
                break;
        }
        if (j >= k)
            vertical_flag = 1;
        else
        {
            vertical_flag = 0;
            break;
        }
    }
    // your code - end
    if (vertical_flag && horizontal_flag)
        printf("%s", "Both");
    else if (vertical_flag)
        printf("%s", "Vertically symmetric");
    else if (horizontal_flag)
        printf("%s", "Horizontally symmetric");
    else
        printf("Neither");
}

int main()
{
    int n, k;
    /*
    for board:
    {"#####",
     ".###.",
     "..#.."}
    type:
    3
    #####
    .###.
    ..#..
    */
    scanf("%d", &n);
    for (k = 0; k < n; k++)
        scanf("%s", board[k]);

    board_symmetric(board, n);
    return 0;
}