#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define N 50
int height[N];

void consecutive_upper(int height[], int n)
{
    int result = 0;
    // your code - start
    int l=0;
    for(int i=0;i<n-1;i++)
    {
        if(height[i]<height[i+1])
        l++;
        else
        l=0;
        result=l>result?l:result;
    }
    // your code - end
    printf("%d", result);
}

int main()
{
 /* for height { 1, 2, 3, 2, 2 }, type:
   1 2 3 2 2 END */
    int n = 0, k;
    while (scanf("%d", &k) == 1) {
        height[n++] = k;
    }
    consecutive_upper(height, n);
    return 0;
}