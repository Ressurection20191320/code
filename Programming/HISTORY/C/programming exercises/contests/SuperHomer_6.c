#include <stdio.h>

int main()
{
    int key, i, j;
    char str[1000];
    scanf("%d", &key);
    key %= 26;
    key = key >= 0 ? key : key + 26;
    getchar();
    gets(str);
    for (i = 0; str[i] == ' '; i++);
    j = i;
    for (; str[i] != '\0'; i++)
    {
        if (str[i] >= 'a' && str[i] <= 'z')
        {
            int x = str[i] - 'a';
            x += key;
            x %= 26;
            x += 'a';
            str[i] = x;
        }
    }
    for (; str[j] != '\0'; j++)
        printf("%c", str[j]);
    printf("\n");
    return 0;
}