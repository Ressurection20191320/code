#include <stdio.h>
#include <math.h>

double solve(double x);

int main()
{
    double x1, x2;
    scanf("%lf", &x2);
    do
    {
        x1 = x2;
        x2 = solve(x1);
    } while (fabs(x2 - x1) > 1e-6);
    printf("%.2lf\n", x2);
}

double solve(double x)
{
    double y = 2 * pow(x, 3) - 3 * pow(x, 2) + 3 * x - 6;
    double k = 6 * x * x - 6 * x + 3;
    x = x - y / k;
    return x;
}

