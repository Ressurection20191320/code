#include <stdio.h>

double cnt(double x)
{
    double a=3.9,y;
    y=a*x*(1-x);
    return y;
}

int main()
{
    double x0=0.11111111,x1=0.11111112;
    int n;
    scanf("%d",&n);
    for(int i=0;i<n;i++)
    {
        x0=cnt(x0);
        x1=cnt(x1);
    }
    printf("%.8lf\n",x1-x0>0?x1-x0:x0-x1);
    return 0;
}