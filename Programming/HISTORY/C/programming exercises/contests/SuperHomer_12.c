#include <stdio.h>

int main()
{
    int score[] = {5, 3, 4, 7, 3, 5, 6};
    int x = 1, flag = 0;
    int rank[7];
    int last;
    for (int i1 = 0; i1 < 10; i1++)
    {
        if (flag)
            x++;
        flag = 0;
        for (int i2 = 0; i2 < 7; i2++)
            if (score[i2] == i1)
            {
                rank[i2] = x;
                flag = 1;
            }
    }
    printf("  Player_NO  Score  Rank\n");
    for (int i = 0; i < 7; i++)
        printf("%6d%8d%7d\n", i + 1, score[i], rank[i]);
    return 0;
}