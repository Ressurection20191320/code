#include <stdio.h>

void inverse(char str[], int n, int m);

int main()
{
    int m, n;
    char str[100];
    printf("input the string:");
    gets(str);
    printf("input m,n:");
    scanf("%d,%d",&n,&m);
    inverse(str, n, m);
    printf("the inverse string:%s\n",str);
    return 0;
}

void inverse(char str[], int n, int m)
{
    m+=n-1;
    n--,m--;
    char temp;
    for(;n<m;n++,m--)
    {
        temp=str[n];
        str[n]=str[m];
        str[m]=temp;
    }
}