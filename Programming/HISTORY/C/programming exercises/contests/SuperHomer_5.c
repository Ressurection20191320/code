#include <stdio.h>

int is_prime(int n)
{
    if (n < 2)
        return 0;
    for (int i = 2; i < n; i++)
        if (n % i == 0)
            return 0;
    return 1;
}

int main()
{
    int n;
    scanf("%d", &n);
    for (int i = 3; i + 2 <= n; i++)
        if (is_prime(i) && is_prime(i + 2))
            printf("%d %d\n", i, i + 2);
    return 0;
}
