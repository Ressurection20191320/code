#include <stdio.h>
#include <string.h>
#include <math.h>
void swap(int num[], int a, int b);
void QC1(int num[], int left, int right);
void QC2(int num[], int left, int right);

int main()
{
    int n, num[4], d;
    scanf("%d", &n);
    int tmp = n;
    for (int i = 0; i < 4; i++)
    {
        num[i] = tmp % 10;
        tmp /= 10;
    }
    if (num[0] == num[1] && num[1] == num[2] && num[2] == num[3])
    {
        printf("有重复的位！\n");
        return 0;
    }
    int num1[4], num2[4], cnt = 0;
    int n1 = 0, n2 = 0;
    while (n != 6174)
    {
        for (int i = 0; i < 4; i++)
        {
            num[i] = n % 10;
            n /= 10;
        }
        for (int i = 0; i < 4; i++)
            num1[i] = num2[i] = num[i];
        QC1(num1, 0, 3), QC2(num2, 0, 3);
        d = 1;
        for (int i = 0; i < 4; i++)
        {
            n1 += num1[i] * d, n2 += num2[i] * d;
            d *= 10;
        }
        cnt++;
        n = fabs(n1 - n2);
        n1 = n2 = 0;
        memset(num, 0, sizeof(num));
    }
    printf("%d\n", cnt);
    return 0;
}

void QC1(int num[], int left, int right)
{
    int i, last;
    if (left > right)
        return;
    swap(num, left, left + (right - left) / 2);
    last = left;
    for (i = left + 1; i <= right; i++)
        if (num[i] < num[left])
            swap(num, ++last, i);
    swap(num, last, left);
    QC1(num, left, last - 1);
    QC1(num, last + 1, right);
}

void QC2(int num[], int left, int right)
{
    int i, last;
    if (left > right)
        return;
    swap(num, left, left + (right - left) / 2);
    last = left;
    for (i = left + 1; i <= right; i++)
        if (num[i] > num[left])
            swap(num, ++last, i);
    swap(num, last, left);
    QC2(num, left, last - 1);
    QC2(num, last + 1, right);
}

void swap(int num[], int a, int b)
{
    int tmp;
    tmp = num[a];
    num[a] = num[b];
    num[b] = tmp;
}