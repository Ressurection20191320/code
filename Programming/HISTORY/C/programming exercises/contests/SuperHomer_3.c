#include <stdio.h>

float an(int n)
{
    float a0 = 0.5, a1 = 0.5, an = 0;
    if (n == 0 || n == 1)
        return 0.5;
    else
    {
        for (int i = 2; i <= n; i++)
        {
            an = a0 + a1;
            a0 = a1;
            a1 = an;
        }
        return an;
    }
}

int main()
{
    int m, n;
    float sum = 0;
    scanf("%d%d", &m, &n);
    for (int i = m; i <= n; i++)
    {
        sum += an(i);
    }
    printf("%.2f ", sum);
    return 0;
}