#include <stdio.h>
#include <string.h>

int Fac(int x)
{
    if (x == 0)
        return 1;
    int rlt = 1;
    for (int i = x; i > 1; i--)
        rlt *= i;
    return rlt;
}

int main()
{
    char word[20];
    scanf("%s", &word);
    int x = 0, y = 0, rlt;
    for (int i = 0; word[i] != '\0'; i++)
    {
        if (word[i] >= 'a' && word[i] <= 'z')
            x++;
    }
    rlt = Fac(x);
    int ch[26];
    memset(ch, 0, sizeof(ch));
    for (int i = 0; i < strlen(word); i++)
    {
        ch[word[i] - 'a']++;
    }
    for (int i = 0; i < 26; i++)
    {
        rlt /= Fac(ch[i]);
    }
    printf("%d ", rlt-1);
    return 0;
}