#include <stdio.h>

int main()
{
    int n = 0, sum = 0, count = -1;
    float result;
    while (n != -1)
    {
        sum += n;
        count += scanf("%d", &n);
    }
    result = (float)sum / count;
    printf("%.2f\n", result);
    return 0;
}