#include <stdio.h>

int is_prime(int n)
{
    if (n < 2)
        return 0;
    for (int i = 2; i < n; i++)
        if (n % i==0)
            return 0;
    return 1;
}

int main()
{
    int sum = 0;
    for (int i = 500, ctr = 0; ctr < 10; i--)
    {
        if (is_prime(i))
        {
            printf("%6d", i);
            sum += i;
            ctr++;
        }
    }
    printf("\n sum=%d",sum);
    return 0;
}