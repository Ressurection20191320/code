#include <stdio.h>

int main()
{
    double spend[7];
    char flag = 0;
    for (int i = 0; i < 7; i++)
        scanf("%lf", spend + i);
    for (int i = 2; i < 7; i++)
    {
        if (spend[i] > 50 && spend[i - 1] > 50 && spend[i - 2] > 50)
            flag = 1;
    }
    if (flag)
        printf("YES\n");
    else
        printf("NO\n");
    return 0;
}