#include <stdio.h>

int main()
{
    int flag = 0, n;
    do
    {
        scanf("%d", &n);
        if (n == 5)
            flag = 1;
    } while (n != -1);
    if (flag)
        printf("have\n");
    else
        printf("not\n");
    return 0;
}