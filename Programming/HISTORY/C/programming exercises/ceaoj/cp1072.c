#include <stdio.h>

int main()
{
    int n, i, result = -2147483647;
    scanf("%d", &n);
    for (int x = 1; x <= n; x++)
    {
        for (int y = 1; y <= n; y++)
        {
            if (x == 1 || x == n)
            {
                scanf("%d", &i);
                result = i > result ? i : result;
            }
            else
            {
                if (y == 1 || y == n)
                {
                    scanf("%d", &i);
                    result = i > result ? i : result;
                }
                else
                    scanf("%d", &i);
            }
        }
    }
    printf("%d\n", result);
    return 0;
}
