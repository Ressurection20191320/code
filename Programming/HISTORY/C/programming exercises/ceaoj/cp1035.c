#include <stdio.h>

int main()
{
    int n = 0, result = 0;
    while (n != -1)
    {
        scanf("%d", &n);
        if (n % 2 == 0)
            result += 1;
    }
    printf("%d\n", result);
    return 0;
}