#include <stdio.h>
#include <string.h>

void Inverse(char str[])
{
    int left,right;
    int len=strlen(str);
    for(left=0,right=len-1;left<right;left++,right--)
    {
        char tmp=str[left];
        str[left]=str[right];
        str[right]=tmp;
    }
}


int main()
{
    char str[81];
    printf("Input a string:\n");
    gets(str);
    Inverse(str);
    printf("Inversed results:\n");
    puts(str);
    return 0;
}