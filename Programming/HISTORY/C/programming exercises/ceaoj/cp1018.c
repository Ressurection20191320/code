#include <stdio.h>

int main()
{
  int n, sum = 0;
  scanf("%d", &n);
  int tmp = n;
  for (; tmp > 0; tmp--)
  {
    if (n % tmp == 0)
      sum += tmp;
  }
  printf("%d\n", sum);
  return 0;
}