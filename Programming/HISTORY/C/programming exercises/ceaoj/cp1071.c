#include <stdio.h>

int check(int x,int y)
{
    for (int i = 1; y * i <= x * x + 1; i++)
    {
        if (x * x + 1 == y * i)
        return 1;
    }
    return 0;
}
int main()
{
    int n;
    scanf("%d", &n);
    for (int x = 1; x <= n; x++)
    {
        for (int y = x; y <= n; y++)
        {
            if (check(x, y) && check(y, x))
                printf("%d %d\n", x, y);
        }
    }
}