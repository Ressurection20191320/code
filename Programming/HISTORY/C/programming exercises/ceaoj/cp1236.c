#include <stdio.h>

int jiecheng(int n)
{
    int rlt = 1;
    for (int i = 2; i <= n; i++)
        rlt *= i;
    return rlt;
}

int main()
{
    int n;
    int sum = 0;
    scanf("%d", &n);
    for (int i = 1; i <= n; i++)
        sum += jiecheng(i);
    printf("1!+2!+...+%d! = %d\n", n, sum);
    return 0;
}