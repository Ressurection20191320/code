#include <stdio.h>

int main()
{
    int n, sum = 0;
    for (int i = 1; i <= 9; i++)
    {
        scanf("%d", &n);
        if (i == 1 || i == 3 || i == 5 || i == 7 || i == 9)
            sum += n;
    }
    printf("%d\n", sum);
    return 0;
}