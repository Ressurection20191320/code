#include <stdio.h>

int main()
{
    int n, sum = 1, x = 0, y = 1, tmp;
    scanf("%d", &n);
    if (n <= 2)
        printf("1\n");
    else if (n > 2)
    {
        for (int i = 0; i < n - 2; i++)
        {
            tmp = x + y + 5;
            sum += tmp;
            y = x;
            x = tmp;
        }
        printf("%d\n", sum);
    }
    return 0;
}