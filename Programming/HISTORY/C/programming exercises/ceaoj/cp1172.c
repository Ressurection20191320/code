#include <stdio.h>

int check(int x)
{
    int cnt = 0;
    for (int i = 1; i <= x; i++)
    {
        if (x % i == 0)
            cnt++;
    }
    return cnt;
}

int main()
{
    for (int i = 1; i <= 1000; i++)
    {
        if(check(i)==check(2*i+1))
        printf("%d\n",i);
    }
    return 0;
}