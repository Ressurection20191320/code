#include <stdio.h>

int main()
{
    int n[3][3], sum1 = 0, sum2 = -0xfffffff, rlt, x;
    for (int i1 = 0; i1 < 3; i1++)
        for (int i2 = 0; i2 < 3; i2++)
            scanf("%d", &n[i1][i2]);
    for (int i = 0; i < 3; i++)
    {
        for (x = 0, sum1 = 0; x < 3; x++)
            sum1 += n[i][x];
        if (sum1 > sum2)
        {
            rlt = i + 1;
            sum2 = sum1;
        }
    }
    printf("%d\n", rlt);
    return 0;
}