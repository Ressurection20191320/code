#include <stdio.h>

int main()
{
    int x, y;
    printf("Input x:");
    scanf("%d", &x);
    while (x != 0)
    {
        y = y * 10 + x % 10;
        x /= 10;
    }
    printf("y = %d\n", y);
    return 0;
}