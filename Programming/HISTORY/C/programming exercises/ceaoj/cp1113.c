#include <stdio.h>

int main()
{
    int count = 0;
    for (int a = 1; a < 100; a++)
    {
        for (int b = 1; b < 100; b++)
            if (a % b == 7 && 5 * b % a == 1&&a>b)
            {
                printf("%8d %8d\n", a, b);
                count++;
            }
    }
    printf("%d\n",count);
    return 0;
}