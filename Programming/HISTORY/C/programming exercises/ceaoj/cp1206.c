#include <stdio.h>

int check(int n)
{
    if (n < 2)
        return 0;
    for (int i = 2; i < n; i++)
    {
        if (n % i == 0)
            return 0;
    }
    return 1;
}

int main()
{
    int n, m, tmp, count = 0;
    scanf("%d%d", &n, &m);
    if (n > m)
    {
        tmp = n;
        n = m;
        m = tmp;
    }
    for (int i = n; i <= m; i++)
    {
        if (check(i))
            count++;
    }
    printf("%d\n",count);
    return 0;
}