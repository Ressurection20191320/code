#include <stdio.h>
int gcd(int a, int b)
{
    int c = 1;
    while (a != b)
    {
        if (a & 1)
        {
            if (b & 1)
            {
                if (a > b)
                    a = (a - b) >> 1;
                else
                    b = (b - a) >> 1;
            }
            else
                b >>= 1;
        }
        else
        {
            if (b & 1)
                a >>= 1;
            else
                c <<= 1, a >>= 1, b >>= 1;
        }
    }
    return c * a;
}

int main()
{
    int n,m;
    scanf("%d%d",&n,&m);
    printf("%d\n",gcd(n+1,m+1));
    return 0;
}