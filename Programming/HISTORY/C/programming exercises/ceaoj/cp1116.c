#include <stdio.h>
#include <string.h>
int main()
{
    char ch[256];
    int ctr1 = 0, ctr2 = 0;
    scanf("%s", &ch);
    for (int i = 0; i < strlen(ch); i++)
    {
        if (ch[i] >= 'a' && ch[i] <= 'z' || ch[i] >= 'A' && ch[i] <= 'Z')
            ctr1++;
        if (ch[i] >= '0' && ch[i] <= '9')
            ctr2++;
    }
    printf("number:%d,character:%d\n", ctr2, ctr1);
    return 0;
}