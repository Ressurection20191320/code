#include <stdio.h>

int main()
{
    int n, x, y;
    scanf("%d", &n);
    for (int i = 1; i <= 2 * n - 1; i++)
    {
        x = i - 1 < n ? i - 1 : 2 * n - 1 - i;
        for (int j = 0; j < x; j++)
            printf(" ");
        y = 2 * n + 1 - 2 * i > 0 ? 2 * n + 1 - 2 * i : -(2 * n + 1 - 2 * i) + 2;
        for (int j = 0; j < y; j++)
            printf("%d", n + 1 - i > 0 ? n + 1 - i : i - n + 1);
        printf("\n");
    }

    return 0;
}