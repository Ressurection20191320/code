#include <stdio.h>

int check(int n);

int main()
{
    int n;
    for (n = 1; n <= 500; n++)
        if (check(n))
            printf("%d\n", n);
    return 0;
}

int check(int n)
{
    int sum = 0;
    for (int i = 1; i < n; i++)
    {
        if (n % i == 0)
            sum += i;
    }
    if (sum == n)
        return 1;
    return 0;
}