#include <stdio.h>

int main()
{
    int n, x, ctr = 0;
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &x);
        if (x % 2 != 0)
            ctr++;
    }
    printf("%d\n",ctr);
    return 0;
}