//PAC

#include <stdio.h>
#include <string.h>

int main()
{
    int n[22][22], x, ctr = 0;
    memset(n, 0, sizeof(n));
    scanf("%d", &x);
    for (int i = 1; i <= x; i++)
        for (int j = 1; j <= x; j++)
        {
            scanf("%d", &n[i][j]);
        }
    for (int i = 1; i <= x; i++)
        for (int j = 1; j <= x; j++)
        {
            if (n[i][j] >= n[i - 1][j - 1] && n[i][j] >= n[i - 1][j + 1] && n[i][j] >= n[i + 1][j - 1] && n[i][j] >= n[i + 1][j + 1] && n[i][j] >= n[i - 1][j] && n[i][j] >= n[i][j - 1] && n[i][j] >= n[i + 1][j] && n[i][j] >= n[i][j + 1])
                ctr++;
        }
    printf("%d", ctr);
    return 0;
}