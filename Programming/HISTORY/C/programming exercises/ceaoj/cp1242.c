#include <stdio.h>
void inverse(char str[], int m, int n)
{
    int i, j;
    char temp;
    for (i = m - 1, j = n + m - 2; i<j; i++, j--)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
    }
}
int main()
{
    char str[1000];
    int m, n;
    printf("input the string:");
    gets(str);
    printf("input m,n:");
    scanf("%d,%d", &m, &n);
    inverse(str, m, n);
    printf("the inverse string:%s", str);
    return 0;
    // printf("\n");
}