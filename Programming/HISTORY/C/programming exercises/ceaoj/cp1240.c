#include <stdio.h>
#include <string.h>

void inverse_string(char str[],int len)
{
    int left,right;
    for(left=0,right=len-1;left<right;left++,right--)
    {
        char tmp=str[left];
        str[left]=str[right];
        str[right]=tmp;
    }
}

int main()
{
    printf("Input a string:\n");
    char str[128];
    scanf("%s",str);
    inverse_string(str,strlen(str));
    printf("Inversed results:\n%s\n",str);
    return 0;
}