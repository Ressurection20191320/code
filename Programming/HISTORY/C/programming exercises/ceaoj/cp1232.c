#include <stdio.h>

int main()
{
    int n=1, m=2,tmp;
    float sum = 0;
    for (int i = 0; i < 20; i++)
    {
        sum += (float)m / n;
        tmp=n+m;
        n=m;
        m=tmp;
    }
    printf("sum=%9.6f\n", sum);
    return 0;
}