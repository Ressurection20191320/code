#include <stdio.h>

int main()
{
    int num[10001];
    for (int i = 0; i < 10001; i++)
        num[i] = i;
    for (int i = 2; i < 101; i++)
    {
        if (num[i] == 0)
            continue;
        for (int j = i + 1; j < 10001; j++)
        {
            if (num[j] == 0)
                continue;
            num[j] = num[j] % num[i] == 0 ? 0 : num[j];
        }
    }
    int ctr = 0;
    for (int i = 2; i < 8849; i++)
    {
        if (8848 % i == 0 && num[i])
            ctr++;
    }
    printf("%d",ctr);
    return 0;
}