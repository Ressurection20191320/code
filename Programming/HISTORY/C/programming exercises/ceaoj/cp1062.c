#include <stdio.h>

int check(int n)
{
    for (int i = 2; i < n; i++)
    {
        if (n % i == 0)
            return 0;
    }
    return 1;
}

int max(int n)
{
    int tmp = n;
    while (1)
    {
        if (tmp % n == 0 && check(n))
            return n;
        n--;
    }
}

int main()
{
    int a, b;
    while (1)
    {
        scanf("%d%d", &a, &b);
        if (a == 0 && b == 0)
            break;
        else if (max(a) > max(b))
            printf("A\n");
        else if (max(a) < max(b))
            printf("B\n");
        else
            printf("AB\n");
    }
    return 0;
}