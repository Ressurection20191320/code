#include <stdio.h>

int check(int n, int i)
{
    if (n % i == 0)
        return 1;
    else
        return 0;
}

int count1(int n)
{
    int count = 0;
    while (n != 0)
    {
        if (n % 10 == 1)
        {
            count++;
        }
        n /= 10;
    }
    return count;
}

int main()
{
    int n, result, count = 0;
    scanf("%d", &n);
    for (int tmp = n; tmp != 0; tmp--)
    {
        if (check(n, tmp))
        {
            if (count1(tmp) >= count)
            {
                count = count1(tmp);
                result = tmp;
            }
        }
    }
    printf("%d\n", result);
    return 0;
}