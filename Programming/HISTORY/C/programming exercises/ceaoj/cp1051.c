#include <stdio.h>
#include <string.h>

int main()
{
    int n, count[10], flag = 0;
    scanf("%d", &n);
    for (int i = 0; i < 10; i++)
        count[i] = 0;
    while (n != 0)
    {
        count[n % 10]++;
        n /= 10;
    }
    for (int i = 0; i < 10; i++)
    {
        if (count[i] > 1)
        {
            printf("%d\n", i);
            flag = 1;
        }
    }
    if (flag == 0)
        printf("NONE\n");
    return 0;
}