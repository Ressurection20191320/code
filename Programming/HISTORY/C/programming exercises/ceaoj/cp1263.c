#include <stdio.h>

int main()
{
    int num[101];
    for (int i = 0; i < 101; i++)
        num[i] = i;
    for (int i = 2; i < 11; i++)
    {
        for (int j = i + 1; j < 101; j++)
            if (j % i==0)
                num[j] = 0;
    }
    int k = 1;
    while (k < 101-5)
    {
        for (int i = 0; i < 5; i++)
        {
            while (num[++k] == 0)
                ;
            printf("%3d", k);
        }
        printf("\n");
    }
}