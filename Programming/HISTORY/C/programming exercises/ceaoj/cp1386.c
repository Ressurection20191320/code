#include <stdio.h>
#include <math.h>

int check(int n)
{
    for (int i = 2; i < n; i++)
    {
        if (n % i == 0)
            return 0;
    }
    return 1;
}

int main()
{
    int n;
    scanf("%d", &n);
    for (int i = 0; pow(2, i) < n; i++)
    {
        if (check(n - pow(2, i)))
        {
            int t = n - pow(2, i);
            printf("%d %d", t, i);
            return 0;
        }
    }
    printf("NO");
    return 0;
}