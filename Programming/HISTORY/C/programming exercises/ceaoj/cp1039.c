#include <stdio.h>

int main()
{
    int n, m;
    scanf("%d%d", &n, &m);
    for (int i = 0; i < n; i++)
    {
        for (int x = 0; x < m; x++)
        {
            printf("*");
        }
        printf("\n");
    }
    return 0;
}