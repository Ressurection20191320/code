#include <stdio.h>

int check(int i)
{
    for (int n = 2; n < i; n++)
        if (i % n == 0)
            return 0;
    return i;
}

int main()
{
    int n, sum;
    scanf("%d", &n);
    for (int i = 2; i <= n; i++)
    {
        sum += check(i);
    }
    printf("%d\n", sum);
    return 0;
}