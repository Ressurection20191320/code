#include <stdio.h>
int main()
{
    int h, m, s;
    scanf("%d%d%d", &h, &m, &s);
    printf("%d\n", 3600 * h + 60 * m + s);
    return 0;
}