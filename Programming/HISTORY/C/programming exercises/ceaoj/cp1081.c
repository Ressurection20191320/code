#include <stdio.h>

int main()
{
    int n, m, x[10][10];
    scanf("%d%d", &n, &m);
    for (int i1 = 0; i1 < n; i1++)
    {
        for (int i2 = 0; i2 < m; i2++)
        {
            scanf("%d", &x[i1][i2]);
        }
    }
    for (int i1 = 0; i1 < n; i1++)
    {
        for (int i2 = m - 1; i2 >= 0; i2--)
        {
            if (i2 != 0)
                printf("%d ", x[i1][i2]);
            else
                printf("%d", x[i1][i2]);
        }
        printf("\n");
    }
    return 0;
}