#include <stdio.h>

int sum(int n);

int main()
{
    for (int a = 1; a <= 1000; a++)
    {
        for (int b = 1; b <= 1000; b++)
        {
            if (a * a * a + 1 == b && a * a + 1 == sum(b))
                printf("%d %d\n", a, b);
        }
    }
}

int sum(int n)
{
    int sum=0;
    while(n!=0)
    {
        sum+=n%10;
        n/=10;
    }
    return sum;
}