#include <stdio.h>

int judge(int n)
{
    if (n < 2)
        return 0;
    for (int i = 2; i < n; i++)
        if (n % i == 0)
            return 0;
    return 1;
}

int main()
{
    int n, m, tmp;
    scanf("%d", &n);
    tmp = n;
    m = n;
    while (1)
    {
        if (judge(n++))
        {
            printf("%d\n", n - tmp - 1);
            return 0;
        }
        if (judge(m--))
        {
            printf("%d\n", tmp - m - 1);
            return 0;
        }
    }
}