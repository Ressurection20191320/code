#include <stdio.h>
int main()
{
    char tmp;
    int num[100], i = 0;
    for(int i=0;i<100;i++)
        num[i]=-1;
    printf("Please enter a string:");
    while (1)
    {
        tmp = getchar();
        if (tmp >= '0' && tmp <= '9')
        {
            ungetc(tmp,stdin);
            scanf("%d", &num[i++]);
        }
        else if (tmp == '\n')
            break;
    }
    printf("the result of output:\n");
    for (int i = 0; num[i] != -1; i++)
        printf("%10d\n", num[i]);
    return 0;
}