#include <stdio.h>

int main()
{
    int n, sum = 1, x = 0, y = 1, tmp;
    scanf("%d", &n);
    if (n == 1)
        printf("1\n");
    else if (n == 2)
        printf("0\n");
    else
    {
        for (int i = 0; i < n - 2; i++)
        {
            tmp = 3 * x + 2 * y + 5;
            y = x;
            x = tmp;
        }
        printf("%d\n", x);
    }
    return 0;
}