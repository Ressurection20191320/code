#include <stdio.h>
#include <math.h>
int factorail(int n)
{
    int result = 1;
    for (int i = 2; i < n + 1; i++)
    {
        result *= i;
        while (result > 10000000)
            result -= 10000000;
    }
    return result;
}

int main()
{
    int n, m, result = 0;
    scanf("%d%d", &n, &m);
    n = factorail(n);
    for (int i = 0; i < m; i++)
    {
        result += (n % 10) * pow(10, i);
        n /= 10;
    }
    printf("%d", result);
    return 0;
}