#include <stdio.h>
int main()
{
    int n, l;
    int v[100]={0};
    scanf("%d", &n);
    l = 0;
    while(n)
    {
        v[l++] = n%3;
        n/=3;
    }
    while(l--)
    {
        printf("%d", v[l]);
    }
    return 0;
}