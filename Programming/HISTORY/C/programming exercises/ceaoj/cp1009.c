#include <stdio.h>

int check(int i)
{
    for (int n = 2; n < i; n++)
    {
        if (i % n == 0)
        {
            return 0;
        }
    }
    return 1;
}

int main()
{
    int n, tmp;
    scanf("%d", &n);
    n = n * n + 1;
    tmp = n;
    while (1)
    {
        if (n % tmp == 0)
        {
            if (check(tmp))
            {
                printf("%d\n", tmp);
                return 0;
            }
        }
        tmp--;
    }
}