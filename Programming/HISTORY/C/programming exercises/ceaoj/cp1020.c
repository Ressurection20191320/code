#include <stdio.h>

int check(int i)
{
    for (int n = 2; n < i; n++)
    {
        if (i % n == 0)
        {
            return 0;
        }
    }
    return 1;
}

int reverse(int i)
{
    int n = 0;
    while (i > 0)
    {
        n *= 10;
        n += i % 10;
        i /= 10;
    }
    return n;
}

int main()
{
    int n;
    scanf("%d", &n);
    for (int i = 2; i < n + 1; i++)
    {
        if (check(i) && check(reverse(i)))
            printf("%d\n", i);
    }
    return 0;
}