#include <stdio.h>
#include <math.h>

int IsPrime(int n)
{
    if (n < 2)
        return 0;
    for (int i = 2; i < n; i++)
        if (n % i == 0)
            return 0;
    return 1;
}

int main()
{
    int n, x = 1, i;
    scanf("%d", &n);
    if (n > 9)
    {
        int a, b;
        b = n % 10;
        for (i = 1; x != 0; i++)
        {
            a = n / pow(10, i);
            x = n / pow(10, i + 1);
        }
        n = n - (int)pow(10, i - 1) * a - b + b * (int)pow(10, i - 1) + a;
    }
    if (IsPrime(n))
        printf("Yes\n");
    else
        printf("No\n");
    return 0;
}