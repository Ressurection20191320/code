#include <stdio.h>

int main()
{
    float n[5];
    for (int i = 0; i < 5; i++)
        scanf("%f", &n[i]);
    float min = n[0], max = n[0];
    for (int i = 1; i < 5; i++)
    {
        min = n[i] < min ? n[i] : min;
        max = n[i] > max ? n[i] : max;
    }
    float sum = 0;
    for (int i = 0; i < 5; i++)
        sum += n[i];
    sum = sum - min - max;
    printf("%.2f\n",sum/3);
    return 0;
}