#include <stdio.h>
#include <string.h>

int main()
{
    int n, k = -1;
    scanf("%d", &n);
    char num[500];
    memset(num, 1, sizeof(num));
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            k++;
            k %= n;
            while (num[k] == 0)
            {
                k++;
                k %= n;
            }
        }
        num[k] = 0;
    }
    int i = 0;
    while (num[i++] == 0)
        ;
    printf("%d", i);
    return 0;
}