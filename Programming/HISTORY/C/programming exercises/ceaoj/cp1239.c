#include <stdio.h>
#include <string.h>

void reverse_string(char *str,int len)
{
    int left,right;
    for(left=0,right=len-1;left<right;left++,right--)
    {
        char tmp=*(str+left);
        *(str+left)=*(str+right);
        *(str+right)=tmp;
    }
}

int main()
{
    printf("Enter string:");
    char str[128];
    scanf("%s",str);
    reverse_string(str,strlen(str));
    printf("a=%s\n",str);
    return 0;
}