#include <stdio.h>

int check(int i)
{
    for (int n = 2; n < i; n++)
    {
        if (i % n == 0)
        {
            return 0;
        }
    }
    return 1;
}

int check123(int num)
{
    int s = num, y = 0;

    while (s > 0)
    {

        y = y * 10 + s % 10;

        s = s / 10;
    }

    if (y == num)
    {

        return 1;
    }
    else
    {

        return 0;
    }
}

int main()
{
    int n, i = 2;
    scanf("%d", &n);
    while (i < n + 1)
    {
        if (check(i) && check123(i))
            printf("%d\n", i);
        i++;
    }
    return 0;
}