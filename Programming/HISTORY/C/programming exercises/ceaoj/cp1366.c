#include <stdio.h>

int check(int n)
{
    for (int i = 2; i < n; i++)
    {
        if (n % i == 0)
            return 0;
    }
    return 1;
}

int check123(int n)
{
    int f1 = 0, f2 = 0, f3 = 0;
    while (n != 0)
    {
        if (n % 10 == 1)
            f1++;
        else if (n % 10 == 2)
            f2++;
        else if (n % 10 == 3)
            f3++;
        n /= 10;
    }
    if (f1 != 0 && f2 != 0 && f3 != 0)
        return 1;
    else
        return 0;
}

int main()
{
    for (int i = 1000; i < 10000; i++)
    {
        if(check(i)&&check123(i))
            printf("%d\n",i);
    }
    return 0;
}