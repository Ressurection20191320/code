#include <stdio.h>

int main()
{
    int num[101];
    for (int i = 0; i < 101; i++)
        num[i] = i;
    for (int i = 2; i < 100; i++)
    {
        if (num[i] == 0)
            continue;
        for (int j = i + 1; j < 101; j++)
        {
            if (num[j] % num[i] == 0)
                num[j] = 0;
        }
    }
    for(int i=3;i<98;i++)
        if(num[i]&&num[i+2])
        printf("%4d/%d",i,i+2);
    return 0;
}