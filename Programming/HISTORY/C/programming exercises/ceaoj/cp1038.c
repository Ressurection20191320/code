#include <stdio.h>
#include <math.h>
int main()
{
    int n = 0, result = 2147483647, flag = 0, tmp = 0;
    while (n != -1)
    {
        scanf("%d", &n);
        if (n != -1)
        {
            if (!flag)
            {
                tmp = n;
                flag = 1;
            }
            else if (result > fabs(n - tmp))
            {
                result = fabs(n - tmp);
            }
            tmp = n;
        }
    }
    printf("%d\n", result);
    return 0;
}