#include <stdio.h>

void Myitoa(int n)
{
    int i;
    if ((i = n / 10) != 0)
        Myitoa(i);
    putchar(n % 10 + '0');
}

int main()
{
    int n;
    char str[20] = {0};
    scanf("%d", &n);
    Myitoa(n);
    return 0;
}