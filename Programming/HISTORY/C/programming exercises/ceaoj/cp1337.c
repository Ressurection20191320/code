#include <stdio.h>

int main()
{
    float v1, v2, t;
    scanf("%f%f", &v1, &v2);
    t = 400.0F / (v1 + v2);
    if (t - (int)t >= 0.5F)
        t++;
    printf("%d\n",(int)t);
    return 0;
}