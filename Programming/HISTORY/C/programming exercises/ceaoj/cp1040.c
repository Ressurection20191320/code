#include <stdio.h>

int main()
{
    int n, m;
    scanf("%d%d", &n, &m);
    for (int i = 0; i < n; i++)
    {
        if (i == 0 || i == n - 1)
        {
            for (int x = 0; x < m; x++)
            {
                printf("*");
            }
        }
        else
        {
            for (int x = 0; x < m; x++)
            {
                if (x == 0 || x == m - 1)
                    printf("*");
                else
                    printf(" ");
            }
        }

        printf("\n");
    }
    return 0;
}