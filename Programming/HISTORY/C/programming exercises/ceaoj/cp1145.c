#include <stdio.h>

int main()
{
    int a, b, result;
    char ch;
    scanf(" %d", &a);
    scanf(" %c", &ch);
    scanf(" %d", &b);
    switch (ch)
    {
    case '+':
        result = a + b;
        break;
    case '-':
        result = a - b;
        break;
    case '*':
        result = a * b;
        break;
    default:
        break;
    }
    printf("%d", result);
    return 0;
}