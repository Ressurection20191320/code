#include <stdio.h>

int check(int i)
{
    if (i < 2)
        return 0;
    for (int x = 2; x < i; x++)
    {
        if (i % x == 0)
        {
            return 0;
        }
    }
    return 1;
}

int main()
{
    int n = -2, result = -1;
    do
    {
        if (check(n) && n > result)
        {
            result = n;
        }
        scanf("%d", &n);
    } while (n != -1);
    printf("%d\n", result);
    return 0;
}