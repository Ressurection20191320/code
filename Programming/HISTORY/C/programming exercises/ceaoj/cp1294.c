#include <stdio.h>
#include <math.h>
int main()
{
    int t;
    scanf("%d", &t);
    for (int n = 1; n < t; n++)
    {
        for (int m = 1; m < t; m++)
            if (pow(n, 2) - 2 * pow(m , 2) == 1)
                printf("%d %d\n", n, m);
    }
    return 0;
}