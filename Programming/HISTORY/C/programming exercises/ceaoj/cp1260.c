#include <stdio.h>

int judge(int n)
{
    int num[4];
    for (int i = 4; n; i--)
    {
        num[i] = n % 10;
        n /= 10;
    }
    if (num[1] == num[2] && num[3] == num[4] && num[1] != num[3])
        return 1;
    return 0;
}

int main()
{
    for (int i = 32;i<=88; i++)
    {
        if (judge(i*i))
        {
            printf("The number is:%d\n", i*i);
            return 0;
        }
    }
}