#include <stdio.h>

int main()
{
    printf("This program sums a series of integers.\nEnter integers (0 to terminate):");
    int n, sum = 0;
    do
    {
        scanf("%d", &n);
        sum += n;
    } while (n != 0);
    printf("The sum is: %d\n", sum);
    return 0;
}