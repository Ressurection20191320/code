#include <stdio.h>

int main()
{
    int n, i = 0, j, t;
    char N[9];
    scanf("%d", &n);
    n *= n;
    while (1)
    {
        if (n != 0)
        {
            N[i] = n % 10 + 48;
            i++;
            n /= 10;
        }
        else
        {
            N[i] = '\0';
            break;
        }
    }
    for (j = 0; j < 8; j++)
        for (i = 0; i < 8 - j; i++)
            if (N[i] > N[i + 1])
            {
                t = N[i];
                N[i] = N[i + 1];
                N[i + 1] = t;
            }
    for (int j = 0; j < 9; j++)
    {
        if (N[j] > 47 && N[j] < 58)
            printf("%c\n", N[j]);
    }
    return 0;
}