#include <stdio.h>
#include <string.h>

typedef enum bool
{
    false,
    true
}Bool;

Bool is_lowcharacter(char ch)
{
    if (ch >= 'a' && ch <= 'z')
        return true;
    return false;
}

Bool is_highcharacter(char ch)
{
    if (ch >= 'A' && ch <= 'Z')
        return true;
    return false;
}

Bool is_character(char ch)
{
    if (ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z')
        return true;
    return false;
}

int main()
{
    char s[81];
    gets(s);
    int strlen_s = strlen(s);
    for (int i = 0; i < strlen_s; i++)
    {
        
        if(is_character(s[i]))
        {
            if(is_lowcharacter(s[i]))
            s[i]+='A'-'a';
            while(is_character(s[++i]));
        }
    }
    puts(s);
    return 0;
}