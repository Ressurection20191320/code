#include <stdio.h>
#include <math.h>

int main()
{
    int num[101], cnt = 0, min = 0b01111111111111111111111111111111, max = 0;
    num[1] = 0;
    for (int i = 1; num[i - 1] != -1; i++)
    {
        cnt += scanf("%d", &num[i]);
    }
    cnt--;
    for (int i = 1; i < cnt; i++)
    {
        min = fabs(num[i] - num[i + 1]) < min ? fabs(num[i] - num[i + 1]) : min;
        max = fabs(num[i] - num[i + 1]) > max ? fabs(num[i] - num[i + 1]) : max;
    }
    printf("min diff=%d, max diff=%d\n",min,max);
    return 0;
}