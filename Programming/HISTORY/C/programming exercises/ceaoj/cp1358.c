#include <stdio.h>

int main()
{
    int n, m, sum = 0, tmp;
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= m; i++)
    {
        for (int p = 0; p < i; p++)
        {
            if (p == 0)
                tmp = n;
            sum += tmp;
            tmp *= 10;
        }
    }
    printf("%d\n", sum);
    return 0;
}