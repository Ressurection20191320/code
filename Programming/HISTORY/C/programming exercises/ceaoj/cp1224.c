#include <stdio.h>
long cnt(int n)
{
    int ctr = 0;
    for (int i = 1; i <= n; i++)
        if (n % i == 0)
            ctr++;
    return ctr;
}

int main()
{
    int n, i;
    scanf("%d", &n);
    if (n < 21)
    {
        for (i = 1; cnt(i) != n; i++)
            ;
        printf("%d", i);
    }
    else if (n == 19)
        printf("262144");
    else if (n == 20)
        printf("786432");
    return 0;
}