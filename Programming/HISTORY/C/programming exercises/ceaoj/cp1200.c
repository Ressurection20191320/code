#include <stdio.h>

int main()
{
    int ch=getchar();
    if (ch>=48&&ch<=57)
        printf("Number");
    else if (ch>=65&&ch<=90||ch>=97&&ch<=122)
        printf("Character");
    else
        printf("Not character or number");
    return 0;
}