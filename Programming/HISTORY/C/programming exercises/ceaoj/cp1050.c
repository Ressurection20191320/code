#include <stdio.h>
#include <string.h>

int main()
{
    char str[80];
    int count = 0, word35 = 0;
    scanf("%[^\n]", &str);
    for (int i = 0; i < strlen(str) + 1; i++)
    {
        while (str[i] > 'a' - 1 && str[i] < 'z' + 1 || str[i] > 'A' - 1 && str[i] < 'Z' + 1)
        {
            i++;
            count++;
        }
        if (count > 2 && count < 6)
        {
            word35++;
        }
        count = 0;
    }
    printf("%d\n", word35);
    return 0;
}