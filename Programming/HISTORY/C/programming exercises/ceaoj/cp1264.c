#include <stdio.h>
#include <math.h>

double solve(double x);

int main()
{
    double x1, x2;
    x2 = 1.5;
    do
    {
        x1 = x2;
        x2 = solve(x1);
    } while (fabs(x2 - x1) > 1e-6);
    printf("%6.2lf\n", x2);
    return 0;
}

double solve(double x)
{
    double y = 2 * pow(x, 3) - 4 * pow(x, 2) + 3 * x - 6;
    double k = 6 * x * x - 8 * x + 3;
    x = x - y / k;
    return x;
}
