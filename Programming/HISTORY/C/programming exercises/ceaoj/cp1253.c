#include <stdio.h>

int main()
{
    int flag;
    for (int x = 1;; x++)
    {
        flag = 1;
        int i = x;
        for (int k = 0; k < 5; k++)
        {
            if (i % 5 != 1 || i / 5 < 1)
            {
                flag = 0;
                break;
            }
            i -= 1;
            i = i / 5 * 4;
        }
        if (flag)
        {
            printf("Total number of fish catched=%d\n", x);
            return 0;
        }
    }
}