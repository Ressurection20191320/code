#include <stdio.h>

int main()
{
    int n = 0, result = -2114125311;
    while (n != -1)
    {
        scanf("%d", &n);
        if (n > result)
            result = n;
    }
    printf("%d\n", result);
    return 0;
}