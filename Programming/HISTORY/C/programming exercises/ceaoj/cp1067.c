#include <stdio.h>
#include <string.h>
int main()
{
    char ch[1000], c1[500], c2[500];
    int n;
    scanf("%s", &ch);
    scanf("%d", &n);
    int len = strlen(ch);
    for (int i = 0; i < n; i++)
    {
        for (int i3 = 0; i3 < len / 2; i3++)
            c1[i3] = ch[i3];
        for (int i4 = len / 2; i4 < len; i4++)
            c2[i4 - len / 2] = ch[i4];
        for (int i1 = 0; i1 < len; i1 += 2)
            ch[i1] = c1[i1 / 2];
        for (int i2 = 0; i2 < len; i2 += 2)
            ch[i2 + 1] = c2[i2 / 2];
    }
    printf("%s\n", ch);
    return 0;
}