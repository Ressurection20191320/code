#include <stdio.h>

int main()
{
    int n, temp, sum = 0;
    scanf("%d", &n);
    temp = n;
    for (int i = 0; i < temp; i++)
    {
        sum += n * n * n;
        --n;
    }

    printf("%d\n", sum);
}