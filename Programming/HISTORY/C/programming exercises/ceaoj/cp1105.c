#include <stdio.h>
int main()
{
    char num_c[9];
    for (int i = 0; i < 9; i++)
    {
        num_c[i] = getchar();
        if (num_c[i] == '-')
            i--;
    }
    getchar();
    int n;
    scanf("%d", &n);
    int num[9];
    for (int i = 0; i < 9; i++)
    {
        num[i] = num_c[i] - '0';
    }
    int sum = 0;
    for (int i = 1; i < 10; i++)
    {
        sum += num[i - 1] * i;
        sum %= 11;
    }
    int crt = sum % 11;
    if (crt == n)
        printf("Right\n");
    else
    {
        int i = 0;
        putchar(num_c[i++]);
        putchar('-');
        putchar(num_c[i++]);
        putchar(num_c[i++]);
        putchar(num_c[i++]);
        putchar('-');
        putchar(num_c[i++]);
        putchar(num_c[i++]);
        putchar(num_c[i++]);
        putchar(num_c[i++]);
        putchar(num_c[i++]);
        putchar('-');
        if(crt==10)
        putchar('X');
        else
        printf("%d", crt);
    }
    return 0;
}