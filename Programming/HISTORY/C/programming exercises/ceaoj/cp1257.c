#include <stdio.h>

int main()
{
    int n;
    scanf("%d", &n);
    int d[3];
    for (int i = 0; i < 3; i++)
    {
        d[i] = n % 10;
        n /= 10;
    }
    printf("b2=%d, b1=%d, b0=%d, sum=%d\n",d[2],d[1],d[0],d[0]+d[1]+d[2]);
    return 0;
}