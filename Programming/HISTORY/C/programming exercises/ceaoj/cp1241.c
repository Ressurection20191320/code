#include <stdio.h>

int Mystrlen(char str[])
{
    int i;
    for(i=0;str[i]!='\0';i++);
    return i;
}

int main()
{
    char str[256];
    printf("Enter a string:");
    gets(str);
    printf("The length of the string is:%d\n",Mystrlen(str));
    return 0;
}