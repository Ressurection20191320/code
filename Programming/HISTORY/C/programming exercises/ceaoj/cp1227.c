#include <stdio.h>

long jiecheng(int n)
{
    int rlt = 1;
    for (int i = 2; i <= n; i++)
        rlt *= i;
    return rlt;
}

int main()
{
    for (int i = 1; i <= 5; i++)
        printf("%4d!=%ld", i, jiecheng(i));
    printf("\n");
    for (int i = 6; i <= 10; i++)
        printf("%4d!=%ld", i, jiecheng(i));
    return 0;
}