#include <stdio.h>
#include <string.h>

int main()
{
    char str[80];
    scanf("%[^\n]", str);
    if (str[0] > ('a' - 1) && str[0] < ('z' + 1))
        printf("%c", str[0] -= 32);
    else if (str[0] > ('A' - 1) && str[0] < ('Z' + 1))
        printf("%c", str[0]);
    for (int i = 1; i < strlen(str); i++)
    {
        if (str[i] == ' ' && str[i + 1] > ('a' - 1) && str[i + 1] < ('z' + 1))
            printf("%c", str[i + 1] -= 32);
        else if (str[i] == ' ' && str[i + 1] > ('A' - 1) && str[i + 1] < ('Z' + 1))
            printf("%c", str[i + 1]);
    }
    printf("\n");
    return 0;
}