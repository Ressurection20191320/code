#include <stdio.h>

int check(int i)
{
    for (int n = 2; n < i; n++)
    {
        if (i % n == 0)
        {
            return 0;
        }
    }
    return 1;
}

int main()
{
    int n, tmp, dif = 0;
    scanf("%d", &n);
    tmp = n;
    while (n > 0)
    {
        if (check(n))
        {
            if (tmp - n > dif)
                dif = tmp - n;
            tmp = n;
        }
        n--;
    }
    printf("%d\n", dif);
    return 0;
}