#include <stdio.h>

int main()
{
    long sum = 0;
    for (int i = 1; i <= 99; i += 2)
        sum += i * (i + 1) * (i + 2);
    printf("sum = %ld\n", sum);
    return 0;
}