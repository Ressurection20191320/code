#include <stdio.h>
#include <math.h>
#include <string.h>
int is_prime(int n)
{
    if (n < 2)
        return 0;
    for (int i = 2; i < n; i++)
    {
        if (n % i == 0)
            return 0;
    }
    return 1;
}

int is_huiwen(int n)
{
    char digit[13];
    int i = 0;
    memset(digit, 0, sizeof(digit));
    for (int d = 1; n; d *= 10, i++)
    {
        digit[i] = n % 10;
        n /= 10;
    }
    for(int a=0,b=i-1;a<b;a++,b--)
    {
        if(digit[a]!=digit[b])
        return 0;
    }
    return 1;    
}

int main()
{
    for (int i = 100; i < 1000; i++)
    {
        if (is_huiwen(i) && is_prime(i))
        printf("%d\n",i);
    }
    return 0;
}