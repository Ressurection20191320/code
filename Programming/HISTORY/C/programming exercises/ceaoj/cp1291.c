#include <stdio.h>

int main()
{
    int n, result;
    scanf("%d", &n);
    result += n / 7 * 9;
    n %= 7;
    if (n < 6)
        result += n;
    else if (n == 6)
        result += 7;
    else
        result += 9;
    printf("%d\n",result);
    return 0;
}