#include <stdio.h>

int min_digit(int x)
{
    int result = 9;
    while (x)
    {
        int d = x % 10;
        result = d < result ? d : result;
        x /= 10;
    }
    return result;
}

int main()
{
    int n;
    int result;
    int max = 0;
    do
    {
        scanf("%d", &n);
        int maxdigit = max_digit(n);
        if (maxdigit > max && n != -1)
        {
            result = n;
            max = maxdigit;
        }
    } while (n != -1);
    printf("%d", result);
    return 0;
}