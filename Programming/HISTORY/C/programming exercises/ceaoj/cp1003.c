#include <stdio.h>

int jiecheng(int i)
{
    int tmp, sum = 1;
    tmp = i;
    for (int x = 0; x < tmp; x++)
    {
        sum *= i;
        --i;
    }
    return sum;
}

int main()
{
    int n, sum = 0, temp;
    scanf("%d", &n);
    temp = n;
    for (int i = 0; i < temp; i++)
        if (i % 2 == 0)
        {
            sum += jiecheng(n);
            --n;
        }
        else
        {
            sum -= jiecheng(n);
            --n;
        }
    printf("%d\n", sum);
}