#include <stdio.h>
#include <math.h>
#define PI 3.141593

int main()
{
    for (int i=0;i<=360;i+=10)
    printf("%d %.5f\n",i,sin((double)i/360*2*PI));
    return 0;
}