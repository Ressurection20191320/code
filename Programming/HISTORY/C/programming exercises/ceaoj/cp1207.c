#include <stdio.h>

void sort(int num[], int n)
{
    for (int k = 1; k < n; k++)
    {
        int tmp = num[k];
        int i;
        for (i = k - 1; i >= 0 && num[i] > tmp; i--)
        {
            num[i + 1] = num[i];
        }
        num[i + 1] = tmp;
    }
}
int main()
{
    int n;
    int num[10086];
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &num[i]);
    }
    sort(num, n);
    for (int i = 0; i < n - 1; i++)
    {
        printf("%d ", num[i]);
    }
    printf("%d", num[n - 1]);
    return 0;
}
