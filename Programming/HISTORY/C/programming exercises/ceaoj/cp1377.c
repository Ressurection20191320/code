#include <stdio.h>
#define MAXDIGIT 1000
int main()
{
    int n;
    int a[MAXDIGIT];
    int digit = 1; //已有位数
    int temp;
    int i, j, carry; //carry：进位

    scanf("%d", &n);
    a[0] = 1;

    for (i = 2; i <= n; i++)
    {
        for (j = 1, carry = 0; j <= digit; j++)
        {
            temp = a[j - 1] * i + carry; //相应阶乘中的一项与当前所得临时结果的某位相乘（加上进位）
            a[j - 1] = temp % 10000;     //更新临时结果的位上信息
            carry = temp / 10000;        //看是否有进位
        }
        while (carry)
        {
            a[digit++] = carry % 10000; //新加一位
            carry = carry / 10000;      //看还能不能进位
        }
    }
    for(;j>=0;j--)
    printf(printf("%04d", r[j]);)
    return 0;
}