#include <stdio.h>

int main()
{
    int tmp, n,sum = 0;
    scanf("%d", &n);
    for (int i = 0; i < 10; i++)
    {
        tmp=n;
        while (tmp > 0)
        {
            if (tmp % 10 == i)
            {
                sum++;
                break;
            }
            tmp /= 10;
        }
    }
    printf("%d\n",sum);
    return 0;
}