#include <stdio.h>
#include <string.h>
int main()
{
    char s1[82], s2[82];
    scanf("%s%s", s1, s2);
    int i = 0, j = 0;
    while (s1[i++] != s2[j] && i < strlen(s1))
        ;
    i--;
    while (s1[i++] == s2[j++] && i < strlen(s1))
        ;
    if (j == strlen(s2)+1)
        printf("yes\n");
    else
        printf("no\n");
    return 0;
}