#include <stdio.h>

long jiecheng(int n)
{
    long rlt=1;
    for(int i=2;i<=n;i++)
        rlt*=i;
    return rlt;
}

int main()
{
    int n;
    printf("Please enter n:");
    scanf("%d",&n);
    for(int i=1;i<=n;i++)
    {
        printf("%d! = %ld\n",i,jiecheng(i));
    }
    return 0;
}