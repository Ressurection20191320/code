#include <stdio.h>

void print(int i)
{
    i = 2 * i - 1;
    for (int n = 0; n < i; n++)
        printf("*");
    printf("\n");
}

int main()
{
    int n;
    scanf("%d", &n);
    int i = 0;
    while (n > 0)
    {
        for (int l = 0; l < i; l++)
            printf(" ");
        print(n);
        n--;
        i++;
    }
}
