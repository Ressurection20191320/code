#include <stdio.h>

int main()
{
    char ch[81];
    for (int i = 0; i < 82; i++)
        ch[i] = '\0';
    scanf("%[^\n]", &ch);
    for (int i = 81; i >= 0; i--)
    {
        if (ch[i] != '\0')
        printf("%c", ch[i]);
    }
    printf("\n");
    return 0;
}