#include <stdio.h>

int main()
{
    char str[100];
    int sum = 0;
    gets(str);
    for (int i = 0; str[i] != '\0'; i++)
    {
        if (str[i] == 'a')
            sum += 10;
        else if (str[i] == 'b')
            sum -= 10;
        else if (str[i] == 'c')
            sum += 5;
        else if (str[i] == 'd')
            sum -= 5;
    }
    printf("%d", sum);
    return 0;
}