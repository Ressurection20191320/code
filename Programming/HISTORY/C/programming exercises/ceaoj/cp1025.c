//partial accepted
#include <stdio.h>
#include <string.h>

int main()
{
    char str[80], word1[80], word2[80], result[80];
    int words_num = 0;
    scanf("%[^\n]", str);

    for (int i = 0; i < strlen(str); i++)
    {
        if (str[i] == ' ')
            words_num += 1;
    }

    if (words_num < 2)
    {
        for (int i = strlen(str); i > -1; i--)
        {
            if (str[i] == '.')
            {
                str[i] = '?';
                break;
            }
        }
        printf("%s\n", str);
    }

    else
    {
        int i = 0;
        while (1)
        {
            if (str[i] == ' ')
            {
                word1[i] = ' ';
                i++;
                word1[i] = '\0';
                break;
            }
            else
            {
                word1[i] = str[i];
            }
            i++;
        }
        int tmp = i;
        while (1)
        {
            if (str[i] == ' ')
            {
                word2[i - tmp] = ' ';
                i++;
                word2[i - tmp] = '\0';
                break;
            }
            else
            {
                word2[i - tmp] = str[i];
            }
            i++;
        }

        word1[0] += 32;
        word2[0] -= 32;

        strncat(result, word2, strlen(word2));
        strncat(result, word1, strlen(word1));

        for (int i = strlen(result); i < strlen(str); i++)
        {
            result[i] = str[i];
        }

        for (int i = strlen(result); i > -1; i--)
        {
            if (result[i] == '.')
            {
                result[i] = '?';
                break;
            }
        }

        printf("%s\n", result);
        return 0;
    }
}