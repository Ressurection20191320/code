#include <stdio.h>

int judge(int x)
{
    int num[4], n = x;
    for (int i = 3; n; i--)
    {
        num[i] = n % 10;
        n /= 10;
    }
    for (int i = 0, j = 3; i < 4; i++, j--)
    {
        int tmp = num[i];
        num[i] = num[j];
        num[j] = tmp;
    }
    int m = 0, d = 1000;
    for (int i = 3; d; d /= 10)
    {
        m += (num[i--] * d);
    }
    if (m / x == 9 && m % x == 0)
        return 1;
    return 0;
}

int main()
{
    for (int n = 1000; n < 10000; n++)
    {
        if (judge(n))
        {
            printf("The number satisfied states condition is : %d\n", n);
            return 0;
        }
    }
}