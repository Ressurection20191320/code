#include <stdio.h>
#include <string.h>

int check_ch(char ch)
{
    if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z')
        return 1;
    return 0;
}

int main()
{
    char ch[80];
    int rlt = 0;
    gets(ch);
    for (int i = 0; i < 80; i++)
    {
        if (i==0&&check_ch(ch[i])||ch[i-1]==' '&&check_ch(ch[i]))
            while (1)
            {
                if (check_ch(ch[i]))
                {
                    i++;
                }
                else if (ch[i] == ' '||ch[i] == '\0')
                {
                    rlt++;
                    break;
                }
                else
                    break;
            }
    }
    printf("%d\n", rlt);
}