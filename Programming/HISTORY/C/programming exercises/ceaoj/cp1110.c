
#include <stdio.h>

int is_prime(int n)
{
    for (int i = 2; i < n; i++)
        if (n % i == 0)
            return 0;
    return 1;
}

int main()
{
    int p, q, count = 0;
    for (p = 2;; p++)
    {
        q = 3000 - p;
        if (is_prime(p) && is_prime(q))
        {
            printf("p=%d,q=%d\n", p, q);
            count++;
            if(p>=q)
            break;
        }
    }
    printf("%d\n", count);
    return 0;
}