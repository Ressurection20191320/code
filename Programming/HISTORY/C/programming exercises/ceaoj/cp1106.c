#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int is_rev_num(int x)
{
    int y = 0;
    int tmp = x;
    while (x)
    {
        y *= 10;
        y += x % 10;
        x /= 10;
    }
    if (tmp == y)
        return 1;
    return 0;
}

int is_prime(int x)
{
    if (x < 2)
        return 0;
    for (int i = 2; i <= sqrt(x) + 1; i++)
        if (x % i == 0)
            return 0;
    return 1;
}

int main()
{
    int i = 1;
    int a, b;
    scanf("%d%d", &a, &b);
    int *rev_num = (int *)malloc(4 * 1000000);
    int foot = 0;
    while (i <= 10000)
    {
        if (is_rev_num(i))
            rev_num[foot++] = i;
        i++;
    }
    for (int d1 = 1; d1 <= 9; d1 += 2)
    {
        for (int d2 = 0; d2 <= 9; d2++)
        {
            for (int d3 = 0; d3 <= 9; d3++)
            {
                rev_num[foot++] = 10000 * d1 + 1000 * d2 + 100 * d3 + 10 * d2 + d1;
            }
        }
    }
    for (int d1 = 1; d1 <= 9; d1 += 2)
    {
        for (int d2 = 0; d2 <= 9; d2++)
        {
            for (int d3 = 0; d3 <= 9; d3++)
            {
                rev_num[foot++] = 100000 * d1 + 10000 * d2 + 1000 * d3 + 100 * d3 + 10 * d2 + d1;
            }
        }
    }
    for (int d1 = 1; d1 <= 9; d1 += 2)
    {
        for (int d2 = 0; d2 <= 9; d2++)
        {
            for (int d3 = 0; d3 <= 9; d3++)
            {
                for (int d4 = 0; d4 <= 9; d4++)
                    rev_num[foot++] = 1000000 * d1 + 100000 * d2 + 10000 * d3 + 1000 * d4 + 100 * d3 + 10 * d2 + d1;
            }
        }
    }
    for (int d1 = 1; d1 <= 9; d1 += 2)
    {
        for (int d2 = 0; d2 <= 9; d2++)
        {
            for (int d3 = 0; d3 <= 9; d3++)
            {
                for (int d4 = 0; d4 <= 9; d4++)
                    rev_num[foot++] = 10000000 * d1 + 1000000 * d2 + 100000 * d3 + 10000 * d4 + 1000 * d4 + 100 * d3 + 10 * d2 + d1;
            }
        }
    }
    for (int d1 = 1; d1 <= 9; d1 += 2)
    {
        for (int d2 = 0; d2 <= 9; d2++)
        {
            for (int d3 = 0; d3 <= 9; d3++)
            {
                for (int d4 = 0; d4 <= 9; d4++)
                    for (int d5 = 0; d5 <= 9; d5++)
                        rev_num[foot++] = 100000000 * d1 + 10000000 * d2 + 1000000 * d3 + 100000 * d4 + 10000 * d5 + 1000 * d4 + 100 * d3 + 10 * d2 + d1;
            }
        }
    }
    rev_num[foot++] = 1000000001;
    int x = -1;
    while (rev_num[++x] < a)
        ;
    while (rev_num[x] <= b)
    {
        if (is_prime(rev_num[x]))
            printf("%d\n", rev_num[x]);
        x++;
    }
    system("pause");
    return 0;
}