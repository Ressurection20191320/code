#include <stdio.h>
#include <math.h>

int main()
{
    int n, z, result;
    int a, i = 0;
    scanf("%d", &n);
    a = n;
    z = n % 10;
    while (a > 9)
    {
        a /= 10;
        i += 1;
    }
    result = n + a + z * pow(10, i) - a * pow(10, i) - z;
    printf("%d\n", result);
    return 0;
}