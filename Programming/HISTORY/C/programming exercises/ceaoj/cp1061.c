#include <stdio.h>

int main()
{
    char ch;
    int x = 0, y = 0, stat = 1;
    while (1)
    {
        scanf("%c", &ch);
        if (ch == 'L')
        {
            stat--;
            if (stat < 1)
                stat = 4;
        }
        else if (ch == 'R')
        {
            stat++;
            if (stat > 4)
                stat = 1;
        }
        else if (ch == 'P')
        {
            if (stat == 1)
                y++;
            else if (stat == 2)
                x++;
            else if (stat == 3)
                y--;
            else if (stat == 4)
                x--;
        }
        if (ch == '\n')
            break;
    }
    printf("%d %d\n", x, y);
    return 0;
}