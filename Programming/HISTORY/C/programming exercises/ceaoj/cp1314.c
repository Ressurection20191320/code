#include <stdio.h>

int main()
{
    int a, b, c, x;
    scanf("%d%d%d", &a, &b, &c);
    x = b * b - 4 * a * c;
    if (x > 0)
        printf("2\n");
    else if (x == 0)
        printf("1\n");
    else
        printf("0\n");
    return 0;
}