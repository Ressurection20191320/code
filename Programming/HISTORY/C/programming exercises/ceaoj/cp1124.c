#include <stdio.h>
#include <math.h>

int main()
{
    int num[101], cnt = 0, min = 0x0fffffff, max = 0;
    num[1] = 0;
    for (int i = 1; num[i - 1] != -1; i++)
    {
        cnt += scanf("%d", &num[i]);
    }
    cnt--;
    for (int i1 = 1; i1 < cnt; i1++)
    {
        for (int i2 = i1 + 1; i2 <= cnt; i2++)
        {
            min = fabs(num[i1] - num[i2]) < min ? fabs(num[i1] - num[i2]) : min;
            max = fabs(num[i1] - num[i2]) > max ? fabs(num[i1] - num[i2]) : max;
        }
    }
    printf("min diff=%d, max diff=%d\n",min,max);
    return 0;
}