#include <stdio.h>

int check(int i)
{
    for (int n = 2; n < i; n++)
    {
        if (i % n == 0)
        {
            return 0;
        }
    }
    return 1;
}

int main()
{
    int n, tmp, sum = 0;
    scanf("%d", &n);
    while (n > 0)
    {
        if (check(n))
        {
            if (tmp - n == 2)
                sum += 1;
            tmp = n;
        }
        n--;
    }
    printf("%d\n", sum);
    return 0;
}