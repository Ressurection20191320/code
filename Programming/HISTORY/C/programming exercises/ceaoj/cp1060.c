#include <stdio.h>

int main()
{
    int a = 1, b, c;
    while (a != 0 || b != 0 || c != 0)
    {
        scanf("%d%d%d", &a, &b, &c);
        if (a + b > c && a + c > b && b + c > a)
            printf("YES\n");
        else if (a == 0 && b == 0 && c == 0)
            ;
        else
            printf("NO\n");
    }
    return 0;
}