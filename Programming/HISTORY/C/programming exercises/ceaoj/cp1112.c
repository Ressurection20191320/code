#include <stdio.h>

int is_prime(int n)
{
    if (n < 2)
        return 0;
    for (int i = 2; i < n; i++)
        if (n % i == 0)
            return 0;
    return 1;
}

int main()
{

    int l = 0;
    for (int x = 1; x <= 1000; x++)
    {
        int sum = 0;
        int i = x;
        while (i != 0)
        {
            sum += i % 10;
            i /= 10;
        }
        if (is_prime(sum))
        {
            printf("%d\t", x);
            l++;
        }
    }
    printf("%d\n",l);
    return 0;
}