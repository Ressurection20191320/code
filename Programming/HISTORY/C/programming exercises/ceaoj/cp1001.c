#include <stdio.h>

int main()
{
    int n, m, s = 1, cont;
    scanf("%d%d", &n, &m);
    cont = n;
    for (int i = 0; i < cont; i++)
    {
        s *= n;
        --n;
        s %= m;
    }

    printf("%d\n", s);
}