#include <stdio.h>

int main()
{
    int n, count[10], flag = 0;
    for (int i = 0; i < 10; i++)
        count[i] = 0;
    while (n != -1)
    {
        scanf("%d", &n);
        if (n == 0)
            count[0]++;
        while (n != 0 && n != -1)
        {
            count[n % 10]++;
            n /= 10;
        }
    }
    for (int i = 0; i < 10; i++)
    {
        if (count[i] == 0)
        {
            printf("%d\n", i);
            flag++;
        }
    }
    if (flag == 0)
        printf("NONE\n");
    return 0;
}