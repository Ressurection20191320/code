#include <stdio.h>
#include <math.h>
int check(int n)
{
    for (int i = 2; i < n; i++)
    {
        if (n % i == 0)
            return 0;
    }
    return 1;
}

int main()
{
    int n = 111, x = 3;
    while (1)
    {
        for (int i = 1; i < pow(10, x - 2); i++)
            if (check(i * 1000 + n))
            {
                printf("%d\n", i * 1000 + n);
                return 0;
            }
        x++;
    }
}