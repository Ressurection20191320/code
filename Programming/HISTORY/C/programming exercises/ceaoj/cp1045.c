#include <stdio.h>

int check(int n, int i)
{
    if (n % i == 0)
        return 1;
    else
        return 0;
}

int check1(int n)
{
    int tmp = n, result = 0;
    while (tmp != 0)
    {
        result += tmp % 10;
        tmp /= 10;
        if (tmp != 0)
            result *= 10;
    }
    if (result == n)
        return 1;
    else
        return 0;
}

int main()
{
    int n, result;
    scanf("%d", &n);
    for (int i = 1; i < n + 1; i++)
    {
        if (check(n, i) && check1(i))
            result = i;
    }
    printf("%d\n", result);
    return 0;
}