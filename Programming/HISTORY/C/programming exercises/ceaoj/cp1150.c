#include <stdio.h>

int main()
{
    int n;
    scanf("%d", &n);
    switch (n)
    {
    case 0/* constant-expression */:
        /* code */printf("zero");
        break;
    case 1/* constant-expression */:
        /* code */printf("one");
        break;
    case 2/* constant-expression */:
        /* code */printf("two");
        break;
    case 3/* constant-expression */:
        /* code */printf("three");
        break;
    case 4/* constant-expression */:
        /* code */printf("four");
        break;
    case 5/* constant-expression */:
        /* code */printf("five");
        break;
    case 6/* constant-expression */:
        /* code */printf("six");
        break;
    case 7/* constant-expression */:
        /* code */printf("seven");
        break;
    case 8/* constant-expression */:
        /* code */printf("eight");
        break;
    case 9/* constant-expression */:
        /* code */printf("nine");
        break;
    case 10/* constant-expression */:
        /* code */printf("ten");
        break;
    }
    return 0;
}