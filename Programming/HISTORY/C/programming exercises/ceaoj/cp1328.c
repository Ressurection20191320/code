#include <stdio.h>
#include <math.h>

int main()
{
    int n;
    scanf("%d", &n);
    float x = sqrt(n);
    x *= 1000;
    printf("%d\n", (int)x % 10);
    return 0;
}