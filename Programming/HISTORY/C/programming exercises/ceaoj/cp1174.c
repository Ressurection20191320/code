#include <stdio.h>

int main()
{
    for (int i = 1;; i++)
    {
        if ((i * i * i + 2 * i * i + 1) % 13 == 0)
        {
            printf("%d\n", i);
            return 0;
        }
    }
}