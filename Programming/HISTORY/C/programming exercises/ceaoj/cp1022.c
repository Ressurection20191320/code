#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int fun(char w[])
{
    char t, *s1, *s2;
    int n = strlen(w);
    s1 = w;
    s2 = w + n - 1;
    while (s1 < s2)
    {
        t = *s1;
        *s1 = *s2;
        *s2 = t;
        s1++;
        s2--;
    }
}

int main()
{

    char str[100];
    scanf("%[^\n]", str);
    fun(str);
    printf("%s\n", str);

    return 0;
}