#include <stdio.h>

int sum(int n)
{
    int sum=0;
    while(n!=0)
    {
        sum+=n%10;
        n/=10;
    }
    return sum;
}

int main()
{
    for(int i=1;i<1000;i++)
    {
        if(sum(i*i)==i+2)
        printf("%d\n",i);
    }
    return 0;
}