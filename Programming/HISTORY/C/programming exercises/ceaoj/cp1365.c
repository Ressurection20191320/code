#include <stdio.h>

int main()
{
    int n,c,ctr=0;
    scanf("%d", &n);
    for (int a = 1; a < n; a++)
    {
        for (int b = 1; b < n; b++)
        {
            c = n - a - b;
            if(c>0&&a%2==0&&b%3==0&&c%5==0)
            ctr++;
        }
    }
    printf("%d\n",ctr);
    return 0;
}