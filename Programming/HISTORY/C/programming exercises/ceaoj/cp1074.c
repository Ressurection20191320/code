#include <stdio.h>

int main()
{
    int num[21][21];
    int n, sum = 0;
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            scanf("%d", &num[i][j]);
    for (int i = 0, j = n - 1; i < n; i++, j--)
    {
        if (i == j)
            sum += num[i][i] * num[i][i];
        else
        {
            sum += num[i][i] * num[i][i];
            sum += num[i][j] * num[i][j];
        }
    }
    printf("%d\n", sum);
    return 0;
}