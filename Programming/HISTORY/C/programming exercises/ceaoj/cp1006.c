#include <stdio.h>

int main()
{
    int n, count = 0;
    scanf("%d", &n);
    n *= n;
    while (n != 0)
    {
        if (n % 10 == 1)
            count++;
        n /= 10;
    }
    printf("%d\n", count);
    return 0;
}