#include <stdio.h>

int cnt5(int i)
{
    if (i % 5 < 3)
        return i % 5;
    else if (i % 5 == 4)
        return 1;
    else if (i % 5 == 3)
        return 2;
}

int main()
{
    int n, tmp = 0, result;
    scanf("%d", &n);
    for (int i = n; i > 0; i--)
    {
        if (n % i == 0)
        {
            if (cnt5(i) >= tmp)
            {
                tmp = cnt5(i);
                result = i;
            }
        }
    }
    printf("%d\n", result);
    return 0;
}