#include <stdio.h>

int main()
{
    float t, h, v;
    scanf("%f%f", &v, &t);
    h = v * t - 0.5 * 9.8 * t * t;
    if (h > 0)
        printf("%.2f\n", h);
    else
        printf("0.00\n");
    return 0;
}