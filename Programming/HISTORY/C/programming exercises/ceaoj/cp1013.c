#include <stdio.h>

int check(int i)
{
    for (int n = 2; n < i; n++)
    {
        if (i % n == 0)
        {
            return 0;
        }
    }
    return 1;
}

int check123(int i)
{
    while (i != 0)
    {
        if (i % 10 != 1 && i % 10 != 2 && i % 10 != 3)
            return 0;
        i /= 10;
    }
    return 1;
}

int main()
{
    int n, i = 2;
    scanf("%d", &n);
    while (i < n + 1)
    {
        if (check(i) && check123(i))
            printf("%d\n", i);
        i++;
    }
    return 0;
}