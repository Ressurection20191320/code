#include <stdio.h>

int main()
{
    int num[10001];
    for (int i = 0; i < 10001; i++)
        num[i] = i;
    for (int i = 2; i < 101; i++)
    {
        if (num[i] == 0)
            continue;
        for (int j = i + 1; j < 10001; j++)
        {
            if (num[j] == 0)
                continue;
            num[j] = num[j] % num[i] == 0 ? 0 : num[j];
        }
    }
    for (int i = 100; i < 9961; i++)
    {
        if (num[i] && num[i + 40])
        {
            int flag = 1;
            for (int k = i + 1; k < i + 40; k++)
            {
                if (num[k] != 0)
                {
                    flag = 0;
                    break;
                }
            }
            if (flag)
            {
                printf("%d,%d\n", i, i + 40);
                return 0;
            }
            continue;
        }
    }
    printf("NO");
    return 0;
}