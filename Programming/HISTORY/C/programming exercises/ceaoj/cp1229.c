#include <stdio.h>

int main()
{
    int n;
    long rlt=1;
    printf("Input m:");
    scanf("%d",&n);
    for(int i=2;i<=n;i++)
        rlt*=i;
    printf("%d! = %ld\n",n,rlt);
    return 0;
}