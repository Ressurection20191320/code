#include <stdio.h>

int check(int n)
{
    int tmp = n, result = 0;
    while (tmp != 0)
    {
        result += tmp % 10;
        tmp /= 10;
        if (tmp != 0)
            result *= 10;
    }
    if (result == n)
        return 1;
    else
        return 0;
}

int main()
{
    int n = 12, count = 0;
    while (n != -1)
    {
        scanf("%d", &n);
        if (check(n))
            count++;
    }
    printf("%d\n", --count);
    return 0;
}