#include <stdio.h>
#include <math.h>
int main()
{
    int n;
    scanf("%d", &n);
    for (int i = 0; i < 100; i++)
    {
        n *= n;
        n %= 10;
    }
    printf("%d\n", n);
}