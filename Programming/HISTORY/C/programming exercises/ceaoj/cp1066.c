#include <stdio.h>
#include <math.h>
int check(int n)
{
    if (n < 2)
        return 0;
    for (int i = 2; i < n; i++)
        if (n % i == 0)
            return 0;
    return 1;
}

int main()
{
    int n, flag;
    scanf("%d", &n);
    for (int i = n; i > 1; i--)
    {
        // if (check(i) == 0)
        //     continue;
        flag = 1;
        for (int x = 1; i % (int)pow(10, x) != i; x++)
        {
            if (check(i % (int)pow(10, x)) == 0)
            {
                flag = 0;
                break;
            }
        }
        if (flag == 1)
        {
            printf("%d\n", i);
            return 0;
        }
    }
}