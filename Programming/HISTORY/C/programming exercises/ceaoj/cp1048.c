#include <stdio.h>
#include <string.h>

int main()
{
    char str[80];
    int count = 0;
    scanf("%[^\n]", &str);
    for (int i = 0; i < strlen(str); i++)
    {
        if (!((str[i - 1] > 'a' - 1 && str[i - 1] < 'z' + 1 || str[i - 1] > 'A' - 1 && str[i - 1] < 'Z' + 1)) || i == 0)
        {
            if (str[i] > 'A' - 1 && str[i] < 'Z' + 1 && (str[i + 1] > 'a' - 1 || str[i + 1] < 'z' + 1 || str[i + 1] > 'A' - 1 || str[i + 1] < 'Z' + 1))
                count++;
        }
    }
    printf("%d\n", count);
    return 0;
}