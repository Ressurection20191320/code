#include <stdio.h>
double y1(double x);
double y2(double x);
double integral(double (*f)(double), double a, double b);

int main()
{
    printf("y1=%f\ny2=%f\n",integral(y1,0,1),integral(y2,0,3));
    return 0;
}

double y1(double x)
{
    return 1 + x * x;
}

double y2(double x)
{
    return x / (1 + x * x);
}

double integral(double (*f)(double), double a, double b)
{
    int n = (b - a) * 1000000;
    double d = (b - a) / n, rlt = 0;
    for (int i=0;i<n;i++)
    {
        rlt+=0.5*d*((*f)(a)+(*f)(a+d));
        a+=d;
    }
    return rlt;
}