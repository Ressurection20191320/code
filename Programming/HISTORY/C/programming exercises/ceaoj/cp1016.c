#include <stdio.h>

int main()
{
    int tmp, n;
    scanf("%d", &n);
    for (int i = 10; i > 0; i--)
    {
        tmp = n;
        while (tmp > 0)
        {
            if (tmp % 10 == i)
            {
                printf("%d\n", i);
                return 0;
            }
            tmp /= 10;
        }
    }
}