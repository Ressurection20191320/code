#include <stdio.h>
#include <string.h>

int main()
{
    char str[82];
    gets(str);
    int len = strlen(str);
    if (len % 2 == 0)
    {
        printf("%c", str[len / 2 - 1]);
        printf("%c", str[len / 2]);
    }
    else
        printf("%c", str[len / 2]);
    printf("\n");
    return 0;
}