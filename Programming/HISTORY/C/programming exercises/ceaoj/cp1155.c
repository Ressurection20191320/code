#include <stdio.h>
void ChangeSort(int num[], int n)
{
    int i, j, tmp;
    for (int i = 0; i < n - 1; i++)
        for (int j = i + 1; j < n; j++)
            if (num[i] > num[j])
            {
                tmp = num[i];
                num[i] = num[j];
                num[j] = tmp;
            }
}
int main()
{
    int num[5];
    for (int i = 0; i < 5; i++)
        scanf("%d", &num[i]);
    ChangeSort(num, 5);
    printf("%d %d\n", num[0], num[1]);
    return 0;
}