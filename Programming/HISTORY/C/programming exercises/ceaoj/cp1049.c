//partial accepted
#include <stdio.h>
#include <string.h>

int main()
{
    char str[80];
    int count = 0;
    scanf("%[^\n]", &str);
    for (int i = 0; i < strlen(str)+1; i++)
    {
        if (str[i - 1] > 'a' - 1 && str[i - 1] < 'z' + 1 || str[i - 1] > 'A' - 1 && str[i - 1] < 'Z' + 1)
        {
            if ((str[i] =='e' || str[i] == 'E') && !(str[i + 1] > 'a' - 1 && str[i + 1] < 'z' + 1 || str[i + 1] > 'A' - 1 && str[i + 1] < 'Z' + 1))
                count++;
        }
    }
    printf("%d\n", count);
    return 0;
}