#include <stdio.h>

int main()
{
    int sum=0, n;
    for (int i = 0; i < 10; i++)
    {
        scanf("%d", &n);
        sum += n;
    }
    printf("%.2f\n",1-(float)sum/10);
    return 0;
}