#include <stdio.h>

int main()
{
    char str[1024];
    char max = 0;
    printf("Input a string:\n");
    gets(str);
    for (int i = 0; str[i] != 0; i++)
        max = str[i] > max ? str[i] : max;
    printf("The largest character of \"%s\" is \'%c\' ,The ASCII is %d.",str,max,max);
    return 0;
}