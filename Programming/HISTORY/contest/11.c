#include <stdio.h>

int reverse(int n)
{
    int m = 0;
    while (n)
    {
        m *= 10;
        m += n % 10;
        n /= 10;
    }
    return m;
}

int main()
{
    int m, flag = 0;
    scanf("%d", &m);
    int son, father;
    for (father = 119; father > 21; father--)
    {
        for (son = 100; son > 0; son--)
        {
            if (reverse(father) == reverse(son) * m && father - son > 20)
            {
                printf("father age: %5d, son age: %5d\n", father, son);
                flag = 1;
            }
        }
    }
    if (!flag)
        printf("NONE\n");
    return 0;
}