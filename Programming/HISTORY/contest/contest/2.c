#include <stdio.h>

int main()
{
    int a[30], b[30], len = 0, tmp;
    for (int i = 0; i < 30; i++)
    {
        a[i] = 0;
    }
    for (int i = 0; a[i - 1] != -1; i++)
    {
        scanf("%d", &a[i]);
        b[i] = a[i];
        len++;
    }
    for (int i = 0; i < len; i++)
    {
        b[i] = b[i] % 10;
    }
    SelectionSort(b, a, len - 1);
    for (int i = 0; i < len - 1; i++)
    {
        printf("%d\n", a[i]);
    }
    return 0;
}

void SelectionSort(int a[], int b[], int n)
{
    int i, j, k, temp;
    for (i = 0; i < n - 1; i++)
    {
        k = i;
        for (j = i + 1; j < n; j++)
        {
            if (a[j] > a[k])
            {
                k = j;
            }
        }
        if (k != j)
        {
            temp = a[k];
            a[k] = a[i];
            a[i] = temp;
            temp = b[k];
            b[k] = b[i];
            b[i] = temp;
        }
    }
}