#include <stdio.h>

int max=0;

int findmax(int n[1000][1000],int now,int r,int c,int x)
{
    if(r>x)
    return 0;
    now+=n[r][c];
    max=now>max?now:max;
    findmax(n,now,r+1,c,x);
    findmax(n,now,r+1,c+1,x);
}

int main()
{
    int n[1000][1000];
    int r;
    scanf("%d", &r);
    int i1, i2;
    for (i1 = 0; i1 < r; i1++)
    {
        for (i2 = 0; i2 < i1+1; i2++)
        {
            scanf("%d", &n[i1][i2]);
        }
    }
    findmax(n,0,0,0,r);
    printf("%d",max);
    return 0;
}