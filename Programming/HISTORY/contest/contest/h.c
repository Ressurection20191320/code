#include <stdio.h>
#include <math.h>

int S(int n1, int n2)
{
    int x = n1;
    for (int i = 0; i < n2; i++)
        x >>= 1;
    int t = x % 2;
    if (!t)
    {
        return n1 + pow(2, n2);
    }
    return n1;
}

int C(int n1, int n2)
{
    int x = n1;
    for (int i = 0; i < n2; i++)
        x >>= 1;
    int t = x % 2;
    if (t)
    {
        return n1 - pow(2, n2);
    }
    return n1;
}

int X(int n1, int n2)
{
    int x = n1;
    for (int i = 0; i < n2; i++)
        x >>= 1;
    int t = x % 2;
    if (t)
    {
        return n1 - pow(2, n2);
    }
    return n1 + pow(2, n2);
}

int R(int n1, int n2)
{
    int x = n1;
    for (int i = 0; i < n2; i++)
        x >>= 1;
    int t = x % 2;
    return t;
}

int main()
{
    char c;
    int n1, n2, flag = 1;
    while (1)
    {
        scanf("%c", &c);
        scanf("%d%d", &n1, &n2);
        switch (c)
        {
        case 'S':
            printf("%d\n", S(n1, n2));
            break;
        case 'C':
            printf("%d\n", C(n1, n2));
            break;
        case 'X':
            printf("%d\n", X(n1, n2));
            break;
        case 'R':
            printf("%d\n", R(n1, n2));
            break;
        case 'E':
            flag = 0;
            break;
        default:
            break;
        }
        if (!flag)
            break;
    }
}