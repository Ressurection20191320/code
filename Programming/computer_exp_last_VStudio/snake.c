#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <conio.h>
#pragma comment(lib,"winmm.lib")

#define up 'w'                                           //72
#define down 's'                                         //80
#define left 'a'                                         //75
#define right 'd'                                        //77
#define TRUESPEED ((difficulty + 5) * (score + 15)) //速度设置方程

//原始窗口大小 58,26

typedef enum Bool
{
    false,
    true
} bool;

void gotoxy(int x, int y);
void gotoprint(int x, int y, int flag);
void gotodelete(int x, int y);
void welcome();
void createcraph();
void createfood();
int clickcontrol();
void movebody();
void changebody(int x, int y);
bool judge();
bool ranking();
void eating();
void finish();
void ChangeSort(int n);

struct snake
{
    int x;
    int y;
    struct snake *next;
};
typedef struct snake snake;

struct snake *head;

struct info
{
    int score;
    char name[20];
};

struct Food
{
    int x;
    int y;
} food;

int score = 0;
int speed = 200;
int len_x;
int len_y;
char click = ' ';

struct info info[20];
int difficulty;
int circulation = 1;

int main()
{
    system("mode con:cols=120 lines=40");
    system("color f2");
	welcome();
    createcraph();
    createfood();
    clickcontrol();
    info[circulation].score = score * TRUESPEED / 100;
    while (ranking())
    {
        score = 0;
        fflush(stdin);
        system("cls");
        gotoxy(60, 26);
        printf("这是第%d次游戏", ++circulation);
        welcome();
        createcraph();
        createfood();
        fflush(stdin);
        click = ' ';
        clickcontrol();
        info[circulation].score = score * TRUESPEED / 10;
        score = 0;
    }
    finish();
    gotoxy(60, 26);
    return 0;
}

void welcome()
{
	PlaySound(TEXT("D:/x.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
	gotoxy(25, 6);
    printf("20191320李泽昊，计算机实习：贪吃蛇游戏");
    gotoxy(25, 9);
    printf("游戏使用WSAD键移动蛇，Q键退出本局，WSAD改变蛇行进方向，其余按键暂停。");
    gotoxy(25, 12);
    printf("请输入用户名:");
    scanf_s("%s", info[circulation].name,20);
    gotoxy(25, 15);
    printf("请输入游戏界面宽度(最小10，最大30，超过边界取最大或最小值):");
    scanf("%d", &len_x);
    len_x = len_x >= 30 ? 29 : len_x;
    len_x = len_x < 10 ? 10 : len_x;
    len_x *= 2;
    gotoxy(25, 16);
    printf("请输入游戏界面长度(最小10，最大40，超过边界取最大或最小值):");
    scanf("%d", &len_y);
    len_y = len_y >= 40 ? 39 : len_y;
    len_y = len_y < 10 ? 10 : len_y;
    gotoxy(25, 18);
    printf("请输入游戏难度(蛇的初始速度和速度增长快慢，最小1,最大10):");
    scanf("%d", &difficulty);
    difficulty = difficulty > 10 ? 10 : difficulty;
    difficulty = difficulty < 1 ? 1 : difficulty;
}

void createcraph()
{
    system("cls");

    int i, j;

    for (i = 0; i <= len_x; i += 2)
    {

        gotoprint(i, 0, 0);
        gotoprint(i, len_y, 0);
    }

    for (i = 1; i <= len_y; i++)
    {
        gotoprint(0, i, 0);
        gotoprint(len_x, i, 0);
    }

    gotoxy(64, 7);
    printf("%s，欢迎您玩贪吃蛇游戏!", info[circulation].name);
    gotoxy(64, 11);
    printf("当前分数为：0     ");
    gotoxy(64, 15);
    printf("操作说明  W:向上 S:向下 A:向左 D:向右 其余按键暂停");
    gotoxy(64, 17);
    printf("游戏中按Q键直接退出本局！");

    head = (snake *)malloc(sizeof(snake));
    snake *q = (snake *)malloc(sizeof(snake));

    head->x = 4;
    head->y = 4;
    q->x = 2;
    q->y = 4;
    head->next = q;
    q->next = NULL;
}

void createfood()
{
    bool flag = false;
    while (!flag)
    {
        flag = true;
        srand((int)time(NULL));
        food.x = rand() % (len_x - 2) + 1;
        food.y = rand() % (len_y - 2) + 1;
        if (food.x % 2 != 0)
        {
            food.x += 1;
        }
        snake *temp = head; //暂存蛇头
        while (true)
        {
            if (temp->x == food.x && temp->y == food.y)
            {
                flag = false;
                break;
            }
            if (temp->next == NULL)
                break;
            temp = temp->next;
        }
    }
    gotoxy(food.x, food.y);
    printf("⊙");
}

void clickcontrol()
{
    while (true)
    {
        if (judge())
            return;
        if (_kbhit()) //当键盘输入时，才会执行，实现异步
            click = _getch();
        movebody();
        eating();
    }
}

void movebody()
{
    int count = 0;
    int x = head->x, y = head->y;
    snake *p = head;
    while (p->next != NULL)
        p = p->next;
    gotodelete(p->x, p->y);
    switch (click)
    {
    case up:
        y -= 1;
        break;
    case down:
        y += 1;
        break;
    case left:
        x -= 2;
        break;
    case right:
        x += 2;
        break;
    default:
        break;
    }
    if (x != head->x || y != head->y)
        changebody(x, y);
    p = head;
    gotoprint(p->x, p->y, 0);
    gotoprint(p->next->x, p->next->y, 1);
    speed = 30000 / TRUESPEED;
    Sleep(speed);
} //在图形上改变蛇

void changebody(int x, int y) //在链表上改变蛇
{
    snake *p = head;
    while (p->next->next != NULL)
    {
        p = p->next;
    }
    free(p->next);
    p->next = NULL;
    snake *new_head = (snake *)malloc(sizeof(snake));
    new_head->x = x;
    new_head->y = y;
    new_head->next = head;
    head = new_head;
}

void eating()
{
    if (food.x == head->x && food.y == head->y)
    {
        createfood();
        snake *_new = (snake *)malloc(sizeof(snake));
        snake *p = head;
        while (p->next != NULL)
            p = p->next;
        p->next = _new;
        _new->next = NULL;
        score += 1;
        gotoxy(64, 11);
        printf("当前分数：%d     ", score * TRUESPEED / 10);
    }
}

bool judge()
{
    if (click == 'q')
    {
		PlaySound(TEXT("D:/Music/超级玛丽失败.wav"), NULL, SND_FILENAME | SND_ASYNC);
		MessageBox(NULL, TEXT("选择退出，本局游戏结束！\n你的成绩将会被记录！"), TEXT("GAME OVER 游戏结束"), MB_ICONINFORMATION);
        return true;
    }
    if (head->x == 0 || head->x == len_x || head->y == 0 || head->y == len_y)
    {
		PlaySound(TEXT("D:/Music/超级玛丽失败.wav"), NULL, SND_FILENAME  | SND_ASYNC);
		MessageBox(NULL, TEXT("撞到墙壁，游戏结束！"), TEXT("GAME OVER  游戏结束"), MB_ICONINFORMATION);
        return true;
    } //锟叫讹拷撞墙
    snake *p = head->next;
    while (true)
    {
        if (p == NULL)
            break;
        if (head->x == p->x && head->y == p->y)
        {
			PlaySound(TEXT("D:/Music/超级玛丽失败.wav"), NULL, SND_FILENAME | SND_ASYNC);
			MessageBox(NULL, TEXT("撞到自己，游戏结束！"), TEXT("GAME OVER  游戏结束"), MB_ICONINFORMATION);
            return true;
        }

        p = p->next;
    }
    return false;
}

bool ranking()
{
    system("cls");
    gotoxy(45, 3);
    printf("排 行 榜");
    ChangeSort(circulation);
    gotoxy(34, 7);
    printf("名次        用户名        分数");
    for (int i = 9; (i - 7) / 2 <= circulation && (i - 7) / 2 <= 8; i += 2)
    {
        int j = (i - 7) / 2;
        gotoxy(35, i);
        printf("%d", j);
        gotoxy(46, i);
        printf("%s", info[j].name);
        gotoxy(60, i);
        printf("%d", info[j].score);
    }
    gotoxy(36, 30);
	PlaySound(TEXT("D:/Music/排行榜.wav"), NULL, SND_FILENAME | SND_ASYNC);
	Sleep(1000);
    fflush(stdin);
    if (circulation < 21)
        printf("再玩一局?(y确认，其他取消)");
    else
    {
        printf("玩了20次了，请休息！ 游戏结束！");
        Sleep(5000);
        return 0;
    }
    fflush(stdin);
    char flag = getchar();
    return flag == 'y';
}

void finish()
{
    system("cls");
    gotoxy(40, 10);

    printf("感谢您玩贪吃蛇游戏！");
    gotoxy(40, 15);
    printf("最高得分为：%d分，记录由%s创造！", info[1].score, info[1].name);
    gotoxy(40, 17);
    printf("计算机实习：20191320李泽昊");
	PlaySound(TEXT("D:/Music/结束.wav"), NULL, SND_FILENAME | SND_ASYNC);
    snake *p = head, *q;
    while (p != NULL)
    {
        q = p->next;
        free(p);
        p = q;
    }
    gotoxy(40, 19);
    system("pause");
}

void gotoxy(int x, int y)
{
    COORD pos;
    pos.X = x;
    pos.Y = y;
    HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleCursorPosition(output, pos);
    CONSOLE_CURSOR_INFO cursor;
    cursor.bVisible = FALSE;
    cursor.dwSize = sizeof(cursor);
    SetConsoleCursorInfo(output, &cursor);
}

void gotoprint(int x, int y, int flag)
{
    gotoxy(x, y);
    if (flag)
        printf("■");
    else
        printf("□");
}

void gotodelete(int x, int y)
{
    gotoxy(x, y);
    printf("  ");
}

void ChangeSort(int n)
{
    int i, j;
    struct info tmp;
    for (int i = 1; i < n; i++)
        for (int j = i + 1; j <= n; j++)
            if (info[i].score < info[j].score)
            {
                tmp = info[i];
                info[i] = info[j];
                info[j] = tmp;
            }
}