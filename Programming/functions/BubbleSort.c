void BubbleSort(int num[], int n)
{
    int i, j, tmp;
    for (i = 0; i < n - 1; i++)
        for (j = 1; j < n - 1; j++)
        {
            if (num[j] < num[j - 1])
            {
                tmp = num[j];
                num[j] = num[j - 1];
                num[j - 1] = tmp;
            }
        }
}