int Binsearch(long num[], long x, int n) //num为原数组，x为被查找对象，n为数组长度。
{
    int low = 0, high = n - 1, mid;
    while (low <= high)
    {
        mid = low + (high - low) / 2;
        if (x > num[mid])
            low = mid + 1;
        else if (x < num[mid])
            high = mid - 1;
        else
            return mid; //注意是返回的是结果的位置
    }
    return -1;
}

int binary_search_first(long num[], long x, int n)
{
    int low = 0, high = n - 1;
    while (low < high)
    {
        int mid = low + (high - low) / 2;
        if (x <= num[mid])
            high = mid;
        else
            low = mid + 1;
    }
    return num[low] == x ? low : -1;
}

int binary_search_last(long num[], long x, int n)
{
    int low = 0, high = n - 1;
    while (low < high)
    {
        int mid = low + (high - low) / 2;
        mid = (high - mid) + low;
        if (x < num[mid])
            high = mid - 1;
        else
            low = mid;
    }
    return num[low] == x ? low : -1;
}