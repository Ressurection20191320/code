void ChangeSort(int num[], int n)
{
    int i, j, tmp;
    for (int i = 0; i < n - 1; i++)
        for (int j = i + 1; j < n; j++)
            if (num[i] < num[j]) //可以通过改变这里的大于小于号来改变升降序
            {
                tmp = num[i];
                num[i] = num[j];
                num[j] = tmp;
            }
}