void SelectionSort(int num[],int n)
{
    int i,j,k,tmp;
    for(i=0;i<n-1;i++)
    {
        k=i;
        for(j=i+1;j<n;j++)
        {
            if(num[i]<num[k])
            k=j;
        }
    }
    if(k!=i)
    {
        tmp=num[k];
        num[k]=num[i];
        num[i]=tmp;
    }
}