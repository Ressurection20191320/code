// 编译该范例前，请把 background.mp3 放在项目文件夹中
// 发布时，请把 background.mp3 和编译的 exe 放在一起
// 编译环境：VC6 + EasyX v20100825
//
#include <conio.h>
#include <stdio.h>
// 引用 Windows Multimedia API
#pragma comment(lib, "Winmm.lib")

void main()
{

    // 打开音乐
    mciSendString("open D://x.mp3 alias mymusic", NULL, 0, NULL);

    getch();

    // 播放音乐
    mciSendString("play mymusic", NULL, 0, NULL);

    getch();

    // 停止播放并关闭音乐
    mciSendString("stop mymusic", NULL, 0, NULL);
    mciSendString("close mymusic", NULL, 0, NULL);

    getch();
}