#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int eular(int n)
{
    int res=n;
    for(int i=2;i<=sqrt(n);i++)
    {
        if(n%i==0)res=res/i*(i-1);
        while(n%i==0)n/=i;
    }
    if(n>1)res=res/n*(n-1);
    return res;
}

int isyuangeng(int a,int n)
{
    int e=eular(n);
    int tmp=a;
    for(int i=2;i<e;i++)
    {
        tmp*=a;
        tmp%=n;
        if(tmp==1)return 0;
    }
    if(tmp*a%n==1)return 1;
    return 0;
}

int main()
{
    int n;
    printf("输入一个存在原根的数，输出其原根:\n"); 
    scanf("%d",&n);
    printf("%d共有%d个原根,其",n,eular(eular(n)));
    printf("原根如下：\n");
    for(int i=2;i<n;i++)
        if(isyuangeng(i,n)==1)printf("%d\t",i);
    putchar('\n');
    system("pause");
    return 0;
}