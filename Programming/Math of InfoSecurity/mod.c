#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
    printf("输入最高次方：");
    int MAX;
    scanf("%d",&MAX);
    MAX++;
    int an[MAX];
    int m;
    int rlt;  
    for (int i = 0; i < MAX; i++)
    {
        printf("输入x^%d的系数:", i);
        scanf("%d", &an[i]);
    }
    printf("输入模数m:");
    scanf("%d", &m);
    printf("所有解如下:\n");
    for (int x = 0; x < m; x++)
    {
        rlt = 0;
        for (int i = 0; i < MAX; i++)
        {
            rlt += (an[i] * pow(x, i));
            rlt%=m;
        }
        if (rlt == 0)
            printf("%d\t", x);
    }
    putchar('\n');
    system("pause");
    return 0;
}