#ifndef _SDF_H
#define _SDF_H

typedef struct DeviceInfo_st{
unsigned char IssuerName[40];
unsigned char DeviceName[16];
unsigned char DeviceSerial[16];
unsigned int DeviceVersion;
unsigned int StandardVersion;
unsigned int AsymAlgAbility[2];
unsigned int SymAlgAbility;
unsigned int HashAlgAbility;
unsigned int BufferSize;
} DEVICEINFO;
# define ECCref_MAX_BITS 512
# define ECCref_MAX_LEN ((ECCref_MAX_BITS+3)/4)
# define SGD_SM2_1 0x00020200
# define SGD_SM2_3 0x00020800
# define SGD_PUBLIC_KEY_SIGN 0x00000103
# define SGD_PUBLIC_KEY_ENCRYPT 0x00000104
# define SGD_PRIVATE_KEY_SIGN 0x00000105
# define SGD_PRIVATE_KEY_ENCRYPT 0x00000106
#define FILE_PATH "Keypair.txt"
#define INVALID_NULL_VALUE_INPUT    0x1000
#define INVALID_INPUT_LENGTH        0x1001
#define CREATE_SM2_KEY_PAIR_FAIL    0x1002
#define COMPUTE_SM3_DIGEST_FAIL     0x1003
#define ALLOCATION_MEMORY_FAIL      0x1004
#define COMPUTE_SM2_SIGNATURE_FAIL  0x1005
#define INVALID_SM2_SIGNATURE       0x1006
#define VERIFY_SM2_SIGNATURE_FAIL   0x1007
typedef struct ECCrefPublicKey_st{
   unsigned int bits;
   unsigned char x[ECCref_MAX_LEN];
   unsigned char y[ECCref_MAX_LEN];
}ECCrefPublicKey;

typedef struct ECCrefPrivateKey_st{
   unsigned int bits;
   unsigned char K[ECCref_MAX_LEN];
}ECCrefPrivateKey;

//定义密钥对
typedef struct sm2_key_pair_structure {
   ECCrefPrivateKey pri_key;
   ECCrefPublicKey pub_key;
}SM2_KEY_PAIR;


//定义密钥容器：盛放两对密钥，签名验签公私钥、加密解密公私钥
typedef struct keybox{
    unsigned length;//默认length=2
    SM2_KEY_PAIR mysm2_key_pair[2];//盛放两对密钥，签名验签公私钥、加密解密公私钥，默认第一对加解密公私钥，第二对签名验签公私钥
}KEYBOX;

#define SDR_OK 0x0
/*打开密码设备*/
int SDF_OpenDevice( void * * phDeviceHandle);

/*关闭设备*/

int SDF_CloseDevice( void * hDeviceHandle);


/*获取设备信息*/
int SDF_GetDeviceInfo(void * hSessionHandle,DEVICEINFO * pstDeviceInfo) ;

/*产生随机数*/
int SDF_GenerateRandom (void * hSessionHandle,unsigned int uiLength,unsigned char * pucRandom);

/*导出ECC签名公钥*/
int SDF_ExportSignPublicKey_ECC(void * hSessionHandle,unsigned int uiKeyIndex,ECCrefPublicKey * pucPublicKey);
/*导出ECC加密公钥*/
int SDF_ExportEncPublicKey_ECC(void * hSessionHandle,unsigned int uiKeyIndex,ECCrefPublicKey * pucPublicKey);
/*产生ECC密钥对并输出*/
int SDF_GenerateKeyPair_ECC(void * hSessionHandle,unsigned int uiAlgID,unsigned int uiKeyBits,ECCrefPublicKey * pucPublicKey,ECCrefPrivateKey * pucPrivateKey);


#endif
