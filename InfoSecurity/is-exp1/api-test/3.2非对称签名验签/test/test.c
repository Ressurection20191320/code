#include "sdf.h"
#include <memory.h>
#include <time.h>
#include <stdio.h>
#include <sys/types.h>

typedef unsigned char byte;
void debug(const char* str) {
    fprintf(stderr, "%s\n", str);
}
int main(int argc, char* argv[])
{
    void* session, * dev;
    ECCrefPrivateKey priv = {};
    ECCSignature sig = {};
    DEVICEINFO devInfo;
    void* keyHandle;
    byte msg[] = "e10adc3949ba59abbe56e057f20f883e";
    int index = 1;

    unsigned char pri_key[32] = { 0x39, 0x45, 0x20, 0x8f, 0x7b, 0x21, 0x44, 0xb1,
                                 0x3f, 0x36, 0xe3, 0x8a, 0xc6, 0xd3, 0x9f, 0x95,
                                 0x88, 0x93, 0x93, 0x69, 0x28, 0x60, 0xb5, 0x1a,
                                     0x42 ,0xfb, 0x81, 0xef, 0x4d, 0xf7, 0xc5, 0xb8 };
    memcpy(priv.D , pri_key, sizeof(pri_key));

    int r = SDF_OpenDevice(&dev);
    if (r != 0) {
        debug("open dev err");
        SDF_CloseDevice(dev);
        return -1;
    }
    else {
        debug("open dev  ok");
    }

    r = SDF_OpenSession(dev, &session);
    if (r != 0) {
        debug("open session err");
        goto end;
    }
    else {
        debug("open session ok");
    }

    r = SDF_ExternalSign_ECC(session, SGD_SM2_1, &priv, msg, 32, &sig);
    if (r) {
        debug("sign err");
        printf("�������%x\n", r);
        goto end;
    }
    else {
        debug("sign ok");
        for (int i = 0;i < 65;i++) {
            if(i<32)printf("%02x", sig.r[i]);
            else if(i==32) printf("\n");
            else printf("%02x", sig.s[i-32]);
        }
        printf("\n");
    }

    r = SDF_InternalVerify_ECC(session, index, msg, 32, &sig);
    if (r) {
        debug("verify err"); 
        printf("�������%x\n",r);
        goto end;
    }
    else {
        debug("verify ok");
    }





end:
    r = SDF_CloseSession(session);
    if (r) {
        debug("close session err");
    }
    else {
        debug("close session ok");
    }

    r = SDF_CloseDevice(dev);
    if (r) {
        debug("close dev err");
    }
    else {
        debug("close dev ok");
    }

    return 0;
}
