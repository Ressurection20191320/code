#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <openssl/rand.h>

#ifndef __SDF_H
#define __SDF_H
//定义设备信息结构
typedef struct DeviceInfo_st{
	unsigned char IssuerName[40]; //设备生产厂商名称
	unsigned char DeviceName[16]; 
	unsigned char DeviceSerial[16]; 
	unsigned int DeviceVersion; 
	unsigned int StandardVersion; 
	unsigned int AsymAlgAbility[2]; 
	unsigned int SymAlgAbilty; 
	unsigned int HashAlgAbility; 
	unsigned int BufferSize;
}DEVICEINFO;

// Error Code
#define SDR_OK 0x0   //操作成功

//********************************
//设备管理
//********************************

/*
功能：打开密码设备。
参数∶
phDeviceHandle[out] 返回设备句柄

返回值∶
   0   成功
  非0  失败，返回错误代码
*/
int SDF_OpenDevice(void ** phDeviceHandle);

/*
功能∶关闭密码设备，并释放相关资源。
参数∶
hDeviceHandle[in] 已打开的设备句柄
返回值∶ 
	0（SDR_OK）	成功
	非0	失败，返回错误代码
*/
int SDF_CloseDevice(void *hDeviceHandle);

/*


功能∶获取密码设备能力描述。;
参数∶
hSesionHandle[in]与设备建立的会话句柄 
pstDevceInfo [out]设备能力描述信息，内容及格式见设备信息定义
返回值∶ 
	0（SDR_OK）	成功
	非0	失败，返回错误代码
*/
int SDF_GetDeviceInfo( void * hSessionHandle, 
                       DEVICEINFO * pstDeviceInfo);



/*
功能：获取指定长度的随机数
参数：
 uiLength[in]  欲获取的随机数长度 
 pucRandom[ out] 缓冲区指针，用于存放获取的随机数
 返回值∶ 
	 00（SDR_OK）	成功
	非0	失败，返回错误代码
*/
int SDF_GenerateRandom (void * hSessionHandle, unsigned int uiLength, unsigned char * pucRandom);

#endif


//********************************
//设备管理
//********************************

int SDF_OpenDevice(void ** phDeviceHandle){
	return SDR_OK;
}


int SDF_CloseDevice(void *hDeviceHandle){
	
	return SDR_OK;
}

int SDF_GetDeviceInfo( void * hSessionHandle, DEVICEINFO * pstDeviceInfo) {
    
    DEVICEINFO di;
	strcpy(di.IssuerName,"LZH_SDF");
	strcpy(di.DeviceName,"SDFBESTI1913");
	strcpy(di.DeviceSerial,"2021040001");
	di.DeviceVersion = 1;
    //...

    //pstDevicelnfo = &di;
    *pstDeviceInfo = di;

	return SDR_OK;
}

static int myRandom(){
    int randint;//用来接收产生的随机数(用openssl重写了)
    RAND_bytes(&randint, sizeof(randint));

    return randint;
}

int getRandom(char *r, int length){
    
    return RAND_bytes(r,length);
}


int SDF_GenerateRandom (void * hSessionHandle, unsigned int uiLength, unsigned char * pucRandom){

	
    
    getRandom(pucRandom, uiLength);

	return SDR_OK;
}




int main(){

    void ** pdh;
	pdh = (void **) malloc(20);
	int ret;

    ret = SDF_OpenDevice(pdh);
	if(ret != SDR_OK){
        printf("error!");
	} else {
		printf("device opened!\n");
	}

    DEVICEINFO testdi;
    ret = SDF_GetDeviceInfo(pdh, &testdi);
	if(ret != SDR_OK){
        printf("error!");
	} else {
        printf("Issuer Name: %s\n", testdi.IssuerName);
        printf("Device Name: %s\n", testdi.DeviceName);
        printf("Device Serial: %s\n", testdi.DeviceSerial);
        printf("Device Version: %d\n", testdi.DeviceVersion);
		
	}

    char pRandom[20];

	printf("1个字节：");
    ret = SDF_GenerateRandom(*pdh,1, pRandom);
	if(ret != SDR_OK){
        printf("error!");
	} else {
        for(int i=0; i<1; i++)
            printf("%d\t", pRandom[i]);
	}

	sleep(1);
	printf("\n5个字节：");
    ret = SDF_GenerateRandom(*pdh,5, pRandom);
	if(ret != SDR_OK){
        printf("error!");
	} else {
        for(int i=0; i<5; i++)
            printf("%d\t", pRandom[i]);
	}
	sleep(1);
	printf("\n20个字节：");
    ret = SDF_GenerateRandom(*pdh,20, pRandom);
	if(ret != SDR_OK){
        printf("error!");
	} else {
        for(int i=0; i<20; i++)
            printf("%d\t", pRandom[i]);
	}
	printf("\n");

    ret = SDF_CloseDevice(*pdh);
	
	if(ret != SDR_OK){
        printf("error!");
	} else {
		free(pdh);
		printf("device closed!\n");
	}

	return 0;
} 


int main(){

    void ** pdh;
	pdh = (void **) malloc(20);
	int ret;

    ret = SDF_OpenDevice(pdh);
	if(ret != SDR_OK){
        printf("error!");
	} else {
		printf("device opened!\n");
	}

    DEVICEINFO testdi;
    ret = SDF_GetDeviceInfo(pdh, &testdi);
	if(ret != SDR_OK){
        printf("error!");
	} else {
        printf("Issuer Name: %s\n", testdi.IssuerName);
        printf("Device Name: %s\n", testdi.DeviceName);
        printf("Device Serial: %s\n", testdi.DeviceSerial);
        printf("Device Version: %d\n", testdi.DeviceVersion);
		
	}

    char pRandom[20];

	printf("1个字节：");
    ret = SDF_GenerateRandom(*pdh,1, pRandom);
	if(ret != SDR_OK){
        printf("error!");
	} else {
        for(int i=0; i<1; i++)
            printf("%d\t", pRandom[i]);
	}

	sleep(1);
	printf("\n5个字节：");
    ret = SDF_GenerateRandom(*pdh,5, pRandom);
	if(ret != SDR_OK){
        printf("error!");
	} else {
        for(int i=0; i<5; i++)
            printf("%d\t", pRandom[i]);
	}
	sleep(1);
	printf("\n20个字节：");
    ret = SDF_GenerateRandom(*pdh,20, pRandom);
	if(ret != SDR_OK){
        printf("error!");
	} else {
        for(int i=0; i<20; i++)
            printf("%d\t", pRandom[i]);
	}
	printf("\n");

    ret = SDF_CloseDevice(*pdh);
	
	if(ret != SDR_OK){
        printf("error!");
	} else {
		free(pdh);
		printf("device closed!\n");
	}

	return 0;
}
