#include "sdf.h"
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <openssl/rand.h>

//********************************
//设备管理
//********************************

int SDF_OpenDevice(void ** phDeviceHandle){
	return SDR_OK;
}


int SDF_CloseDevice(void *hDeviceHandle){
	
	return SDR_OK;
}

int SDF_GetDeviceInfo( void * hSessionHandle, DEVICEINFO * pstDeviceInfo) {
    
    DEVICEINFO di;
	strcpy(di.IssuerName,"LZH_SDF");
	strcpy(di.DeviceName,"SDFBESTI1913");
	strcpy(di.DeviceSerial,"2021040001");
	di.DeviceVersion = 1;
    //...

    //pstDevicelnfo = &di;
    *pstDeviceInfo = di;

	return SDR_OK;
}

static int myRandom(){
    int randint;//用来接收产生的随机数(用openssl重写了)
    RAND_bytes(&randint, sizeof(randint));

    return randint;
}

int getRandom(char *r, int length){
    
    return RAND_bytes(r,length);
}


int SDF_GenerateRandom (void * hSessionHandle, unsigned int uiLength, unsigned char * pucRandom){

	
    
    getRandom(pucRandom, uiLength);


    // for(int i=0; i<uiLength; i++){
    //     sleep(2); 
    //     *(pucRandom+i) = myRandom();
    //    // pucRandom[i] = i;
    // }

	return SDR_OK;
}


