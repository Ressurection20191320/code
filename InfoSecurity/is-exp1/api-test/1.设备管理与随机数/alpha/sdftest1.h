#ifndef __SDF_H
#define __SDF_H
//定义结构体
typedef struct Devicelnfo_st {
	unsigned char IssuerName[40];
	unsigned char DeviceName[16];
	unsigned char DeviceSerial[16];
	unsigned int DeviceVersion;
	unsigned int StandardVersion;
	unsigned int AsymAlgAbility[2];
	unsigned int SymAlgAbility;
	unsigned int HashAlgAbility;
	unsigned int BufferSize;
}DEVICEINFO;
//ERROR CODE
#define SDR_OK 0x0 //操作成功
#define SDR_BASE 0x01000000 //错误码基础值
#define SDR_UNKNOWERR SDR_BASE + 0x00000001 //未知错误
#define SDR_NOTSUPPORT SDR_BASE + 0x00000002 //不支持的接口调用
#define SDR_COMMFAI SDR_BASE + 0x00000003 //与设备通信失败
#define SDR_HARDFAIL SDR_BASE + 0x00000004 //运算模块无响应
#define SDR_OPENDEVICE SDR_BASE + 0x00000005//打开设备失败
#define SDR_OPENSESSION SDR_BASE + 0x0000006//创建会话失败
#define SDR_PARDENY SDR_BASE + 0x00000007 //无私钥使用权限
#define SDR_KEYNOTEXIST SDR_BASE + 0x00000008 //不存在的密钥调用
#define SDR_ALGNOTSUPPORT SDR_BASE + 0x00000009//不支持的算法调用
#define SDRALGMODNOTSUPPORT SDR_BASE + 0x0000000A//不支持的算法模式调用
#define SDR_PKOPER SDR_BASE + 0x0000000B//公钥运算失败


//******************************************
//设备管理
//******************************************
/*
功能：打开密码设备，返回设备句柄。
参数：
返回值：0(SDR OK)	成功
		非0		失败，返回错误代码
*/
int SDF_OpenDevice(void** phDeviceHandle);
/*
功能：关闭密码设备，并释放相关资源。
参数：hDeviceHandle[in]    已打开的设备句柄
返回值:0(SDR OK)	成功
		非0		失败，返回错误代码
*/
int SDF_CloseDevice(void* hDeviceHandle);
/*
功能： 创建与密码设备的会话。
已打开的设备句柄
hDeviceHandlein]参数：h
phessionHiandle[out]
返回与密码设备建立的新会话句柄成功
返回值：0(SDR OK)	成功
		非0		失败，返回错误代码
*/
int SDF_OpenSession(void* hDeviceHandle, void** phSessionHandle);

/*
功能：关闭与密码设备已建立的会话，并释放相关资源。
参数：hSessionHandle[in]	与密码设备已建立的会话句柄
返回值：0(SDR OK)	成功
		非0		失败，返回错误代码
*/
int SDF_CloseSesson(void* hSessionHandle);

/*
功能：获取密码设备能力描述。
参数：hSessionHandle[in]	与设备建立的会话句柄
	  pstDevicelnfo[our]	设备能力描述信息，内容及格式见设备信息定义成功
返回值：0(SDR OK)	成功
		非0		失败，返回错误代码
*/
int SDF_GetDeviceInfo(
	void* hSessionHandle,
	DEVICEINFO* pstDeviceInfo);

/*
功能：获取指定长度的随机数。
参数：
hSessonHandle[in]	与设备建立的会话句柄
uiLegth[in]	欲获取的随机数长度
pucRandom[out]	缓冲区指针，用于存放获取的随机数
返回值：0(SDR OK)	成功
		非0		失败，返回错误代码
*/
int SDF_GenerateRandom(
	void* hSessionHandle,
	unsigned int uiLength,
	unsigned char* pucRandom);

/*
功能：获取密码设备内部存储的指定索引私钥的使用权。
参数：
hSessionHandle[in]	与设备建立的会话句柄
uiKeyIndex[in]	密码设备存储私钥的索引值
pucPassword[in]	使用私钥权限的标识码
uiPwdLength[in]	私钥访问控制码长度，不少于8 字节
返回值：0(SDR OK)	成功
		非0		失败，返回错误代码
*/
int SDF_GetPrivateKeyAccessRight(
	void* hSessionHandle,
	unsigned int uiKeyIndex,
	unsigned char* pucPassword,
	unsigned int uiPwdLength);

/*
功能：释放密码设备存储的指定索引私钥的使用授权。
参数：
hSessonHandle[in]	与设备建立的会话句柄
uiKeyIndex[in]	密码设备存储私钥索引值成功
返回值∶0(SDR OK)	成功
		非0		失败，返回错误代码
*/
int SDF_ReleasePrivateKeyAccessRight(
	void* hSessionHandle,
	unsigned int uiKeyIndex);

#endif
