#include "SDF.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	int hSessionHandle = 0;
	int hKeyHandle = 1;
	unsigned int uiAlgID = SGD_SM4_CBC;
	unsigned char pucIV[16] = { 0xd4,0x2c,0x65,0xe1,0x8a,0xc4,0xcc,0x35,0xa6,0xbf,0x9d,0x74,0xea,0x76,0xda,0x22 };
	unsigned char pucData[] = "20191320";
	unsigned int uiDataLength = strlen((const char*)pucData);
	unsigned char pucEncData[1024];
	unsigned int puiEncDataLength;
	SDF_Encrypt((void*)hSessionHandle, (void*)hKeyHandle, uiAlgID, pucIV, pucData, uiDataLength, pucEncData, &puiEncDataLength);
	printf("明文为：%s\n", pucData);
	printf("密文长度为：%d\n密文为：\n", puiEncDataLength);
	for (int i = 0; i < puiEncDataLength; i++)
	{
		printf("0x%02x ", pucEncData[i]);
	}
	printf("\n");
	unsigned char pucDataagain[1024];
	unsigned int puiDataLength;
	SDF_Decrypt((void*)hSessionHandle, (void*)hKeyHandle, uiAlgID, pucIV, pucEncData, puiEncDataLength, pucDataagain, &puiDataLength);
	printf("密文为：\n");
	for (int i = 0; i < puiEncDataLength; i++)
	{
		printf("0x%02x ", pucEncData[i]);
	}
	printf("\n");
	printf("明文长度为：%d\n明文为：\n", puiDataLength);
	for (int i = 0; i < puiDataLength; i++)
	{
		printf("%c", pucDataagain[i]);
	}
	unsigned char pucMAC[1024];
	unsigned int puiMACLength;
	SDF_CalculateMAC((void*)hSessionHandle, (void*)hKeyHandle, uiAlgID, pucIV, pucData, uiDataLength, pucMAC, &puiMACLength);
	printf("明文为：%s\n", pucData);
	printf("MAC长度为：%d\nMAC为：\n", puiDataLength);
	for (int i = 0; i < puiEncDataLength; i++)
	{
		printf("0x%02x ", pucMAC[i]);
	}
	putchar('\n');
}