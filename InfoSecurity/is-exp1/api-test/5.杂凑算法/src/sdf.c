#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <openssl/evp.h>
#include "sdf.h"
#define ECC_MAX_DIGITS  4
#define SGD_SM3 1

EVP_MD_CTX* md_ctx;
const EVP_MD* md;

/********************************************/
//杂凑运算类函数
/********************************************/

/*
功能：杂凑运算初始化

参数：
	hSessionHandle[in]		与设备建立的会话句柄
	uiAlgID[in]				指定杂凑算法标识
	pucPublicKey[in]		签名者公钥。当uiAlgID为SGD_SM3时有效。
	pucID[in]				签名者的ID值，当uiAlgID为SGD_SM3时有效。
	uiIDLength[in]			签名者ID的长度，当uiAlgID为SGD_SM3时有效。

返回值：
	0		成功
	非0		失败，返回错误代码

*/


int SDF_HashInit(
	void* hSessionHandle,
	unsigned int uiAlgID,
	ECCrefPublicKey* pucPublicKey,
	unsigned char* pucID,
	unsigned int uiIDLength) {
	md = EVP_sm3();
	md_ctx = EVP_MD_CTX_new();
	EVP_DigestInit_ex(md_ctx, md, NULL);
	
	return SDR_OK;
	
}

/*
功能：多包杂凑运算

参数：
	hSessionHandle[in]		与设备建立的会话句柄
	pucData[in]				缓冲区指针﹐用于存放输入的数据明文
	uiDataLength[in]		输入的数据明文长度

返回值：
	0						成功
	非0						失败, 返回错误代码

*/

int SDF_HashUpdate(
	void* hSessionHandle,
	unsigned char* pucData,
	unsigned int uiDataLength) {
	

	EVP_DigestUpdate(md_ctx, pucData, uiDataLength);

	return SDR_OK;

}



/*
功能：杂凑函数结束

参数:
	hSessionHandle[in]			与设备建立的会话句柄
	pucHash[out]				缓冲区指针﹐用于存放输出的杂凑数据
	puiHashLength[out]			返回的杂凑数据长度

返回值：
	0							成功
	非0							失败,返回错误代码


*/

int SDF_HashFinal(
	void* hSessionHandle,
	unsigned char* pucHash,
	unsigned int* puiHashLength) {

	EVP_DigestFinal_ex(md_ctx, pucHash, puiHashLength);
	EVP_MD_CTX_free(md_ctx);
	return SDR_OK;
}




