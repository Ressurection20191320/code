#pragma once
#ifndef _SDF_H
#define _SDF_H


//定义设备信息结构
typedef struct DeviceInfo_st {
	unsigned char IssuerName[40];//设备生产厂商名称
	unsigned char DeviceName[16];//设备型号
	unsigned char DeviceSerial[16];//设备编号
	//包含：日期(8字符)、批次号(3字符)、流水号(5字符)
	unsigned int DeviceVersion; //密码设备内部软件的版本号
	unsigned int StandardVersion; //密码设备支持的接口规范版本号
	unsigned int AsymAlgAbility[2];
	//前４字节表示支持的算法，表示方法为非对称算法标识 按位或的结果
	//后４字节表示算法的最大模长，表示方法为支持的模长按位或的结果
	unsigned int SymAlgAbility;//所有支持的对称算法，表示方法为对称算法标识按位或运算结果
	unsigned int HashAlgAbility;//所有支持的杂凑算法，表示方法为杂凑算法标识按位或运算结果
	unsigned int BufferSize;//支持的最大文件存储空间（单位字节）
}DEVICEINFO;


//Error Code
#define		SDR_OK					0x0						//操作成功
#define		SDR_BASE				ox01000000				//错误码基础值
#define		SDR_UNKNOWERR			SDR_BASE+0x00000001		//未知错误
#define		SDR_NOTSUPPORT			SDR_BASE+0x00000002		//不支持的接口调用
#define		SDR_COMMFAIL			SDR_BASE+0x00000003		//与设备通信失败
#define		SDR_HARDFAIL			SDR_BASE+0x00000004		//运算模块无响应
#define		SDR_OPENDEVICE			SDR_BASE+0x00000005		//打开设备失败
#define		SDR_OPENSESSION			SDR_BASE+0x00000006		//创建会话失败
#define		SDR_PARDENY				SDR_BASE+0x00000007		//无私钥使用权限
#define		SDR_KEYNOTEXIST			SDR_BASE+0x00000008		//不存在的密钥调用
#define		SDR_ALGNOTSUPPORT		SDR_BASE+0x00000009		//不支持的算法调用
#define		SDR_ALGMODNOTSUPPORT	SDR_BASE+0x0000000A		//不支持的算法模式调用
#define		SDR_PKOPERR				SDR_BASE+0x0000000B		//公钥运算失败
#define		SDR_SKOPERR				SDR_BASE+0x0000000C		//私钥运算失败
#define		SDR_SIGNERR				SDR_BASE+0x0000000D		//签名运算失败
#define		SDR_VERIFYERR			SDR_BASE+0x000000OE		//验证签名失败
#define		SDR_SYMOPERR			SDR_BASE+0x0000000F		//对称算法运算失败
#define		SDR_STEPERR				SDR_BASE+0x00000010		//多步运算步骤错误
#define		SDR_FILESIZEERR			SDR_BASE+0x00000011		//文件长度超出限制
#define		SDR_FILENOEXIST			SDR_BASE+0x00000012		//指定的文件不存在
#define		SDR_FILEOFSERR			SDR_BASE+0x00000013		//文件起始位置错误
#define		SDR_KEYTYPEERR			SDR_BASE+0x00000014		//密钥类型错误
#define		SDR_KEYERR				SDR_BASE+0x00000015		//密钥错误
#define		SDR_ENCDATAERR			SDR_BASE+0x00000016		//ECC加密数据错误
#define		SDR_RANDERR				SDR_BASE+0x00000017		//随机数产生失败
#define		SDR_PRKRERR				SDR_BASE+0x00000018		//私钥使用权限获取失败
#define		SDR_MACERR				SDR_BASE+0x00000019		//MAC运算失败
#define		SDR_FILEEXISTS			SDR_BASE+0x0000001A		//指定文件已存在
#define		SDR_FILEWERR			SDR_BASE+0x0000001B		//文件写入失败
#define		SDR_NOBUFFER			SDR_BASE+0x0000001C		//存储空间不足
#define		SDR_INARGERR			SDR_BASE+0x0000001D		//输入参数错误
#define		SDR_OUTARGERR			SDR_BASE+0x0000001E		//输出参数错误

#define		u32						unsigned int 
#define		u16						unsigned short 
#define		s32						int 
#define		s16						short int 
#define		u8						unsigned char 
#define		s8						char


//ECC密钥数据结构定义
#define		ECCref_MAX_BITS		512
#define		ECCref_MAX_LEN		((ECCref_MAX_BITS + 7) / 8)
typedef struct ECCrefPublicKey_st
{
	unsigned int bits;
	unsigned char x[ECCref_MAX_LEN];
	unsigned char y[ECCref_MAX_LEN];
}ECCrefPublicKey;

typedef struct ECCrefPrivateKey_st {
		unsigned int bits;
		unsigned char K[ECCref_MAX_LEN];
}ECCrefPrivateKey;

typedef 

#ifdef  __cplusplus
extern "C" {
#endif

	int sm3_hash(const unsigned char* message, size_t len, unsigned char* hash, unsigned int* hash_len);

#ifdef  __cplusplus
}
#endif


#endif

