#pragma once

#define SDR_OK 0x0      // 操作成功
#define SDR_BASE 0x01000000      // 错误码基础值
#define SDR_UNKOWNERR SDR_BASE + 0x00000001      // 未知错误
#define SDR_NOTSUPPORT SDR_BASE + 0x00000002      // 不支持的接口调用
#define SDR_COMMFAIL SDR_BASE + 0x00000003      // 与设备通信失败
#define SDR_HARDFAIL SDR_BASE + 0x00000004      // 运算模块无响应
#define SDR_OPENDEVICE SDR_BASE + 0x00000005      // 打开设备失败
#define SDR_OPENSESSION SDR_BASE + 0x00000006      // 创建会话失败
#define SDR_PARDENY SDR_BASE + 0x00000007      // 无私钥使用权限
#define SDR_KEYNOTEXIST SDR_BASE + 0x00000008      // 不存在的密钥调用
#define SDR_ALGNOTSUPPORT SDR_BASE + 0x00000009      // 不支持的算法调用
#define SDR_ALGMODNOTSUPPORT SDR_BASE + 0x0000000A      // 不支持的算法模式调用
#define SDR_PKOPERR SDR_BASE + 0x0000000B      // 公钥运算失败
#define SDR_SKOPERR SDR_BASE + 0x0000000C      // 私钥运算失败
#define SDR_SIGNERR SDR_BASE + 0x0000000D      // 签名运算失败
#define SDR_VERIFYERR SDR_BASE + 0x0000000E      // 验证签名失败
#define SDR_SYMOPERR SDR_BASE + 0x0000000F      // 对称算法运算失败
#define SDR_STEPERR SDR_BASE + 0x00000010      // 多步运算步骤错误
#define SDR_FILESIZEERR SDR_BASE + 0x00000011      // 文件长度超出限制
#define SDR_FILENOTEXIST SDR_BASE + 0x00000012      // 指定的文件不存在
#define SDR_FILEOFSERR SDR_BASE + 0x00000013      // 文件起始位置错误
#define SDR_KEYTYPEERR SDR_BASE + 0x00000014      // 密钥类型错误
#define SDR_KEYERR SDR_BASE + 0x00000015      // 密钥错误
#define SDR_ENCDATAERR SDR_BASE + 0x00000016      // ECC加密数据错误
#define SDR_RANDERR SDR_BASE + 0x00000017      // 随机数产生失败
#define SDR_PRKRERR SDR_BASE + 0x00000018      // 私钥使用权限获取失败
#define SDR_MACERR SDR_BASE + 0x00000019      // MAC运算失败
#define SDR_FILEEXISTS SDR_BASE + 0x0000001A      // 指定文件已存在
#define SDR_FILEWERR SDR_BASE + 0x0000001B      // 文件写入失败
#define SDR_NOBUFFER SDR_BASE + 0x0000001C      // 存储空间不足
#define SDR_INARGERR SDR_BASE + 0x0000001D      // 输入参数错误
#define SDR_OUTARGERR SDR_BASE + 0x0000001E      // 输出参数错误

typedef struct DeviceInfo_st {
    unsigned char IssuerName[40];
    unsigned char DeviceName[16];
    unsigned char DeviceSerial[16];
    unsigned int DeviceVersion;
    unsigned int StandardVersion;
    unsigned int AsymAlgAbility[2];
    unsigned int SymAlgAbility;
    unsigned int HashAlgAbility;
    unsigned int BufferSize;
}DEVICEINFO;

#define RSAref_MAX_BITS     2048
#define RSAref_MAX_LEN      ((RSAref_MAX_BITS + 7) / 8)
#define RSAref_MAX_PBITS    ((RSAref_MAX_BITS + 1) / 2)
#define RSAref_MAX_PLEN     ((RSAref_MAX_PBITS + 7) / 8)
typedef struct RSArefPublicKey_st {
    unsigned int bits;
    unsigned char m[RSAref_MAX_LEN];
    unsigned char e[RSAref_MAX_LEN];
}RSArefPublicKey;

typedef struct RSArefPrivateKey_st {
    unsigned int bits;
    unsigned char m[RSAref_MAX_LEN];
    unsigned char e[RSAref_MAX_LEN];
    unsigned char d[RSAref_MAX_LEN];
    unsigned char prime[2][RSAref_MAX_PLEN];
    unsigned char pexp[2][RSAref_MAX_PLEN];
    unsigned char coef[RSAref_MAX_PLEN];
}RSArefPrivateKey;

#define ECCref_MAX_BITS     256
#define ECCref_MAX_LEN      ((ECCref_MAX_BITS + 7) / 8)
typedef struct ECCrefPublicKey_st {
    unsigned int bits;
    unsigned char x[ECCref_MAX_LEN];
    unsigned char y[ECCref_MAX_LEN];
}ECCrefPublicKey;

typedef struct ECCrefPrivateKey_st {
    unsigned int bits;
    unsigned char D[ECCref_MAX_LEN];
}ECCrefPrivateKey;

typedef struct ECCCipher_st
{
    unsigned char x[ECCref_MAX_LEN];
    unsigned char y[ECCref_MAX_LEN];
    unsigned char C[ECCref_MAX_LEN];
    unsigned char M[ECCref_MAX_LEN];
}ECCCipher;

typedef struct ECCSignature_st {
    unsigned char r[ECCref_MAX_LEN];
    unsigned char s[ECCref_MAX_LEN];
}ECCSignature;

// typedef struct SDF_ENVELOPEDKEYBLOB{
//     unsigned long ulAsymmAlgID;
//     unsigned long ulSymmAlgID;
//     ECCCIPHERBLOB ECCCipherBlob;
//     ECCPUBLICKEYBLOB PubKey;
//     unsigned char cbEncryptedPrivKey[64];
// }ENVELOPEDKEYBLOB, *PENVELOPEDKEYBLOB;

int SDF_OpenDevice(void** phDeviceHandle);
int SDF_CloseDevice(void* hDeviceHandle);
int SDF_OpenSession(void* hDeviceHandle, void** phSessionHandle);
int SDF_CloseSession(void* hSessionHandle);
int SDF_GetDeviceInfo(void* hSessionHandle, DEVICEINFO* pstDeviceInfo);
int SDF_GenerateRandom(void* hSessionHandle, unsigned int uiLength, unsigned char* pucRandom);

int SDF_ExportSignPublicKey_ECC(void* hSessionHandle, unsigned int uiKeyIndex, ECCrefPublicKey* pucPublicKey);
int SDF_ExportEncPublicKey_ECC(void* hSessionHandle, unsigned int uiKeyIndex, ECCrefPublicKey* pucPublicKey);
int SDF_GenerateKeyPair_ECC(void* hSessionHandle, unsigned int uiAlgID, unsigned int uiKeyBits, ECCrefPublicKey* pucPublicKey, ECCrefPrivateKey* pucPrivateKey);

int SDF_ExternalVerify_ECC(void* hSessionHandle, unsigned int uiAlgID, ECCrefPublicKey* pucPublicKey, unsigned char* pucDataInput, unsigned int uiInputLength, ECCSignature* pucSignature);
int SDF_InternalSign_ECC(void* hSessionHandle, unsigned int uiISKIndex, unsigned char* pucData, unsigned int uiDataLength, ECCSignature* pucSignature);
int SDF_InternalVerify_ECC(void* hSessionHandle, unsigned int uiISKIndex, unsigned char* pucData, unsigned int uiDataLength, ECCSignature* pucSignature);
int SDF_ExternalEncrypt_ECC(void* hSessionHandle, unsigned int uiAlgID, ECCrefPublicKey* pucPublicKey, unsigned char* pucData, unsigned int uiDataLength, ECCCipher* pucEncData);
int SDF_ExternalDecrypt_ECC(void* hSessionHandle, unsigned int uiAlgID, ECCrefPrivateKey* pucPrivateKey, ECCCipher* pucEncData, unsigned char* pucData, unsigned int* puiDataLength);

int SDF_Encrypt(void* hSessionHandle, void* hKeyHandle, unsigned int uiAlgID, unsigned char* pucIV, unsigned char* pucData, unsigned int uiDataLength, unsigned char* pucEncData, unsigned int* puiEncDataLength);
int SDF_Decrypt(void* hSessionHandle, void* hKeyHandle, unsigned int uiAlgID, unsigned char* pucIV, unsigned char* pucEncData, unsigned int uiEncDataLength, unsigned char* pucData, unsigned int* puiDataLength);
int CalculateMAC(void* hSessionHandle, void* hKeyHandle, unsigned int uiAlgID, unsigned char* pucIV, unsigned char* pucData, unsigned int uiDataLength, unsigned char* pucMAC, unsigned int* puiMACLength);

int SDF_HashInit(void* hSessionHandle, unsigned int uiAlgID, ECCrefPublicKey* pucPublicKey, unsigned char* pucID, unsigned int uiDLength);
int SDF_HashUpdate(void* hSession, unsigned char* pucData, unsigned int uiDataLength);
int SDF_HashFinal(void* hSessionHandle, unsigned char* pucHash, unsigned int* puiHashLength);
