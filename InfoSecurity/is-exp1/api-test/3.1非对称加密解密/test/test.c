#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sdf.h"

int main()
{
	void *hSessionHandle = NULL;
	unsigned int uiAlgID = 1;

	ECCrefPublicKey pucPublicKey;
	ECCCipher pucEncData;
	ECCrefPrivateKey pucPrivateKey;
	pucPublicKey.bits = 64;
	char pub_key_x[] = {0x78, 0x80, 0x90, 0x4c, 0xc6, 0x24, 0xa5, 0x60,
						0x42, 0xe1, 0xc3, 0x7d, 0xdc, 0xf3, 0xc0, 0x27,
						0x41, 0x91, 0xf6, 0x08, 0x8f, 0x22, 0x48, 0xc8,
						0x8e, 0x38, 0xf7, 0x91, 0xe7, 0x0a, 0xfb, 0xf0};
	char pub_key_y[] = {0x33, 0x2d, 0xd7, 0x96, 0xee, 0xee, 0xaf, 0x45,
						0xf1, 0x26, 0xaf, 0x1e, 0xf9, 0x8e, 0xa8, 0xa0,
						0xe7, 0x78, 0xa0, 0x9f, 0x00, 0xd5, 0x8f, 0xe2,
						0xbd, 0xed, 0xd2, 0xa1, 0xa1, 0xdf, 0xbe, 0xa5};
	char pri_key[] = {0x7d, 0xcd, 0x5a, 0xd7, 0x6a, 0x2a, 0x68, 0xf4,
					  0x8b, 0xc9, 0x42, 0x8f, 0x12, 0x70, 0xb4, 0x92,
					  0x6e, 0x5b, 0x50, 0x59, 0x72, 0x12, 0x9e, 0x5c,
					  0x9e, 0x01, 0x77, 0xa4, 0x70, 0xd0, 0xef, 0xe8};
	int i;
	for (i = 0; i < 32; i++)
	{
		pucPublicKey.x[i] = pub_key_x[i];
		pucPublicKey.y[i] = pub_key_y[i];
	}
	pucPrivateKey.bits = 32;
	;
	for (i = 0; i < 32; i++)
	{
		pucPrivateKey.D[i] = pri_key[i];
	}

	unsigned char msg[] = {"20191320"};
	int msg_len = (int)(strlen((char *)msg));
	unsigned char *out = (unsigned char *)malloc(sizeof(msg));
	unsigned int out_len = 0;

	SDF_ExternalEncrypt_ECC(hSessionHandle, uiAlgID, &pucPublicKey, msg, msg_len, &pucEncData);
	/*
	 * print the output
	 */
	printf("The value of x in C1 component:\n");
	for (i = 0; i < 32; i++)
	{
		printf("0x%02x ", pucEncData.x[i]);
	}
	printf("\n\n");
	printf("The value of y in C1 component:\n");
	for (i = 0; i < 32; i++)
	{
		printf("0x%02x ", pucEncData.y[i]);
	}
	printf("\n\n");
	printf("C2 component:\n");
	for (i = 0; i < msg_len; i++)
	{
		printf("0x%02x ", pucEncData.C[i]);
	}
	printf("\n\n");
	printf("C3 component:\n");
	for (i = 0; i < 32; i++)
	{
		printf("0x%02x ", pucEncData.M[i]);
	}
	printf("\n\n");

	SDF_ExternalDecrypt_ECC(hSessionHandle, uiAlgID, &pucPrivateKey, &pucEncData, out, &out_len);
	/*
	 * print the output
	 */
	printf("原始数据:\n");
	for (i = 0; i < msg_len; i++)
	{
		printf("0x%02x ", msg[i]);
	}
	printf("\n\n");
	printf("解密输出:\n");
	// printf("%d\n\n", out_len);
	for (i = 0; i < out_len; i++)
	{
		printf("0x%02x ", out[i]);
	}
	printf("\n(%s)\n\n",out);

	return 0;
}