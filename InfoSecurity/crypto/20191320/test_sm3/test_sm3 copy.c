#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

int sm3_hash(const unsigned char *message, size_t len, unsigned char *hash, unsigned int *hash_len)
{
    EVP_MD_CTX *md_ctx;
    const EVP_MD *md;

    md = EVP_sm3();
    md_ctx = EVP_MD_CTX_new();
    EVP_DigestInit_ex(md_ctx, md, NULL);
    EVP_DigestUpdate(md_ctx, message, len);
    EVP_DigestFinal_ex(md_ctx, hash, hash_len);
    EVP_MD_CTX_free(md_ctx);
    return 0;
}

int main(void)
{
    unsigned char sample1[]="20191201 20191202 20191203 20191204 20191205 20191206 20191207 20191208 20191209 20191210 20191211 20191212 20191213 20191214 20191215 20191216 20191217 20191218 20191219 20191220 20191221 20191222 20191223 20191224 20191225 20191226 20191227 20191228 20191229 20191230 20191231 20191232 20191301 20191302 20191303 20191304 20191305 20191306 20191307 20191308 20191309 20191310 20191311 20191312 20191313 20191314 20191315 20191316 20191317 20191318 20191319 20191320 20191321 20191322 20191323 20191324 20191325 20191326 20191327 20191328 20191329 20191330 20191331";
    
    unsigned int sample1_len = strlen((char *)sample1);

    

    unsigned char hash_value[64];
    unsigned int i, hash_len;

    sm3_hash(sample1, sample1_len, hash_value, &hash_len);
    printf("哈希值长度: %d字节\n", hash_len);
    printf("字符串哈希结果:\n");
    for (i = 0; i < hash_len; i++)
    {
        printf("%x", hash_value[i]);
    }
    printf("\n\n");

    return 0;
}