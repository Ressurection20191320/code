#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>

int sm3_hash(const unsigned char *message, size_t len, unsigned char *hash, unsigned int *hash_len)
{
    EVP_MD_CTX *md_ctx;
    const EVP_MD *md;

    md = EVP_sm3();
    md_ctx = EVP_MD_CTX_new();
    EVP_DigestInit_ex(md_ctx, md, NULL);
    EVP_DigestUpdate(md_ctx, message, len);
    EVP_DigestFinal_ex(md_ctx, hash, hash_len);
    EVP_MD_CTX_free(md_ctx);
    return 0;
}

int main(void)
{
    unsigned char sample1[128];
    printf("请输入要计算哈希值的字符串(长度小于127个字符):");
    scanf("%s", &sample1);
    unsigned int sample1_len = strlen((char *)sample1);

    // FILE *fp;
    // printf("将计算test_hash.dat(大小小于1024字节)的哈希值。");
    // fp = fopen("test_sm3.dat", "rb");
    // unsigned char sample2[1024];
    // unsigned int sample2_len;
    // if (fp == NULL)
    // {
    //     printf("无法打开文件test_sm3.dat。");
    // }
    // else
    // {
    //     sample2_len = fread(sample2, 1, 1024, fp);
    // }

    unsigned char hash_value[64];
    unsigned int i, hash_len;

    sm3_hash(sample1, sample1_len, hash_value, &hash_len);
    printf("输入的字符串: %s\n", sample1);
    printf("哈希值长度: %d字节\n", hash_len);
    printf("字符串哈希结果:\n");
    for (i = 0; i < hash_len; i++)
    {
        printf("%x", hash_value[i]);
    }
    printf("\n\n");

    // if (fp != NULL) //如果文件指针不为空
    // {
    //     sm3_hash(sample2, sample2_len, hash_value, &hash_len);
    //     printf("成功计算文件哈希值!\n");
    //     printf("哈希值长度: %d字节\n", hash_len);
    //     printf("文件哈希值结果:\n");
    //     for (i = 0; i < hash_len; i++)
    //     {
    //         printf("%x", hash_value[i]);
    //     }
    //     printf("\n");
    // }
    return 0;
}