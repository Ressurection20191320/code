	.text
	.global sub, a, b, printf
sub:
	push	%rbp
	mov	%rsp, %rbp
	
	push	a
	push	b
	movl	-8(%rbp),%esi
	movl	-16(%rbp),%edx	
	movl	$fmt,%edi

	call	printf
	
	add	$16, %rsp
	mov	%rbp, %rsp
	pop	%rbp
	ret
	
	.data
fmt:	.asciz	"a=%d,b=%d\n"
