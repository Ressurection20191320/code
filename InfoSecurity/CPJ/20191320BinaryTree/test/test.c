#include"btree.h"
#include"queue.h"
#include<stdio.h>

int main()
{
    BinTree T;
    printf("请以先序,字符型(不加空格,空用\"#\"表示)输入二叉树:\n");
    T = CreateBinTree();
    for(int i=0;i<10;i++){
	    printf("i=%d\t",i);
    }
    printf("以先序输出二叉树结果如下:\n");
    PreOrder(T);
    printf("\n以中序输出二叉树结果如下:\n");
    InOrder(T);
    printf("\n以后序输出二叉树结果如下:\n");
    PostOrder(T);
    printf("\n以层序输出二叉树结果如下:\n");
    LevelOrder(T);
    putchar('\n');
    return 0;
}
