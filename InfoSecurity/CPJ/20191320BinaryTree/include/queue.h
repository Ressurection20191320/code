#ifndef __QUEUE_H__
#define __QUEUE_H__
#define MAXSIZE 100
#include "btree.h"

typedef struct SeqQueue
{
    int max;
    int f, r;
    BinTree *q;
} * Queue;      //队列结构定义

Queue CreateQueue(int m);//创建队列
void AddQueue(Queue paqu, BinTree x);//将一个二叉树指针压入队列
int IsEmptyQueue(Queue paqu);//判断队列是否为空
BinTree DeleteQueue(Queue paqu);//从队尾删除并取出元素

#endif
