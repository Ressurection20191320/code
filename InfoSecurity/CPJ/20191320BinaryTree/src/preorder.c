#include"btree.h"
#include<stdio.h>

void PreOrder(BinTree t){
    if(t != NULL)
    {
        printf("%c",t->data);
        PreOrder(t->Left);
        PreOrder(t->Right);
    }
}
