#include"queue.h"
#include"btree.h"
#include<stdio.h>
#include<stdlib.h>


Queue CreateQueue(int m)
{
    Queue pQ;
    pQ = (Queue)malloc(sizeof(struct SeqQueue));
    pQ->q = (BinTree *)malloc(sizeof(BinTree) * m);
    pQ->f = pQ->r = 0;
    pQ->max = m;
    return pQ;
}//创建队列

void AddQueue(Queue paqu, BinTree x)
{
    if ((paqu->r + 1) % paqu->max == paqu->f)
        printf("Queue is full!\n");
    else
    {
        paqu->q[paqu->r] = x;
        paqu->r++;
        paqu->r %= paqu->max;
    }
}//将一个二叉树指针压入队列

int IsEmptyQueue(Queue paqu)
{
    return (paqu->r == paqu->f);
}//判断队列是否为空

BinTree DeleteQueue(Queue paqu)
{
    int t = paqu->f;
    paqu->f++;
    paqu->f %= paqu->max;
    return (paqu->q[t]);
}//从队尾删除并取出元素
