#include <iostream>
#include <gmpxx.h>
using namespace std;

int main()
{
        mpz_t a,b,c;
        mpz_init_set_str(a,"2",10);
        mpz_init_set_str(b,"2",10);
        for(int i=0;i<=1209;i++)
        {
                mpz_mul(b,b,a);
        }
        gmp_printf("%Zd\n",b);
        mpz_clear(a);
        mpz_clear(b);
        return 0;
}