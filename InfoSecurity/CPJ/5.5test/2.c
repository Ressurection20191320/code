#include <gmp.h>
#include <string.h>
int main(int argc, const char *argv[])
{
    mpz_t z_r, z_1320, z_1;
    mpz_init_set_str(z_r, "1", 10);
    mpz_init_set_str(z_1320, "20191315", 10);
    mpz_init_set_str(z_1, "1", 10);
    int i;
    for (i = 1315; i <= 1325; i++)
    {
        mpz_mul(z_r, z_r, z_1320);
        mpz_add(z_1320,z_1320,z_1);
    }
    gmp_printf("%Zd\n",z_r);
    mpz_clear(z_r);
    mpz_clear(z_1320);
    mpz_clear(z_1);
    getchar();
    return 0;
}