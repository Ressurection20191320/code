#include <gmp.h>
#include <string.h>
int main(int argc, const char *argv[])
{
    mpz_t z_r, z_2;
    mpz_init_set_str(z_r, "1", 10);
    mpz_init_set_str(z_2, "2", 10);
    int i;
    for (i = 0; i < 1320; i++)
    {
        mpz_mul(z_r, z_r, z_2);
    }
    gmp_printf("%Zd\n",z_r);
    mpz_clear(z_r);
    mpz_clear(z_2);
    getchar();
    return 0;
}