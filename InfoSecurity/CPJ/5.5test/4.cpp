#include <gmp.h>
#include <string.h>
int main(int argc, const char *argv[])
{
    mpz_t z_r, z_1210, z_1;
    mpz_init_set_str(z_r, "1", 10);
    mpz_init_set_str(z_1210, "1205", 10);
    mpz_init_set_str(z_1, "1", 10);
    int i;
    for (i = 1210; i <= 1220; i++)
    {
        mpz_mul(z_r, z_r, z_1210);
        mpz_add(z_1210,z_1210,z_1);
    }
    gmp_printf("%Zd\n",z_r);
    mpz_clear(z_r);
    mpz_clear(z_1210);
    mpz_clear(z_1);
    getchar();
    return 0;
}