#include <stdio.h>
#include <string.h>
#include "sm3hash.h"

int main(void)
{
    const unsigned char sample[] = "20191320lzh";
    unsigned int sample_len = sizeof(sample);
    unsigned char hash_value[64];
    unsigned int i, hash_len;
    sm3_hash(sample, sample_len, hash_value, &hash_len);
   
	printf("源数据:");
    for (i = 0; i < sample_len; i++)
    {
        printf("%c", sample[i]);
    }
    printf("\n");
    printf("哈希长度: %d bytes.\n", hash_len);
    printf("SM3哈希值:\n");
    for (i = 0; i < hash_len; i++)
    {
        printf("%x ", hash_value[i]);
    }
    printf("\n");

    return 0;
}
