#include "myod.h"
#include <stdio.h>

char filestr[256];

void myod(){
	int i=0,j=0,k,flag=1;
	while(1){
		if(filestr[i]=='\0'&&filestr[i-1]!='\0'){
			flag=0;
			k=i;
		}
		if(i%16==0){
			if(flag)
				k=i;
			printf("\n%08x",k*2);
			if(!flag)
				break;
		}
		if(i%4==0)
			printf("\t%02x",filestr[i+3]);
		if(i%4==1)
			printf("%02x",filestr[i+1]);
		if(i%4==2)
			printf("%02x",filestr[i-1]);
		if(i%4==3)
			printf("%02x",filestr[i-3]);
		if(i%16==15){
			printf("\n\t");
			for(;j<=i&&filestr[j]!='\0';j++){
				if(filestr[j]!='\n')
					printf("   %c",filestr[j]);
				else
					printf("   \\n");
			}
		}
		i++;
	}
	putchar('\n');
}
