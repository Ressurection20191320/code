#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>

int main()
{
    BIGNUM * bn_rlt;
    BIGNUM * bn_1;
    BIGNUM * bn_1320;
    BN_CTX * ctx;
    char* show;

    ctx=BN_CTX_new();
    bn_rlt=BN_new();
    bn_1=BN_new();
    bn_1320=BN_new();

    BN_dec2bn(&bn_rlt,"1");
    BN_dec2bn(&bn_1,"1");
    BN_dec2bn(&bn_1320,"20191315");

    for(int i=1315;i<=1325;i++){
        BN_mul(bn_rlt,bn_rlt,bn_1320,ctx);
        BN_add(bn_1320,bn_1320,bn_1);
    }
    
    show=BN_bn2dec(bn_rlt);
    printf("%s\n",show);
    BN_free(bn_rlt);
    BN_free(bn_1);
    BN_free(bn_1320);
    BN_CTX_free(ctx);
    return 0;
}