#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bn.h>

int main()
{
    BIGNUM * bn_rlt;
    BIGNUM * bn_2;
    BN_CTX * ctx;
    char* show;
    ctx=BN_CTX_new();
    bn_rlt=BN_new();
    bn_2=BN_new();
    BN_dec2bn(&bn_rlt,"1");
    BN_dec2bn(&bn_2,"2");
    for(int i=0;i<1320;i++){
        BN_mul(bn_rlt,bn_rlt,bn_2,ctx);
    }
    show=BN_bn2dec(bn_rlt);
    printf("%s\n",show);
    BN_free(bn_rlt);
    BN_free(bn_2);
    BN_CTX_free(ctx);
    return 0;
}