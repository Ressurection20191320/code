#include "utils.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{

	unsigned char c;
	unsigned char i;
	Hex2Char(1, &c);
	//结果：c = '1'
	Hex2Char(15, &c);
	//结果：c = 'F'
	Char2Hex('1', &i);
	//结果：i = 1
	Char2Hex('F', &i);
	//结果：i = 15
	return 0;
}
