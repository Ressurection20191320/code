<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>个人简历</title>
</head>
<body>
<h1 style="text-align: center">个人简历</h1>
<table  border="1"
		   cellspacing="0px"
		   style="margin:auto;"
		   width="800px">
	<tr height="50"  style="text-align: center;">
		<td width="15%">姓名</td>
		<td width="25%">李泽昊</td>
		<td width="15%">姓别</td>
		<td width="25%">男</td>
		<td  rowspan="4"><img src="./2.jpg" height="100px"></td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td >出生年月</td>
		<td >2000年11月</td>
		<td >民族</td>
		<td >汉</td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td >政治面貌</td>
		<td >共青团员</td>
		<td >籍贯</td>
		<td >四川</td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td >毕业院校</td>
		<td >北京电子科技学院</td>
		<td >专业</td>
		<td >信息安全</td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td >联系电话</td>
		<td >17396201719</td>
		<td >地址</td>
		<td  colspan="3">丰台区北京电子科技学院</td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td >求职意向</td>
		<td  colspan="4">求职意向求职意向求职意向</td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td >学习经历</td>
		<td  colspan="4" style="text-align:left"><ul>
				<li>20xx</li>
				<li>20xx</li>
				<li>20xx</li>
			</ul></td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td  rowspan="5">工作经历</td>
		<td>起止时间</td>
		<td>工作单位与职务</td>
		<td colspan="2">岗位职责与业绩表现</td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td>没有工作过QAQ</td>
		<td></td>
		<td colspan="2"></td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td></td>
		<td></td>
		<td colspan="2"></td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td></td>
		<td></td>
		<td colspan="2"></td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td></td>
		<td></td>
		<td colspan="2"></td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td >所得奖项</td>
		<td  colspan="4" style="text-align:left"><ol>
				<li>20xx</li>
				<li>20xx</li>
				<li>20xx</li>
			</ol></td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td >计算机水平</td>
		<td  colspan="4">无</td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td >英语水平</td>
		<td  colspan="4">4级</td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td >兴趣爱好</td>
		<td  colspan="4">广泛</td>
	</tr>
	<tr height="50"  style="text-align: center;">
		<td >自我评价</td>
		<td  colspan="4">呵呵</td>
	</tr>
</table>
</body>
</html>