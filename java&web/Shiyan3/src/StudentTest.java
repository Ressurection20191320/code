class Student {
	private String name;
	private int age;
	private int id;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", id=" + id + "]";
	}
}

public class StudentTest {
	public static void main(String[] args) {
		Student s = new Student();
		s.setName("lzh");
		s.setAge(19);
		s.setId(20191320);

		System.out.println(extracted(s));
	}

	private static String extracted(Student s) {
		return "学生信息：姓名：" + s.getName() + " 年龄：" + s.getAge() + " 学号：" + s.getId();
	}
}
