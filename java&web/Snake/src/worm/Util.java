package worm;

import java.awt.Color;

public class Util {
	public static final int FOOD_WIDTH =20 ;
	public static final int FOOD_HEIGHT =20 ;
	public static final Color FOOD_COLOR = Color.red ;
	public static final int CELL_WIDTH = 20 ;
	public static final int CELL_HEIGHT = 20 ;
	public static final Color CELL_COLOR = Color.blue ;
	
	public static final int STAGE_ROWS = 20 ;
	public static final int STAGE_COLS = 20 ;
	
}
