package worm;

import java.util.Arrays;

public class Worm {
	private Cell [] cells ;
	private int dir ; //蛇默认走的方向
	
	//定义蛇走的四个方向
	public static final int UP = -10 ; 
	public static final int DOWN = 10 ;
	public static final int LEFT = -1 ;
	public static final int RIGHT = 1 ;
	
	public Worm(){
		cells = new Cell[]{
				new Cell(5,5),
				new Cell(5,6),
				new Cell(5,7),
				new Cell(6,7),
				new Cell(7,7),
				new Cell(7,8),
				new Cell(7,9)
				
		};
		dir = LEFT ;
	}
	
	/**
	 *走一步
	 *1.判断给定的方向跟当前的方向是否相反
	 *  如果不相反：
	 *   *** 记录当前的方向
	 *  1.取出当前蛇的头节点
	 *  2.根据给定的方向计算新的节点
	 *  3.判断是否吃掉食物
	 *      如果吃掉食物：
	 *        1.扩展数组的长度
	 *        2.平移数组的元素
	 *        3.将新节点放入数组[0]的位置
	 *        4.记录吃掉食物
	 *      如果没有吃掉食物：
	 *        1.平移数组的元素
	 *        2.将新节点放入数组[0]的位置
	 *  
	 *  如果相反：
	 *  直接结束
	 *  
	 *  
	 */  
	public boolean  stepForEat(int dir ,Food food){
		if(this.dir +dir ==0){
			return false ;
		}
		//记录当前的方向
		this.dir = dir ;
		Cell head = cells[0] ;
		int i = head.getI() + dir /10 ;
		int j = head.getJ() + dir %10 ;
		boolean eat = false ;
		if(i == food.getI() && j == food.getJ()){
			cells=Arrays.copyOf(cells, cells.length+1);
			eat = true ;
		}
		for(int index =cells.length-1 ;index >=1; index --){
			cells[index] = cells[index-1] ;
		}
		cells[0] = new Cell(i,j) ;
		return eat ;
	}
	
	//追加定义重载的走一步方法
	public boolean stepForEat(Food food){
		return this.stepForEat(dir, food) ;
	}
	
	/**
	 * 判断是否撞到墙 或者吃掉自己
	 * @return
	 * 1.判断给定的方向是否根当前方向相反
	 * 2.取出头节点
	 * 3.根据给定的方向，计算新节点
	 * 4.判断新节点的i,j是否越界，或者是否在蛇的身上
	 */
	public boolean headBreak(int dir ,int rows, int cols){
		if(this.dir + dir == 0){
			return false ;
		}
		
		Cell head = cells[0] ;
		int i = head.getI() + dir /10 ;
		int j = head.getJ() + dir %10 ;
		
//		boolean isEat = false ;
//		//判断是否在蛇的身上
//		for(int index = 0 ; index <cells.length ; index ++){
//			Cell currentCell = cells[index] ;
//			if(i == currentCell.getI() && j == currentCell.getJ()){
//				isEat = true ;
//				break ;
//			}
//		}
		
		return 	i<0 || i>=rows || j <0 || j>=cols || this.contains(i, j) ;
	
	}
	
	//追加定义重载的判断是否撞墙的方法
	public boolean headBreak(int rows,int cols){
		return this.headBreak(dir, rows, cols) ;
	}
	
	//追加判断一个节点是否在蛇的身上的方法
	public boolean contains(int i, int j){
		for(int index = 0 ; index <cells.length ;index++){
			Cell cell = cells[index] ;
			if(i == cell.getI() && j == cell.getJ()){
				return true ;
			}
		}
		return false ;
	}
	

	public Cell[] getCells() {
		return cells;
	}
	public void setCells(Cell[] cells) {
		this.cells = cells;
	}
}
