package worm;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main {
	public static void main(String [] args){
		//1.创建窗体对象
		JFrame frame = new JFrame("贪吃蛇") ;
		//frame.setTitle(title)
		//设置窗体的大小
		frame.setSize(430,460) ;
		//设置窗体居中显示
		frame.setLocationRelativeTo(null) ;
		//创建背景面板
		JPanel backPanel = new JPanel() ;
		//设置背景面板的颜色
		backPanel.setBackground(new Color(28,227,218)) ;
		//设置背景面板的布局
		backPanel.setLayout(null) ;
		//创建蛇的舞台
		//暂时没有
		WormStage stage = new WormStage() ;
		stage.setBackground(new Color(159,230,230)) ;
		stage.setSize(400,400) ;
		//设置舞台显示的位置
		stage.setLocation(10, 15) ;
		//组装
		backPanel.add(stage) ;
		frame.add(backPanel) ;
		//显示
		frame.setVisible(true) ;
		//设置默认的关闭操作
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		//启动游戏
		stage.go() ;
	}
}
