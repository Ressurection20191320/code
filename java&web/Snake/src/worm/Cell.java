package worm;

import java.awt.Color;
//蛇身上每个节点

public class Cell {
	private Color color ; //颜色
	private int width ; // 宽
	private int height ; // 高
	private int x ;  //x坐标
	private int y ;   // y坐标
	private int i ;   // 行号
	private int j ;   // 列号
	
	public Cell(int i ,int j){
		this.color = Util.CELL_COLOR ;
		this.width = Util.CELL_WIDTH ;
		this.height = Util.CELL_HEIGHT ;
		this.i = i ; 
		this.j = j ;
		
		this.x = j * width ;
		this.y = i * height; 
	}
	
	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}
	public int getJ() {
		return j;
	}
	public void setJ(int j) {
		this.j = j;
	}
	
	
}
