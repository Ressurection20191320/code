package Shiyan2;

class superClass1 {
	int y;

	superClass1() {
		y = 30;
		System.out.println("In super Class:y=" + y);
	}

	void doPrint() {
		System.out.println("In superClass1.doPrint()");
	}
}

class subClass1 extends superClass1 {
	int y;

	subClass1() {
		super();
		y = 50;
		System.out.println("in subClass1:y=" + y);
	}

	void doPrint() {
		super.doPrint();
		System.out.println("in subClass1.doPrint()");
		System.out.println("super.y=" + super.y + " sub.y=" + y);
	}
}

public class inviteSuper {
	public static void main(String args[]) {
		subClass1 subSC = new subClass1();
		subSC.doPrint();
	}
}