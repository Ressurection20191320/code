import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Xml {
	public static void main(String arge[]) {
		try {
			File f = new File("GroupMembers.xml");
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(f);
			NodeList nl = doc.getElementsByTagName("GroupMember");
			System.out.println("方法1：按标签输出");
			for (int i = 0; i < nl.getLength(); i++) {
				System.out.print("姓名:" + doc.getElementsByTagName("name").item(i).getFirstChild().getNodeValue() + "\t");
				System.out.print("学号:" + doc.getElementsByTagName("id").item(i).getFirstChild().getNodeValue() + "\t");
				System.out.print("性别:" + doc.getElementsByTagName("sex").item(i).getFirstChild().getNodeValue() + "\t");
				System.out.println("年龄:" + doc.getElementsByTagName("age").item(i).getFirstChild().getNodeValue());
			}
			System.out.println("\n方法2：遍历根节点");
			for (int i = 0; i < nl.getLength(); i++) {
				Node n = nl.item(i);
				NodeList pl = n.getChildNodes();
				for(int j=0;j<pl.getLength();j++) {
					if(pl.item(j).getNodeType() == Node.ELEMENT_NODE) {
						System.out.print(pl.item(j).getNodeName()+":"+pl.item(j).getFirstChild().getNodeValue()+"\t");
					}
				}
				System.out.println();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}