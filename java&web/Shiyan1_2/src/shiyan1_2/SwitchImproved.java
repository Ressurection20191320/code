package shiyan1_2;

import java.util.Scanner;

public class SwitchImproved {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int month = input.nextInt();
		while(month>12||month<1)
		{
			System.out.println("Wrong month! Please input a number between 1 and 12:");
			month = input.nextInt();
		}
		// int month =5;
		String season = null;
		String mon = null;
		switch (month) {
		case 12:
			mon = "December";
			season = "Winter";
			break;
		case 1:
			mon = "January";
			season = "Winter";
			break;
		case 2:
			mon = "Debruary";
			season = "Winter";
			break;
		case 3:
			mon = "March";
			season = "Spring";
			break;
		case 4:
			mon = "April";
			season = "Spring";
			break;
		case 5:
			mon = "May";
			season = "Spring";
			break;
		case 6:
			mon = "June";
			season = "Summer";
			break;
		case 7:
			mon = "July";
			season = "Summer";
			break;
		case 8:
			mon = "August";
			season = "Summer";
			break;
		case 9:
			mon = "September";
			season = "Autumn";
			break;
		case 10:
			mon = "October";
			season = "Autumn";
			break;
		case 11:
			mon = "November";
			season = "Autumn";
			break;
		default:
			season = "Wrong Month";
		}
		System.out.println(mon + "�� is the season of " + season + ".");
	}
}