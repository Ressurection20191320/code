import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.*;


public class Send extends JFrame {
    public Send() {
        setSize(400, 500);//设计窗体的大小
        setBounds(600,300,400,500);
        setTitle("发消息");
        JTextArea messageArea = new JTextArea(10, 20);
        JButton sendButton = new JButton("发送");
        setVisible(true);//使窗体可视化
        Container container = getContentPane();//获取一个容器
        container.add(messageArea);
        container.add(sendButton);
        container.setLayout(null);
        messageArea.setBounds(37, 50, 300, 300);
        sendButton.setBounds(280, 380, 60, 30);

        sendButton.addActionListener(new ActionListener() {//对快速注册按钮添加监听事件
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent arg0) {
                try {
                    SendMessage(messageArea.getText());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    void SendMessage(String message) throws Exception {
        DatagramSocket ds = new DatagramSocket();//通过DatagramSocket对象创建udp服务
            byte[] buf = message.getBytes();
            DatagramPacket dp =
                    new DatagramPacket(buf, buf.length, InetAddress.getByName("127.0.0.1"), 1320);//发送至指定IP，指定端口
            ds.send(dp);//通过send方法将数据包发送出去
        ds.close();//关闭资源
    }

    public static void main(String[] args) {
        new Send();
    }
}

