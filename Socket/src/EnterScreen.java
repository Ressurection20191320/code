import java.awt.*;//导入awt包
import javax.swing.*;//导入swing包
import java.awt.event.ActionListener;//导入awt包中的监听器事件包
import java.awt.event.ActionEvent;//导入awt包中的ActionEvent事件包


public class EnterScreen extends JFrame {
    static int s = 0;
    static String fileUrl = "Userlist.txt";

    public EnterScreen() {
        setBounds(800, 300, 300, 290);
        setTitle("登录");
        JLabel introLabel = new JLabel("Socket传输器");
        JLabel numLabel = new JLabel("20191320李泽昊");
        JLabel usernameLabel = new JLabel("用户名"); //实例化JLabel对象
        JLabel passwordLabel = new JLabel("口    令");
        JTextField usernameFiled = new JTextField(15);//实例化用户名文本框
        JPasswordField passwordField = new JPasswordField(15);//实例化密码框
        JButton loginButton = new JButton("登录");
        JButton registerButton = new JButton("注册");
        setVisible(true);//使窗体可视化
        Container container = getContentPane();//获取一个容器
        getContentPane().setBackground(Color.WHITE);//设置窗体填充色
//        将用户名、密码的Label和用户名JTextField文本框、密码JPasswordField密码框以及确定JButton、快速注册JButton添加到container容器里面                         //
        container.add(usernameLabel);
        container.add(passwordLabel);
        container.add(introLabel);
        container.add(numLabel);
        container.add(usernameFiled);
        container.add(passwordField);
        container.add(loginButton);
        container.add(registerButton);
        container.setLayout(null);
        introLabel.setBounds(50, 10, 250, 50);
        introLabel.setFont(new Font("黑体", Font.BOLD, 30));
        numLabel.setBounds(85, 40, 130, 50);
        usernameLabel.setBounds(30, 90, 50, 25);
        passwordLabel.setBounds(30, 130, 50, 25);
        usernameFiled.setBounds(75, 90, 130, 25);
        passwordField.setBounds(75, 130, 130, 25);
        loginButton.setBounds(30, 200, 100, 30);
        registerButton.setBounds(140, 200, 100, 30);

        loginButton.addActionListener(new ActionListener() {//对登录按钮添加监听事件

            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent arg0) {
                // TODO Auto-generated method stub
                if (usernameFiled.getText().equals("") || passwordField.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "请先输入用户名和口令！");
                } else {
                    boolean inBlack = MyFileHandler.StringSearchLine(usernameFiled.getText(), "BlackList.txt");
                    if (inBlack) {
                        JOptionPane.showMessageDialog(null, "账户" + usernameFiled.getText() + "在黑名单中，无法登陆！");
                    } else {
                        boolean status = MyFileHandler.UserValidate(usernameFiled.getText(), passwordField.getText(), fileUrl);
                        if (status) {
                            JOptionPane.showMessageDialog(null, "登录成功！");
                            setVisible(false);
                            new SendOrRecieve();
                        } else {
                            JOptionPane.showMessageDialog(null, "用户名或密码错误！");
                        }
                    }
                }
            }

        });
        registerButton.addActionListener(new ActionListener() {//对快速注册按钮添加监听事件
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent arg0) {
                // TODO Auto-generated method stub
                setVisible(false);
                new Register();
            }

        });
    }


    class SendOrRecieve extends JFrame {


        public SendOrRecieve() {
            setBounds(800, 550, 300, 90);
            setTitle("请选择");
            JButton SendButton = new JButton("发送消息");
            JButton ReceiveButton = new JButton("接受消息");
            Container container = getContentPane();//获取一个容器
            container.setLayout(new GridLayout(1, 2));

            container.add(SendButton);
            container.add(ReceiveButton);
            setVisible(true);
            SendButton.addActionListener(new ActionListener() {//对快速注册按钮添加监听事件
                @SuppressWarnings("deprecation")
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    // TODO Auto-generated method stub
                    new Send();
                }
            });
            ReceiveButton.addActionListener(new ActionListener() {//对快速注册按钮添加监听事件
                @SuppressWarnings("deprecation")
                @Override
                public void actionPerformed(ActionEvent arg0) {
                    // TODO Auto-generated method stub
                    JFrame receiveFrame = new Receive();
                    final Thread timer = new Thread(new Runnable() {
                        public void run() {
                            while (true) {
                                try {
                                    Thread.sleep(1000);
                                    Receive.time--;
                                    Receive.timeLabel.setText("超时机制：若"+Receive.time + "秒内无信息传入且无操作则自动退出！");
                                    if (Receive.time == 0) {
                                        receiveFrame.dispose();
                                        Receive.time=30;
                                        break;
                                    }
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                    timer.start();
                }
            });
        }

    }

    public static void main(String[] args) {
        new EnterScreen();
    }
}
