import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;


public class MyFileHandler {
    static boolean FileExistTest(String fileUrl) {
        boolean out = false;
        try {
            File f = new File(fileUrl);
            if(!f.exists()) {						//若文件不存在
                f.createNewFile();					//新建该文件

                String sets=" attrib +H  "+f.getAbsolutePath(); 	//设置文件属性为隐藏
                Runtime.getRuntime().exec(sets);			//执行文件设置
                out = true;
            }
            out = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out;
    }

    static boolean StringSearchLine(String strDst,String fileUrl) {
        boolean out = false;								//定义布尔类型的返回值
        try {
            FileInputStream fis = new FileInputStream(fileUrl);
            InputStreamReader isr = new InputStreamReader(fis,"UTF-8");
            BufferedReader br = new BufferedReader(isr);				//至此，变量br与文件中的字符流已建立连接

            String line = "";							//暂存br中取出的每一行字符串
            while((line = br.readLine()) != null) {				//文件未读取完毕
                if(line.equals(strDst)) {					//若找到匹配的字符串
                    out = true;						//则返回值赋值为true，终止循环
                    break;
                }
            }

            br.close();								//结束对文件的访问
            isr.close();
            fis.close();
        } catch (Exception e) {							//处理异常（输出异常）
            e.printStackTrace();
        }
        return out;									//返回查询结果
    }

    static boolean StringWriteLine(String strIn,String fileUrl) {
        boolean out = false;
        try {
            FileWriter fw = new FileWriter(fileUrl,true);				//建立文件写入访问连接

            fw.write("\n"+strIn);							//向文件中追加新的字符串StrIn
            fw.close();								//结束对文件的访问

            out = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }

    static boolean UserValidate(String userName,String password,String fileUrl) {
        boolean out = false;
        try {
            FileInputStream fis = new FileInputStream(fileUrl);
            InputStreamReader isr = new InputStreamReader(fis,"UTF-8");
            BufferedReader br = new BufferedReader(isr);				//至此，变量br与文件中的字符流建立连接

            String line = "";							//暂存br中取出的每一行字符串

            while((line=br.readLine()) != null) {					//若文件未读取完毕，则继续读取下一行
                if(line.equals("un" + userName)) {				//若当前行与用户名相匹配
                    if(br.readLine().equals(String.valueOf(("pw"+password).hashCode()))) {		//看密码是否匹配
                        out = true;									//若密码匹配，返回true
                    }
                    break;											//只要检索到用户名，不论密码是否匹配，都终止循环
                }
            }

            br.close();												//关闭对文件的访问
            isr.close();
            fis.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }
    static boolean UserRegister(String userName,String password,String fileUrl) {
        boolean out = false;
        if(StringSearchLine("un" + userName, fileUrl)) {				//检验用户名是否已存在
            return out;
        }else {										//若不存在，依次写入用户名和密码（分别加上前缀"un"和"pw"）
            StringWriteLine("un" + userName, fileUrl);
            StringWriteLine(String.valueOf(("pw" + password).hashCode()), fileUrl);
            out = true;
        }
        return out;
    }
}