import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.*;


class MessageThread extends Thread {
    public void run() {
        while (true) {
            try {
                Receive.messageArea.append(receiveMessage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    String receiveMessage() throws Exception {
        @SuppressWarnings("resource")
        DatagramSocket ds = new DatagramSocket(1320);//接收端监听指定端口
        while (true) {
            //定义数据包,用于存储数据
            byte[] buf = new byte[1024];
            DatagramPacket dp = new DatagramPacket(buf, buf.length);
            ds.receive(dp);//通过服务的receive方法将收到数据存入数据包中,receive()为阻塞式方法
            //通过数据包的方法获取其中的数据
            Receive.time=30;
            String ip = dp.getAddress().getHostAddress();
            String data = new String(dp.getData(), 0, dp.getLength());
            String info = "收到来自" + ip + "的数据:\n" + data + "\n\n";
            return info;
        }
    }
}




public  class Receive extends JFrame {
    static JTextArea messageArea = new JTextArea(10, 20);
    static int time = 30;
    static JLabel timeLabel = new JLabel();
    public Receive() {
        setSize(400, 500);//设计窗体的大小
        setBounds(1100, 300, 400, 500);
        setTitle("收消息");
        setVisible(true);//使窗体可视化
        JButton clearButton = new JButton("清空");
        Container container = getContentPane();//获取一个容器
        container.add(messageArea);
        container.add(timeLabel);
        container.add(clearButton);
        container.setLayout(null);
        clearButton.setBounds(280, 380, 60, 30);
        messageArea.setBounds(37, 50, 300, 300);
        messageArea.setEditable(false);
        messageArea.append("收到的消息在这里显示：\n\n\n");
        timeLabel.setBounds(37, 25, 350, 30);
        MessageThread messageThread = new MessageThread();
        messageThread.start();

        clearButton.addActionListener(new ActionListener() {//对快速注册按钮添加监听事件
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent arg0) {
                messageArea.setText("收到的消息在这里显示：\n\n\n");
                time=30;
            }
        });

    }

    public static void main(String args[]){
        new Receive();
    }
}
