import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Register extends JFrame {
    public Register() {
        setBounds(800,300,300,290);
        JLabel username = new JLabel("用户名"); //实例化JLabel对象
        JLabel password = new JLabel("密    码");
        JLabel passwordretyped = new JLabel("再次确认密码");
        JTextField Username = new JTextField(15);//实例化用户名文本框
        JPasswordField Password = new JPasswordField(15);//实例化密码文本框
        JPasswordField Password2 = new JPasswordField(15);//实例化密码文本框
        JButton submit = new JButton("确定");
        JButton back = new JButton("返回");
        setVisible(true);
        //获取一个容器
        Container container = getContentPane();
//        将用户名、密码的Jlabel和用户名JTextField文本框、密码JPasswordField密码框以及确定JButton、快速注册JButton添加到container容器里面
        container.add(username);
        container.add(password);
        container.add(passwordretyped);
        container.add(Password2);
        container.add(Username);
        container.add(Password);
        container.add(submit);
        container.add(back);
        //设置窗体的长宽各为300、250  让其显示在左上方的300、250处
        container.setLayout(null);
//        a、b、c、d、e、f显示在container容器中的位置坐标
        username.setBounds(10, 40, 50, 18);
        password.setBounds(10, 80, 50, 18);
        passwordretyped.setBounds(5, 120, 80, 18);
        Username.setBounds(60, 40, 200, 18);
        Password.setBounds(60, 80, 200, 18);
        Password2.setBounds(90, 120, 180, 18);
        submit.setBounds(110, 160, 60, 30);
        back.setBounds(190, 160, 60, 30);

        back.addActionListener(new ActionListener() {//对返回按钮添加监听事件
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent arg0) {
                // TODO Auto-generated method stub
                setVisible(false);
                new EnterScreen();

            }
        });
        submit.addActionListener(new ActionListener() {//对确认按钮添加监听事件
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent arg0) {
                // TODO Auto-generated method stub
                if (Username.getText().equals("") || Password.getText().equals("") || Password2.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "请完整输入后再注册！");
                }
                else if (Password.getText().equals(Password2.getText())) {
                    boolean status = MyFileHandler.UserRegister(Username.getText(), Password.getText(), EnterScreen.fileUrl);
                    if (status==true) {
                        JOptionPane.showMessageDialog(null, "成功注册，请您登录！");
                        setVisible(false);
                        new EnterScreen();
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "注册失败，用户已存在！");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "两次输入的密码不一致，请您检查后再试！");
                }
            }
        });

    }
}