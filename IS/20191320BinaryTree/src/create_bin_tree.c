#include"btree.h"
#include<stdio.h>
#include<stdlib.h>

BinTree CreateBinTree() //树的建立（依照前序遍历）
{
    char data;
    BinTree T;
    scanf("%c", &data); //输入二叉树数据
    if (data == '#')    //二叉树为空，即为'#'
        T = NULL;
    else
    {
        T = (BinTree)malloc(sizeof(struct BinTreeNode)); 
        T->data = data;
        T->Left = CreateBinTree();
        T->Right = CreateBinTree();
    }//继续递归创建子树
    return T;
}
