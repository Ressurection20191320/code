#include"btree.h"
#include<stdio.h>

void InOrder(BinTree t){
    if(t != NULL)
    {
        InOrder(t->Left);
        printf("%c",t->data);
        InOrder(t->Right);
    }
}
