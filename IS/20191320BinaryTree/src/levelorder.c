#include"btree.h"
#include"queue.h"
#include<stdio.h>

void LevelOrder(BinTree t)
{
    Queue q;
    BinTree bt;
    if (t == NULL)
        return;               // 若是空树直接返回
    q = CreateQueue(MAXSIZE); // 创建空队列
    AddQueue(q, t);//将根节点放入队列
    while (!IsEmptyQueue(q))
    {
        bt = DeleteQueue(q);
        printf("%c", bt->data); // 访问取出队列的结点
        if (bt->Left)
            AddQueue(q, bt->Left);
        if (bt->Right)
            AddQueue(q, bt->Right);//将左右子树放入队列
    }
}
