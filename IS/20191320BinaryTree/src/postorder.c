#include"btree.h"
#include<stdio.h>

void PostOrder(BinTree t){
    if(t != NULL)
    {
        PostOrder(t->Left);
        PostOrder(t->Right);
        printf("%c",t->data);
    }
}
