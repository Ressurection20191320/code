#ifndef __BTREE_H__
#define __BTREE_H__

typedef struct BinTreeNode
{
    char data;
    struct BinTreeNode *Left;
    struct BinTreeNode *Right;
} * BinTree;    //二叉树结构定义，数据类型是字符型

BinTree CreateBinTree();
void LevelOrder(BinTree t);//层序遍历
void PreOrder(BinTree t);  //先序遍历
void InOrder(BinTree t);   //中序遍历
void PostOrder(BinTree t); //后序遍历

#endif
