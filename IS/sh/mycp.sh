cpf2f()
{
	if [ ! -e $1 ]; then
		echo "没有找到文件："$1
		return 1
	fi
	if [ $1 -ef $2 ]; then
		echo "错误操作：复制给自己"
		return 1
	fi
	if [ -L $1 ]; then
		echo "复制"$1"..."
		link=$(readlink $1)
			ln -s $link $2
			return 0
	fi
	echo "复制$1给$2"
	cp $1 $2 2> /dev/null
}
cpf2d()
{
	newfile=$2/$(basename $1)
	echo "复制$1给路径$2"
	cpf2f $1 $newfile
}
cpd2d()
{
	echo "复制路径$1给路径$2"
	cp -r $1 $2
}

if  [ "$#" != "3" ];  then
  echo "参数数目错误"
elif  [ "$1" == "cpf2f" ];  then
  cpf2f $2 $3
elif  [ "$1" == "cpf2d" ];  then
  cpf2d $2 $3
elif  [ "$1" == "cpd2d" ];  then
  cpd2d $2 $3
else
  echo "错误使用"
fi
