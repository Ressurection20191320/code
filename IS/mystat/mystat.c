#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int rwxMode[] = {0,S_IRUSR, S_IWUSR, S_IXUSR, S_IRGRP, S_IWGRP, S_IXGRP, S_IROTH, S_IWOTH, S_IXOTH};

char getFileType(mode_t mode){
  if(S_ISDIR(mode)) return 'd';
  else if(S_ISCHR(mode))  return 'c';
  else if(S_ISBLK(mode)) return 'b';
  else if(S_ISFIFO(mode)) return 'f';
  else if(S_ISLNK(mode)) return 'l';
  else if(S_ISSOCK(mode)) return 's';
  else return '-';
}

int main(int argc, char *argv[])
{
    struct stat st;
    if (argc != 2)
    {
        fprintf(stderr, "使用“stat <filename or dirname>”。\n");
        exit(EXIT_FAILURE);
    }
    if (stat(argv[1], &st) == -1)
    {
        perror("stat");
        exit(EXIT_FAILURE);
    }
    printf("  文件: %s\n", argv[1]);
    printf("  大小：%ld\t\t",(long)st.st_size);
    printf("  块: %lld\t\t", (long long)st.st_blocks);
    printf("IO 块: %ld  ", (long)st.st_blksize);
    switch (st.st_mode & S_IFMT)
    {
    case S_IFBLK:
        printf("块设备\n");
        break;
    case S_IFCHR:
        printf("character device\n");
        break;
    case S_IFDIR:
        printf("目录\n");
        break;
    case S_IFIFO:
        printf("FIFO/管道\n");
        break;
    case S_IFLNK:
        printf("符号链接\n");
        break;
    case S_IFREG:
        printf("普通文件\n");
        break;
    case S_IFSOCK:
        printf("socket\n");
        break;
    default:
        printf("未知?\n");
        break;
    }
    printf("设备: %lxh/%lud\t",st.st_dev,st.st_dev);
    printf("Inode: %ld\t\t", (long)st.st_ino);
    printf("硬连接: %ld\n", (long)st.st_nlink);
    char fileMode[11];
    strcpy(fileMode, "-rwxrwxrwx");
    fileMode[0] = getFileType(st.st_mode);
    for(int j = 1; j < 10; ++j)
      if(!(st.st_mode & rwxMode[j])) fileMode[j] = '-';
    printf("权限: (%s)",fileMode);
    printf("\tidD=( %ld/   lzh)   Gid=( %ld/   lzh)\n", (long)st.st_uid, (long)st.st_gid);
    printf("最近访问: %s", ctime(&st.st_atime));
    printf("最近更改: %s", ctime(&st.st_mtime));
    printf("最近改动: %s", ctime(&st.st_ctime));
    printf("创建时间: -\n");
    exit(EXIT_SUCCESS);
}

