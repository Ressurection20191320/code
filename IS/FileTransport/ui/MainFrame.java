package ui;

import base.FileInfo;
import client.ClientMainWindow;
import client.ProtocolClientSSL;
import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import javax.swing.plaf.FontUIResource;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.net.ssl.KeyManagerFactory;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import ssl.ClientConnector;
import ssl.Config;
import ssl.FileReceiver;
import ssl.FileSender;
import ssl.KeyManage;
import ssl.ServerConnector;

public class MainFrame {

	static ProtocolClientSSL client;
	//public static final String IP_ADDR = "81.70.18.119";
	public static final String IP_ADDR = "127.0.0.1";
	public static final int PORT = 4321;

	public MainFrame() throws Exception {
		client = new ProtocolClientSSL(IP_ADDR, PORT);
		BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.translucencySmallShadow;//translucencyAppleLike;
		org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.launchBeautyEyeLNF();
		//InitGlobalFont(new Font("微软雅黑", Font.PLAIN, 12));
		UIManager.put("RootPane.setupButtonVisible", false);

		initFromCookie();
	}

	/*private static void InitGlobalFont(Font font) {
		FontUIResource fontRes = new FontUIResource(font);
		for (Enumeration<Object> keys = UIManager.getDefaults().keys(); keys.hasMoreElements();) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value instanceof FontUIResource) {
				UIManager.put(key, fontRes);
			}
		}
	}*/

	private void initFromCookie() throws FileNotFoundException {
		String status = client.loadCookie();
		//frameFileInitialize();
		frameLoginInitialize();
		frameRegisterInitialize();
		frameLogin.setVisible(true);
		jFrame.setVisible(false);
		/*if(status != null) {
			jFrame.setVisible(true);
		}else {
			frameLogin.setVisible(true);
		}*/
	}

	/**
	 * 主窗体
	 */
	private static JFrame jFrame = new JFrame("安全公文传输系统");
	private JTable table;
	static List<FileInfo> fileList;
	JPanel panelFile, panelLogin,panelRegister;

	static JFrame frameLogin;
	JFrame frameRegister;
	JButton buttonRegister,buttonRegister2,buttonLogin,buttonUp,buttonCancel;
	String username;
	String password;
	JTextField textFieldUsernameLogin,textFieldUsernameRegister, textFieldServer;
	JPasswordField passwordFieldLogin,passwordFieldRegister;
	JLabel labelUsernameLogin,labelPasswordLogin,labelUsernameRegister,labelPasswordRegister,labelServer;

	JScrollPane scrollPane;
	/**
	 * 顶部panel
	 */
	private JPanel topPanel = new JPanel();
	private JLabel chooseKeyStoreLabel = new JLabel("选择证书库:");
	private JTextField chooseKeyStoreTextField  = new JTextField(20);
	private JButton chooseKeyStoreButton = new JButton("选择文件");
	private JFileChooser chooseKeyStoreFileChooser = new JFileChooser(Config.getWorkPath());
	private JLabel chooseTrustKeyStoreLabel = new JLabel("选择受信任证书库:");
	private JTextField chooseTrustKeyStoreTextField  = new JTextField(20);
	private JButton chooseTrustKeyStoreButton = new JButton("选择文件");
	private JFileChooser chooseTrustKeyStoreFileChooser = new JFileChooser(Config.getWorkPath());
	/**
	 * 中部panel
	 */
	private JPanel middlePanel1 = new JPanel();
	private JPanel middlePanel2 = new JPanel();
	private JPanel middlePanel3 = new JPanel();

	//发送方
	private JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	private JLabel chooseFileLabel = new JLabel("选择要发送的文件");
	private JTextField chooseFileTextField = new JTextField(18);
	private JButton chooseFileButton = new JButton("选择文件");
	private JFileChooser chooseFileFileChooser = new JFileChooser();
	private JLabel ipAddressLabel = new JLabel("接收方IP地址：");
	private JTextField ipAddressTextField = new JTextField(10);
	private JLabel portLabel1 = new JLabel("接收方端口号：");
	private JTextField portTextField1 = new JTextField(5);
	private JButton sendButton = new JButton("点击发送");
	private JButton sendButton1 = new JButton("点击发送2");
	//接收方
	private JLabel chooseFilePathLabel = new JLabel("选择文件存放路径");
	private JTextField chooseFilePathTextField = new JTextField(48);
	private JButton chooseFilePathButton = new JButton("选择路径");
	private JFileChooser chooseFilePathFileChooser = new JFileChooser();
	private JLabel portLabel2 = new JLabel("接收文件端口号：");
	private JTextField portTextField2 = new JTextField(5);
	private JButton receiveButton = new JButton("点击接收");
	private JButton flushButton = new JButton("点击查看公文库");

	//上传
	//private JTabbedPane tabbedPane1 = new JTabbedPane(JTabbedPane.TOP);
	private JLabel chooseFileLabel1 = new JLabel("选择要上传的文件");
	private JTextField chooseFileTextField1 = new JTextField(18);
	private JButton chooseFileButton1 = new JButton("选择文件");
	private JFileChooser chooseFileFileChooser1 = new JFileChooser();
	private JLabel ipAddressLabel1 = new JLabel("服务器IP地址：");
	private JTextField ipAddressTextField1 = new JTextField(10);
	private JLabel portLabel3 = new JLabel("服务器端口号：");
	private JTextField portTextField3 = new JTextField(5);
	private JButton uploadButton = new JButton("点击上传");


	/**
	 * 底部panel
	 */
	private JPanel boottomPanel = new JPanel();
	private JTextArea textArea = new JTextArea(20,80);

	/**
	 * 全局变量
	 */
	private ServerConnector serverConnector = new ServerConnector();
	private ClientConnector clientConnector = new ClientConnector();
	private FileSender fileSender  = new FileSender();
	private FileReceiver fileReceiver = new FileReceiver();
	private KeyManage keyManage = new KeyManage();
	private String fileName;
	private String filePath;
	private String address = ipAddressTextField.getText().trim();
	private String portStr = portTextField1.getText().trim();
	private int port;
	private String keyStoreFile = "";
	private String keyTrustStoreFile = "";

	/**
	 * 窗口初始化函数
	 */
	public void init(){
		initTop();
		initMiddle();
		initBottom();
		//重定位输出流
		GUIPrintStream guiPrintStream = new GUIPrintStream(System.out, textArea);
		System.setOut(guiPrintStream);
	}
	/**
	 * 初始化Top Panel
	 */
	private void initTop(){
		//************************Top Panel配置***************************

		topPanel.setLayout(new GridLayout(2,1,5,0));//两行一列的布局
		//第一行
		Box topBox1 = Box.createHorizontalBox();
		chooseKeyStoreTextField.setMaximumSize(new Dimension(90, 25));
		topBox1.add(chooseKeyStoreLabel);
		topBox1.add(Box.createHorizontalStrut(40));
		topBox1.add(chooseKeyStoreTextField);
		topBox1.add(Box.createHorizontalStrut(40));
		topBox1.add(chooseKeyStoreButton);
		chooseKeyStoreTextField.setEditable(false);
		chooseKeyStoreFileChooser.addChoosableFileFilter(new JksFileFilter());
		chooseKeyStoreFileChooser.setAcceptAllFileFilterUsed(false);
		//点击“选择证书库”事件
		chooseKeyStoreButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				chooseKeyStoreFileChooser.setCurrentDirectory(new File("."+File.separator+"certs"));
				int result = chooseKeyStoreFileChooser.showDialog(jFrame, "打开证书库");
				if(result == JFileChooser.APPROVE_OPTION){
					String name = chooseKeyStoreFileChooser.getSelectedFile().getPath();
					if(showKeyStorePassDialog(chooseKeyStoreFileChooser, name)){
						chooseKeyStoreTextField.setText(name);
					}
					//System.out.println(name);
				}
			}
		});
		topPanel.add(topBox1);
		//第二行
		Box topBox2 = Box.createHorizontalBox();
		topBox2.add(chooseTrustKeyStoreLabel);
		topBox2.add(Box.createHorizontalStrut(20));
		chooseTrustKeyStoreTextField.setMaximumSize(new Dimension(80, 25));
		topBox2.add(chooseTrustKeyStoreTextField);
		topBox2.add(Box.createHorizontalStrut(20));
		topBox2.add(chooseTrustKeyStoreButton);
		chooseTrustKeyStoreTextField.setEditable(false);
		chooseTrustKeyStoreFileChooser.addChoosableFileFilter(new JksFileFilter());
		chooseTrustKeyStoreFileChooser.setAcceptAllFileFilterUsed(false);
		//点击“选择证书库”事件
		chooseTrustKeyStoreButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				chooseTrustKeyStoreFileChooser.setCurrentDirectory(new File("."+File.separator+"certs"));
				int result = chooseTrustKeyStoreFileChooser.showDialog(jFrame, "打开证书库");
				if(result == JFileChooser.APPROVE_OPTION){
					String name = chooseTrustKeyStoreFileChooser.getSelectedFile().getPath();
					if(showTrustKeyStorePassDialog(chooseTrustKeyStoreFileChooser, name)){
						chooseTrustKeyStoreTextField.setText(name);
					}
					//System.out.println(name);
				}
			}
		});
		topPanel.add(topBox2);
	}
	/**
	 * 初始化Middle Panel
	 */
	private void initMiddle(){
		//*********************发送文件Tab***********************
		middlePanel1.setLayout(new GridLayout(4, 1,20,15));
		//第一行，选择文件
		Box topBox1 = Box.createHorizontalBox();
		topBox1.add(Box.createHorizontalStrut(20));
		topBox1.add(chooseFileLabel);
		topBox1.add(Box.createHorizontalStrut(20));
		topBox1.add(chooseFileTextField);
		topBox1.add(Box.createHorizontalStrut(20));
		topBox1.add(chooseFileButton);
		topBox1.add(Box.createHorizontalStrut(20));
		chooseFileTextField.setEditable(false);
		//点击“选择文件”事件
		chooseFileButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//chooseFileFileChooser.setCurrentDirectory();
				int result = chooseFileFileChooser.showDialog(jFrame, "选择文件");
				if(result == JFileChooser.APPROVE_OPTION){
					String name = chooseFileFileChooser.getSelectedFile().getPath();
					chooseFileTextField.setText(name);
					//To do 这里应将文件名传出！！
					fileName = name;
					//System.out.println(name);
				}
			}
		});
		middlePanel1.add(topBox1);
		//第二行，设置接收方IP
		Box topBox2 = Box.createHorizontalBox();
		topBox2.add(Box.createHorizontalStrut(80));
		topBox2.add(ipAddressLabel);
		topBox2.add(Box.createHorizontalStrut(20));
		topBox2.add(ipAddressTextField);
		topBox2.add(Box.createHorizontalStrut(80));

		middlePanel1.add(topBox2);
		//第三行，设置接收方端口
		Box topBox3 = Box.createHorizontalBox();
		topBox3.add(Box.createHorizontalStrut(80));
		topBox3.add(portLabel1);
		topBox3.add(Box.createHorizontalStrut(20));
		topBox3.add(portTextField1);
		topBox3.add(Box.createHorizontalStrut(80));
		middlePanel1.add(topBox3);
		portTextField1.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				int keyChar = e.getKeyChar();
                if(keyChar >= KeyEvent.VK_0 && keyChar <= KeyEvent.VK_9){

                }else{
                    e.consume(); //关键，屏蔽掉非法输入
                }
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});
		middlePanel1.add(topBox3);
		//第四行，添加发送按钮
		Box topBoxsend = Box.createHorizontalBox();
		topBoxsend.add(Box.createHorizontalStrut(140));
		topBoxsend.add(sendButton);
		//topBoxsend.add(sendButton1);
		topBoxsend.add(Box.createHorizontalStrut(100));
		middlePanel1.add(topBoxsend);

		sendButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				sendButtonAction();
			}
		});
		sendButton1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				sendButtonAction();
			}
		});
		//向Tab添加标签
		tabbedPane.addTab("发送文件",middlePanel1);
		//*************************接收文件件Tab*******************************

		middlePanel2.setLayout(new GridLayout(3, 2,20,30));
		//第一行，选择文件存放路径
		Box topBox4 = Box.createHorizontalBox();
		topBox4.add(Box.createHorizontalStrut(20));
		topBox4.add(chooseFilePathLabel);
		topBox4.add(Box.createHorizontalStrut(80));
		topBox4.add(chooseFilePathTextField);
		topBox4.add(Box.createHorizontalStrut(20));
		topBox4.add(chooseFilePathButton);
		topBox4.add(Box.createHorizontalStrut(20));

		chooseFilePathTextField.setEditable(false);
		chooseFilePathFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		//点击“选择路径”事件
		chooseFilePathButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//chooseFileFileChooser.setCurrentDirectory();
				int result = chooseFilePathFileChooser.showDialog(jFrame, "选择文件存放目录");
				if(result == JFileChooser.APPROVE_OPTION){
					String name = chooseFilePathFileChooser.getSelectedFile().getPath();
					chooseFilePathTextField.setText(name);
					//To do 这里应将目录传出！！
					filePath = name;
					//System.out.println(name);
				}
			}
		});
		middlePanel2.add(topBox4);
		//第二行，设置接收方端口
		Box topBox5 = Box.createHorizontalBox();
		topBox5.add(Box.createHorizontalStrut(20));
		topBox5.add(portLabel2);
		topBox5.add(Box.createHorizontalStrut(20));
		topBox5.add(portTextField2);
		topBox5.add(Box.createHorizontalStrut(70));
		middlePanel2.add(topBox5);
		tabbedPane.addTab("接收文件",middlePanel2);
		portTextField2.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				int keyChar = e.getKeyChar();
                if(keyChar >= KeyEvent.VK_0 && keyChar <= KeyEvent.VK_9){

                }else{
                    e.consume(); //关键，屏蔽掉非法输入
                }
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});
		middlePanel2.add(topBox5);

		//第三行，添加接收按钮
		Box topBoxreceive = Box.createHorizontalBox();
		topBoxreceive.add(Box.createHorizontalStrut(140));
		topBoxreceive.add(receiveButton);
		topBoxreceive.add(Box.createHorizontalStrut(100));
		middlePanel2.add(topBoxreceive);
		receiveButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				receiveButtonAction();
			}
		});

		//第四行，添加查看公文库按钮
		Box watchku = Box.createHorizontalBox();
		watchku.add(Box.createHorizontalStrut(20));
		watchku.add(flushButton);
		watchku.add(Box.createHorizontalStrut(20));
		middlePanel2.add(watchku);
		fileList = new ArrayList<FileInfo>();
		flushButton.addActionListener(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent e) {
				//System.out.println("path: " + "afaf");
				Map<String, Object> res = ProtocolClientSSL.getDirectoryList();
				if(res.get("status").equals("invalid")) {
					jFrame.setVisible(false);
					frameLogin.setVisible(true);
				}else {
					if(res.get("fileList") != null) {
						fileList = (List<FileInfo>) res.get("fileList");
						String[][] fileTable=new String[fileList.size()][3];
						for(int row=0;row<fileList.size();row++) {
							fileTable[row][0]=fileList.get(row).getFilename();
							fileTable[row][1]=fileList.get(row).getFilesize()+"";
							fileTable[row][2]="下载";
						}

						String[] columnNames = { "文件", "文件大小(字节)", "" };
						DefaultTableModel model = new DefaultTableModel();
						model.setDataVector(fileTable, columnNames);

						table = new JTable(model);
						scrollPane.setViewportView(table);
						table.setColumnSelectionAllowed(true);
						table.setCellSelectionEnabled(true);
						ui.MainFrame.ButtonColumn buttonsColumn = new ButtonColumn(table, 2);
					}
				}
			}
		});

		panelFile = new JPanel();
		/* File list */
		scrollPane = new JScrollPane();
		scrollPane.setBounds(46, 80, 700, 250);
		panelFile.add(scrollPane);
		tabbedPane.addTab("接收文件",middlePanel2);
		flushButton.setBounds(312, 30, 150, 23);
		jFrame.getContentPane().add(panelFile);
		panelFile.setLayout(null);
		panelFile.add(uploadButton);
		panelFile.add(flushButton);


		middlePanel3.setLayout(new GridLayout(4, 1,20,15));
		//第一行，选择文件
		Box topBox10 = Box.createHorizontalBox();
		topBox10.add(Box.createHorizontalStrut(20));
		topBox10.add(chooseFileLabel1);
		topBox10.add(Box.createHorizontalStrut(20));
		topBox10.add(chooseFileTextField1);
		topBox10.add(Box.createHorizontalStrut(20));
		topBox10.add(chooseFileButton1);
		topBox10.add(Box.createHorizontalStrut(20));
		chooseFileTextField1.setEditable(false);
		//点击“选择文件”事件
		chooseFileButton1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//chooseFileFileChooser.setCurrentDirectory();
				int result = chooseFileFileChooser1.showDialog(jFrame, "选择文件");
				if(result == JFileChooser.APPROVE_OPTION){
					String name = chooseFileFileChooser1.getSelectedFile().getPath();
					chooseFileTextField1.setText(name);
					filePath = chooseFileFileChooser1.getSelectedFile().getPath();
					//To do 这里应将文件名传出！！
					fileName = name;
					//System.out.println(name);
				}
			}
		});
		middlePanel3.add(topBox10);
		//第二行，设置接收方IP
		Box topBox20 = Box.createHorizontalBox();
		topBox20.add(Box.createHorizontalStrut(80));
		topBox20.add(ipAddressLabel1);
		topBox20.add(Box.createHorizontalStrut(20));
		topBox20.add(ipAddressTextField1);
		topBox20.add(Box.createHorizontalStrut(80));

		middlePanel3.add(topBox20);
		//第三行，设置服务器端口
		Box topBox30 = Box.createHorizontalBox();
		topBox30.add(Box.createHorizontalStrut(80));
		topBox30.add(portLabel3);
		topBox30.add(Box.createHorizontalStrut(20));
		topBox30.add(portTextField3);
		topBox30.add(Box.createHorizontalStrut(80));
		middlePanel3.add(topBox30);
		portTextField3.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				int keyChar = e.getKeyChar();
				if(keyChar >= KeyEvent.VK_0 && keyChar <= KeyEvent.VK_9){

				}else{
					e.consume(); //关键，屏蔽掉非法输入
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});
		middlePanel3.add(topBox30);
		//第四行，添加上传按钮
		Box topBoxsend40 = Box.createHorizontalBox();
		topBoxsend40.add(Box.createHorizontalStrut(140));
		topBoxsend40.add(uploadButton);
		topBoxsend40.add(Box.createHorizontalStrut(100));
		middlePanel3.add(topBoxsend40);

		uploadButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				uploadButtonAction();
			}
		});

		//向Tab添加标签
		tabbedPane.addTab("上传文件",middlePanel3);



	}
	/**
	 * 初始化底部Panel
	 */
	private void initBottom(){

		boottomPanel.add(new JScrollPane(textArea));
		textArea.setEditable(false);
	}
	public void show(){

		//添加Top Panel
		((JPanel)jFrame.getContentPane()).setBorder(BorderFactory.createEmptyBorder(15,60,20,60));
		  //水平间隔120，垂直间隔80
		jFrame.getContentPane().setLayout(new GridLayout(3,1,50,30));
		jFrame.add(topPanel);
		//添加Middle Panel
		jFrame.add(tabbedPane);
		//添加Bottom Panel
		jFrame.add(boottomPanel);
		//设置Jframe属性
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.pack();//自适应大小
		jFrame.setVisible(true);
		jFrame.setLocationRelativeTo(null);
		jFrame.setSize(1820,980);
	}
	/**
	 * 弹出填写证书库密码的框
	 * @param parent
	 * @param filePath
	 * @return
	 */
	private boolean showKeyStorePassDialog(Component parent,String filePath){
		JPasswordField pf=new JPasswordField(15);
		int result;
		result = JOptionPane.showOptionDialog(chooseKeyStoreFileChooser, pf, "请输入密码：", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
		if(result == JOptionPane.CANCEL_OPTION){
			return false;
		}
		while(result == JOptionPane.OK_OPTION){
			if(result == JOptionPane.CANCEL_OPTION){
				return false;
			}
			if(pf.getText().trim().isEmpty()){
				JOptionPane.showConfirmDialog(null, "密码不能为空！", "提示", JOptionPane.DEFAULT_OPTION);
				result = JOptionPane.showOptionDialog(chooseKeyStoreFileChooser, pf, "请输入密码：", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
				continue;
			}
			//判断密码是否正确
			if(keyManage.setKeyStoreFile(filePath, pf.getText().trim())){
				keyStoreFile = filePath;
				return true;
			}else{
				keyStoreFile = "";
				JOptionPane.showConfirmDialog(null, "密码不正确！", "提示", JOptionPane.DEFAULT_OPTION);
				result = JOptionPane.showOptionDialog(chooseTrustKeyStoreFileChooser, pf, "请输入密码：", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
				continue;
			}
		}
		if(result == JOptionPane.CANCEL_OPTION || result == JOptionPane.CLOSED_OPTION){
			return false;
		}
		System.out.println(pf.getPassword());
		return true;
	}
	/**
	 * 弹出填写证书库密码的框
	 * @param parent
	 * @param filePath
	 * @return
	 */
	private boolean showTrustKeyStorePassDialog(Component parent,String filePath){
		JPasswordField pf=new JPasswordField(15);
		int result;
		result = JOptionPane.showOptionDialog(chooseTrustKeyStoreFileChooser, pf, "请输入密码：", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
		if(result == JOptionPane.CANCEL_OPTION){
			return false;
		}
		while(result == JOptionPane.OK_OPTION){
			if(result == JOptionPane.CANCEL_OPTION){
				return false;
			}
			if(pf.getText().trim().isEmpty()){
				JOptionPane.showConfirmDialog(null, "密码不能为空！", "提示", JOptionPane.DEFAULT_OPTION);
				result = JOptionPane.showOptionDialog(chooseTrustKeyStoreFileChooser, pf, "请输入密码：", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
				continue;
			}
			//判断密码是否正确
			if(keyManage.setTrustKeyStoreFile(filePath, pf.getText().trim())){
				keyTrustStoreFile = filePath;
				return true;
			}else{
				keyTrustStoreFile = "";
				JOptionPane.showConfirmDialog(null, "密码不正确！", "提示", JOptionPane.DEFAULT_OPTION);
				result = JOptionPane.showOptionDialog(chooseTrustKeyStoreFileChooser, pf, "请输入密码：", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, null, null);
				continue;
			}
		}
		if(result == JOptionPane.CANCEL_OPTION || result == JOptionPane.CLOSED_OPTION){
			return false;
		}
		System.out.println(pf.getPassword());
		return true;
	}
	/**
	 * 发送按钮事件
	 */
	private void sendButtonAction(){
		address = ipAddressTextField.getText().trim();
		portStr = portTextField1.getText().trim();
		//检查数据合法性
		if(keyStoreFile.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "请先选择要使用的证书库！", "提示", JOptionPane.DEFAULT_OPTION);
			return;
		}
		if(keyTrustStoreFile.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "请先选择要使用的证书信任库！", "提示", JOptionPane.DEFAULT_OPTION);
		}
		if(fileName.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "请先选择要发送的文件！", "提示", JOptionPane.DEFAULT_OPTION);
		}
		if(address.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "IP地址不能为空！", "提示", JOptionPane.DEFAULT_OPTION);
			return;
		}
		if(portStr.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "port不能为空！", "提示", JOptionPane.DEFAULT_OPTION);
			return;
		}
		port = Integer.valueOf(portStr);
		if(port > 65535 || port <0){
			JOptionPane.showConfirmDialog(jFrame, "端口号不在合法范围！", "提示", JOptionPane.DEFAULT_OPTION);
			return;
		}
		//先将按钮置为不可用
		sendButton.setEnabled(false);
		//以下为新线程内进行，
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				//1、重新初始化KeyManage，检查是否初始化完成
				if(!keyManage.isReady()){
					JOptionPane.showConfirmDialog(jFrame, keyManage.getLastError(), "提示", JOptionPane.DEFAULT_OPTION);
					sendButton.setEnabled(true);
					sendButton1.setEnabled(true);
					return;
				}
				//2、重置ClientConnector，传入KeyManage参数
				clientConnector.reset(keyManage);
				//3、连接接收端
				if(!clientConnector.connect(address,port)){
					JOptionPane.showConfirmDialog(jFrame, "连接服务端失败！", "提示", JOptionPane.DEFAULT_OPTION);
					sendButton.setEnabled(true);
					return;
				}
				//4、发送文件
				fileSender.setSocket(clientConnector.getSSLSocket());
				if(!fileSender.sendFile(fileName)){
					JOptionPane.showConfirmDialog(jFrame, "发送文件失败，原因：\n"+fileSender.getLastError(), "提示", JOptionPane.DEFAULT_OPTION);
					sendButton.setEnabled(true);
					return;
				}
				//发送成功
				clientConnector.closeConnection();
				JOptionPane.showConfirmDialog(jFrame, "文件发送成功！", "提示", JOptionPane.DEFAULT_OPTION);
				sendButton.setEnabled(true);
			}
		});
		thread.start();
	}
	/**
	 * 接收按钮事件
	 */
	private void receiveButtonAction(){
		portStr = portTextField2.getText().trim();
		//检查数据合法性
		if(keyStoreFile.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "请先选择要使用的证书库！", "提示", JOptionPane.DEFAULT_OPTION);
			return;
		}
		if(keyTrustStoreFile.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "请先选择要使用的证书信任库！", "提示", JOptionPane.DEFAULT_OPTION);
		}
		if(filePath.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "请先选择要发送的文件！", "提示", JOptionPane.DEFAULT_OPTION);
		}
		if(portStr.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "port不能为空！", "提示", JOptionPane.DEFAULT_OPTION);
			return;
		}
		port = Integer.valueOf(portStr);
		if(port > 65535 || port <0){
			JOptionPane.showConfirmDialog(jFrame, "端口号不在合法范围！", "提示", JOptionPane.DEFAULT_OPTION);
			return;
		}
		//先将按钮置为不可用
		receiveButton.setEnabled(false);
		//以下为新线程内进行，
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				//1、重新初始化KeyManage，检查是否初始化完成
				if(!keyManage.isReady()){
					JOptionPane.showConfirmDialog(jFrame, keyManage.getLastError(), "提示", JOptionPane.DEFAULT_OPTION);
					receiveButton.setEnabled(true);
					return;
				}
				//2、重置ClientConnector，传入KeyManage参数
				serverConnector.reset(keyManage);
				//3、开始监听
				if(!serverConnector.startListen(port)){
					JOptionPane.showConfirmDialog(jFrame, "等待发送端连接失败！", "提示", JOptionPane.DEFAULT_OPTION);
					receiveButton.setEnabled(true);
					return;
				}
				//4、接收
				fileReceiver.setSocket(serverConnector.getSSLSocket());
				if(!fileReceiver.receiveFile(filePath)){
					JOptionPane.showConfirmDialog(jFrame, "文件接收失败，原因：\n"+fileReceiver.getLastError(), "提示", JOptionPane.DEFAULT_OPTION);
					receiveButton.setEnabled(true);
					return;
				}
				//接收成功
				serverConnector.closeConnection();
				JOptionPane.showConfirmDialog(jFrame, "文件接收成功！", "提示", JOptionPane.DEFAULT_OPTION);
				receiveButton.setEnabled(true);;
			}
		});
		thread.start();
	}

	private void uploadButtonAction(){
		address = ipAddressTextField1.getText().trim();
		portStr = portTextField3.getText().trim();
		//检查数据合法性
		if(keyStoreFile.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "请先选择要使用的证书库！", "提示", JOptionPane.DEFAULT_OPTION);
			return;
		}
		if(keyTrustStoreFile.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "请先选择要使用的证书信任库！", "提示", JOptionPane.DEFAULT_OPTION);
		}
		if(fileName.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "请先选择要发送的文件！", "提示", JOptionPane.DEFAULT_OPTION);
		}
		if(address.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "IP地址不能为空！", "提示", JOptionPane.DEFAULT_OPTION);
			return;
		}
		if(portStr.isEmpty()){
			JOptionPane.showConfirmDialog(jFrame, "port不能为空！", "提示", JOptionPane.DEFAULT_OPTION);
			return;
		}
		port = Integer.valueOf(portStr);
		if(port > 65535 || port <0){
			JOptionPane.showConfirmDialog(jFrame, "端口号不在合法范围！", "提示", JOptionPane.DEFAULT_OPTION);
			return;
		}
		//先将按钮置为不可用
		uploadButton.setEnabled(false);
		//以下为新线程内进行，
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				//1、重新初始化KeyManage，检查是否初始化完成
				if(!keyManage.isReady()){
					JOptionPane.showConfirmDialog(jFrame, keyManage.getLastError(), "提示", JOptionPane.DEFAULT_OPTION);
					uploadButton.setEnabled(true);
					return;
				}
				//2、重置ClientConnector，传入KeyManage参数
				clientConnector.reset(keyManage);
				//3、连接接收端
				if(!clientConnector.connect(address,port)){
					JOptionPane.showConfirmDialog(jFrame, "连接服务器失败！", "提示", JOptionPane.DEFAULT_OPTION);
					uploadButton.setEnabled(true);
					return;
				}
				//4、发送文件
				//fileSender.setSocket(clientConnector.getSSLSocket());

				try {
					String status = ProtocolClientSSL.upload(filePath);
					if(status.equals("invalid")) {
						jFrame.setVisible(false);
						frameLogin.setVisible(true);
					}else if(status.equals("上传文件失败")) {
						JOptionPane.showMessageDialog(null, status);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}

				/*if(!fileSender.sendFile(fileName)){
					JOptionPane.showConfirmDialog(jFrame, "上传文件失败，原因：\n"+fileSender.getLastError(), "提示", JOptionPane.DEFAULT_OPTION);
					uploadButton.setEnabled(true);
					return;
				}*/
				//发送成功
				clientConnector.closeConnection();
				JOptionPane.showConfirmDialog(jFrame, "文件上传成功！", "提示", JOptionPane.DEFAULT_OPTION);
				uploadButton.setEnabled(true);
			}
		});
		thread.start();
	}

	/************注册页面初始化**************/
	public void frameRegisterInitialize() {
		frameRegister = new JFrame("Sign up");
		frameRegister = new JFrame("Sign up2");
		frameRegister.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panelRegister = new JPanel();
		buttonCancel = new JButton("Cancel");
		buttonCancel.setBounds(84, 150, 95, 29);
		buttonCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				frameRegister.setVisible(false);
				frameLogin.setVisible(true);
			}});
		buttonUp = new JButton("Register");
		buttonUp.setBounds(226, 150, 95, 29);
		buttonUp.addActionListener(new ActionListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					username = textFieldUsernameRegister.getText();
					password = passwordFieldRegister.getText();
					String status = client.register(username, password);
					JOptionPane.showMessageDialog(null, status);
					if(status.equals("success")) {
						frameRegister.setVisible(false);
						frameLogin.setVisible(true);
						//jFrame.setVisible(true);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}

		});

		labelUsernameRegister = new JLabel("Username");
		labelUsernameRegister.setBounds(59, 40, 90, 21);
		textFieldUsernameRegister = new JTextField(18);
		textFieldUsernameRegister.setBounds(174, 40, 168, 27);
		textFieldUsernameRegister.requestFocus();

		labelPasswordRegister = new JLabel("Password");
		labelPasswordRegister.setBounds(59, 90, 90, 21);
		passwordFieldRegister = new JPasswordField(18);
		passwordFieldRegister.setBounds(174, 90, 168, 27);

		frameRegister.getContentPane().add(panelRegister);
		panelRegister.setLayout(null);
		panelRegister.add(labelUsernameRegister);
		panelRegister.add(textFieldUsernameRegister);
		panelRegister.add(labelPasswordRegister);
		panelRegister.add(passwordFieldRegister);
		panelRegister.add(buttonUp);
		panelRegister.add(buttonCancel);

		frameRegister.setBounds(460, 180, 450, 270);

	}

	/************登录页面初始化**************/
	public void frameLoginInitialize() {
		frameLogin = new JFrame("Sign in");
		frameLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panelLogin = new JPanel();

		labelUsernameLogin = new JLabel("Username");
		labelUsernameLogin.setBounds(59, 40, 90, 21);
		textFieldUsernameLogin = new JTextField(18);
		textFieldUsernameLogin.setBounds(174, 40, 168, 27);

		labelPasswordLogin = new JLabel("Password");
		labelPasswordLogin.setBounds(59, 90, 90, 21);
		passwordFieldLogin = new JPasswordField(18);
		passwordFieldLogin.setBounds(174, 90, 168, 27);

		buttonRegister = new JButton("Sign up");
		buttonRegister2 = new JButton("Sign up2");

		buttonRegister.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				frameLogin.setVisible(false);
				frameRegister.setVisible(true);
			}
		});
		buttonRegister2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				frameLogin.setVisible(false);
				frameRegister.setVisible(true);
			}
		});
		buttonRegister.setBounds(84, 150, 95, 29);
		buttonRegister2.setBounds(170, 150, 95, 29);
		buttonLogin = new JButton("Sign in");
		buttonLogin.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					username = textFieldUsernameLogin.getText();
					password = passwordFieldLogin.getText();
					String status = client.login(username, password);
					JOptionPane.showMessageDialog(null, status);
					if(status.equals("success")) {
						show();
						frameLogin.setVisible(false);
						jFrame.setVisible(true);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}

		});
		buttonLogin.setBounds(226, 150, 95, 29);

		frameLogin.getContentPane().add(panelLogin);
		panelLogin.setLayout(null);
		panelLogin.add(labelUsernameLogin);
		panelLogin.add(textFieldUsernameLogin);
		panelLogin.add(labelPasswordLogin);
		panelLogin.add(passwordFieldLogin);
		panelLogin.add(buttonLogin);
		panelLogin.add(buttonRegister);
		panelLogin.add(buttonRegister2);

		frameLogin.setBounds(460, 180, 450, 270);
	}

	private static void InitGlobalFont(Font font) {
		FontUIResource fontRes = new FontUIResource(font);
		for (Enumeration<Object> keys = UIManager.getDefaults().keys(); keys.hasMoreElements();) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value instanceof FontUIResource) {
				UIManager.put(key, fontRes);
			}
		}
	}



	public static class ButtonColumn extends AbstractCellEditor implements
			TableCellRenderer, TableCellEditor, ActionListener {
		JTable table;
		JButton renderButton;
		JButton editButton;
		String text;

		public ButtonColumn(JTable table, int column) {
			super();
			this.table = table;
			renderButton = new JButton();
			editButton = new JButton();
			editButton.setFocusPainted(false);
			editButton.addActionListener(this);

			TableColumnModel columnModel = table.getColumnModel();
			columnModel.getColumn(column).setCellRenderer(this);
			columnModel.getColumn(column).setCellEditor(this);
		}

		public Component getTableCellRendererComponent(JTable table, Object value,
													   boolean isSelected, boolean hasFocus, int row, int column) {
			if (hasFocus) {
				renderButton.setForeground(table.getForeground());
				renderButton.setBackground(UIManager.getColor("Button.background"));
			} else if (isSelected) {
				renderButton.setForeground(table.getSelectionForeground());
				renderButton.setBackground(table.getSelectionBackground());
			} else {
				renderButton.setForeground(table.getForeground());
				renderButton.setBackground(UIManager.getColor("Button.background"));
			}

			renderButton.setText((value == null) ? " " : value.toString());
			return renderButton;
		}

		public Component getTableCellEditorComponent(JTable table, Object value,
													 boolean isSelected, int row, int column) {
			text = (value == null) ? " " : value.toString();
			editButton.setText(text);
			return editButton;
		}

		public Object getCellEditorValue() {
			return text;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			String file_name=fileList.get(table.getSelectedRow()).getFilename();
			Long length = fileList.get(table.getSelectedRow()).getFilesize();
			int result = 0;
			String path = null;
			JFileChooser fileChooser = new JFileChooser("ClientStorage/");
			FileSystemView fsv = FileSystemView.getFileSystemView();
			fsv.createFileObject(file_name);
			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			fileChooser.setDialogTitle("Select the folder:");
			result = fileChooser.showSaveDialog(null);
			if (JFileChooser.APPROVE_OPTION == result) {
				path=fileChooser.getSelectedFile().getPath()+"\\";
				System.out.println("path: "+path);
				System.out.println("file_name:"+file_name);
				String status = client.download(path, file_name, length);
				if(status.equals("invalid")) {
					jFrame.setVisible(false);
					frameLogin.setVisible(true);
				}else if(status.equals("error")) {
					JOptionPane.showMessageDialog(null, status);
				}
			}
		}
	}



	public static void main(String args[]){
		MainFrame mainFrame = null;
		try {
			mainFrame = new MainFrame();
		} catch (Exception e) {
			e.printStackTrace();
		}
		mainFrame.init();
		//mainFrame.show();
	}
}

