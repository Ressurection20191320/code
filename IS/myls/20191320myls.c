#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>

void myls(char []);

int main(int argc,char * argv[]){
    if (argc == 1) {
        myls(".");
    } else {
        while (--argc) {
            ++argv;
            myls(*argv);
        }
    }
    return 0;
}

void myls(char dirname[]){
    DIR *dir_ptr;
    struct dirent  *direntp;
    if ((dir_ptr = opendir(dirname)) == NULL) {
        printf("can't open directroy %s!\n",dirname);
    } else {
        while ((direntp = readdir(dir_ptr)) != NULL)
            printf("%s   ", direntp->d_name);
        printf("\n");
        closedir(dir_ptr);
    }
}
