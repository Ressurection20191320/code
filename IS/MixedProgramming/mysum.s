	.text
	.global mysum, printf	#mysum被调用，调用printf
	
mysum:
#	首先建立栈帧
	push	%rbp
	mov	%rsp, %rbp
#	mysum函数代码
	mov	24(%rbp), %rax	# AX = x
	add	28(%rbp), %rax	# AX += y
	
	mov	%rbp, %rsp
	pop	%rbp
	ret
