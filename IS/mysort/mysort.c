#include <stdio.h>
#include <stdlib.h>

int values[128];

int compare(const void * a, const void * b)
{
    return (*(int *)a - *(int *)b);
}

int main(int argc, char* argv[])
{
    int i,k;
    FILE* fp;
    if(argc!=2){
    	printf("参数错误，请传入一个文件名。\n");
    	return -1;
    }
    fp=fopen(argv[1],"r");
    if(fp==NULL){
    	printf("无法打开文件！\n");
    	return -1;
    }
    for(k=0;k<128;k++){
    	if(fscanf(fp,"%d",&values[k])==0)
    	    break;
    	if(values[k]==0)
    	    break;
    }
    qsort(values,k, sizeof(int), compare);
    for (i = 0; i < k; i++){
        printf("%d ", values[i]);
    }
    putchar('\n');
    return 0;
}
