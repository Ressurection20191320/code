#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
	int cnt=1;
	FILE *fp;
	fp=fopen("countline.txt","r");
	while(1){
		if(fgetc(fp)=='\n')
			cnt++;
		if(fgetc(fp)==EOF)
			break;
	}
	printf("Total lines:%d\n",cnt+1);
	return 0;
}
