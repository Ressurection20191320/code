#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int is_low_letter(char letter){
	return letter>='a'&&letter<='z';
}

int main(int argc, char* argv[]){
	FILE *fp,*gp;
	char ch;
	fp = fopen("text","r");
	if(fp == NULL){
		printf("Can't read file \"%s\"\n",argv[i]);
		return -1;
	}
	gp = fopen("upper","w");
	while(1){
		ch = fgetc(fp);
		if(is_low_letter(ch))ch+=('A'-'a');
		fputc(ch,gp);
		if(ch==EOF)break;
	}
	fclose(fp);
	return 0;
}

