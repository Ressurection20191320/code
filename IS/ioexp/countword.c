#include <stdio.h>
#include <stdlib.h>

int is_letter(char ch){
	if(ch>='a'&&ch<='z'||ch>='A'&&ch<='Z')
		return 1;
	return 0;
}

int main(){
	int cnt=0;
	FILE *fp;
	fp=fopen("countword.txt","r");
	while(1){
		if(fgetc(fp)==EOF)
			break;
		if(is_letter(fgetc(fp))){
			cnt++;
			while(is_letter(fgetc(fp)));
		}
	}
	printf("Number of words:%d\n",cnt);
	return 0;
}
