package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.InsertUpdateDelBean;

public class AdminAddServlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public AdminAddServlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html;charset=gb2312");
		request.setCharacterEncoding("gb2312");

		InsertUpdateDelBean ib = new InsertUpdateDelBean();
		String name = request.getParameter("name");
		String pwd = request.getParameter("pwd");
		String type = request.getParameter("type");
		String sql = "";
		if(type.equals("1")){
			sql = "insert into admin(name,pwd,type,status) values('"+name+"','"+pwd+"','"+type+"','管理员')";
		}else if(type.equals("2")){
			sql = "insert into admin(name,pwd,type,status) values('"+name+"','"+pwd+"','"+type+"','拟稿人')";
		}else if(type.equals("3")){
			String status = request.getParameter("status");
			sql = "insert into admin(name,pwd,type,status) values('"+name+"','"+pwd+"','"+type+"','"+status+"')";
		}
		int responseText = ib.insertANDupdateANDdel(sql);
		if(responseText == -1 ){
			request.setAttribute("message","'用户名重复！'");
		}else{
			request.setAttribute("message","'操作成功！'");
		}
		
		request.getRequestDispatcher("/admin/systemuser.jsp").forward(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occure
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
