package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

import bean.InsertUpdateDelBean;

public class DelServlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public DelServlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html;charset=gb2312");
		request.setCharacterEncoding("gb2312");
		
		String sql = "";
		String str = "";
		InsertUpdateDelBean ib = new InsertUpdateDelBean();
		
		String admin = request.getParameter("admin");
		String dep = request.getParameter("dep");
		String document = request.getParameter("document");
		
		if(admin != null && !admin.equals("")){
			sql = "delete from examine where document in (select id from document where admin="+admin+")";
			ib.insertANDupdateANDdel(sql);
			sql = "delete from document where admin="+admin;
			ib.insertANDupdateANDdel(sql);
			sql = "delete from admin where id="+admin;
			ib.insertANDupdateANDdel(sql);
			str = "/admin/systemuser.jsp";			
		}
		if(dep != null && !dep.equals("")){
			sql = "delete from examine where dep in (select id from document where dep="+dep+")";
			ib.insertANDupdateANDdel(sql);
			sql = "delete from examine where dep="+dep;
			ib.insertANDupdateANDdel(sql);
			sql = "delete from document where dep="+dep;
			ib.insertANDupdateANDdel(sql);
			sql = "delete from dep where id="+dep;
			ib.insertANDupdateANDdel(sql);
			str = "/admin/dep.jsp";			
		}
		if(document != null && !document.equals("")){
			sql = "delete from examine where document="+document;
			ib.insertANDupdateANDdel(sql);
			sql = "delete from document where id="+document;
			ib.insertANDupdateANDdel(sql);
			str = "/admin/document.jsp";			
		}
		
		request.getRequestDispatcher(str).forward(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occure
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
