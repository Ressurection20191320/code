package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.InsertUpdateDelBean;
import bean.SelectBean;

public class DocumentServlet extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public DocumentServlet() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html;charset=gb2312");
		request.setCharacterEncoding("gb2312");

		InsertUpdateDelBean ib = new InsertUpdateDelBean();
		HttpSession session = request.getSession();
		ArrayList adminlogin = (ArrayList)session.getAttribute("adminlogin");
		String title = request.getParameter("title");
		String content = request.getParameter("content");
		String mode = request.getParameter("mode");
		String dep = request.getParameter("dep");
		String sql = "insert into document(admin,title,content,mode,dep) values('"+adminlogin.get(0)+"','"+title+"','"+content+"','"+mode+"','"+dep+"')";
		int responseText = ib.insertANDupdateANDdel(sql);
		if(responseText == -1 ){
			request.setAttribute("message","'����ʧ�ܣ�'");
		}else{
			request.setAttribute("message","'�����ɹ���'");
			String examine[] = request.getParameterValues("examine");
			sql = "select max(id) as id from document";
			String args[] = {"id"};
			SelectBean sb = new SelectBean();
			ArrayList al = sb.selectRow(args, sql);
			for(int i = 0;i < examine.length;i++){
				sql = "insert into examine(document,dep) values("+al.get(0)+","+examine[i]+")";
				ib.insertANDupdateANDdel(sql);
			}
		}
		
		request.getRequestDispatcher("/admin/document.jsp").forward(request, response);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
