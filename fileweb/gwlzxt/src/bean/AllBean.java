package bean;

import java.util.ArrayList;

public class AllBean {
	SelectBean sb = new SelectBean();
	ArrayList al = null;
	
	public ArrayList getAllAdmin(String id){
		String sql = "select * from admin where id!=1 order by id desc";
		String args[] = {"id","name","pwd","type","status"};
		al = sb.select(sql, args);
		return al;
	}
	
	public ArrayList getDep(){
		String sql = "select * from dep order by id desc";
		String args[] = {"id","name"};
		al = sb.select(sql, args);
		return al;
	}
	
	public ArrayList getDep(String id){
		String sql = "select * from dep where id="+id;
		String args[] = {"id","name"};
		al = sb.selectRow(args, sql);
		return al;
	}
	
	public ArrayList getDocument(){
		String sql = "select * from document order by id desc";
		String args[] = {"id","admin","title","content","mode","dep","dates","lingdao","taohong"};
		al = sb.select(sql, args);
		return al;
	}
	
	public ArrayList getDocument(String id){
		String sql = "select * from document where id="+id;
		String args[] = {"id","admin","title","content","mode","dep","dates","lingdao","taohong"};
		al = sb.selectRow(args, sql);
		return al;
	}
	
	public ArrayList getDocumentUser(String admin){
		String sql = "select * from document where admin='"+admin+"' order by id desc";
		String args[] = {"id","admin","title","content","mode","dep","dates","lingdao","taohong"};
		al = sb.select(sql, args);
		return al;
	}
	
	public ArrayList getDocumentDep(String dep){
		String sql = "select * from document where dep='"+dep+"' order by id desc";
		String args[] = {"id","admin","title","content","mode","dep","dates","lingdao","taohong"};
		al = sb.select(sql, args);
		return al;
	}
	
	public ArrayList getExamine(){
		String sql = "select * from examine order by id desc";
		String args[] = {"id","document","dep","state"};
		al = sb.select(sql, args);
		return al;
	}
	
	public ArrayList getExamine(String id){
		String sql = "select * from examine where id="+id;
		String args[] = {"id","document","dep","state"};
		al = sb.selectRow(args, sql);
		return al;
	}
	
	public ArrayList getExamineDocument(String document){
		String sql = "select * from examine where document="+document+" order by id desc";
		String args[] = {"id","document","dep","state"};
		al = sb.select(sql, args);
		return al;
	}
	
	public ArrayList getExamineDep(String dep){
		String sql = "select * from examine where dep="+dep+" order by id desc";
		String args[] = {"id","document","dep","state"};
		al = sb.select(sql, args);
		return al;
	}
}
