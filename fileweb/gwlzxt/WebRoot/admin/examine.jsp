<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<jsp:useBean id="array" scope="page" class="bean.AllBean"/>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
ArrayList adminlogin = (ArrayList)session.getAttribute("adminlogin");
boolean closed = false;
ArrayList examine = null;
if(adminlogin != null && adminlogin.size() != 0){
	closed = true;
	examine = array.getExamineDep(adminlogin.get(4).toString());
}
String message = (String)request.getAttribute("message");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="<%=path %>/css/base.css" />
<link rel="stylesheet" type="text/css" href="<%=path %>/css/admin-css.css" />
<link rel="stylesheet" type="text/css" href="<%=path %>/css/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=path %>/css/themes/icon.css">
<script type="text/javascript" src="<%=path %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=path %>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path %>/js/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="<%=path %>/js/admin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('tbody tr:even').css({'background':'#ffffff'});
	$('tbody tr:odd').css({'background':'#eeeeff'});
	var message = <%=message %>;
	if(message != null && message != ""){
		alert(message);
	}
	login('<%=path %>',<%=closed %>);
	$("#click_logout").click(function(){
		logout('<%=path %>');
	});
	$("#editpass").click(function(){
		editpass('<%=path %>');
	});
})
</script>
</head>
<body class="easyui-layout" style="overflow-y: hidden"  scroll="no" resizable="false">
<div id="north" region="north" split="false" border="false">
  <jsp:include page="/admin/head.jsp"></jsp:include>
</div>
<div region="west" split="false" border="false" title="导航菜单" style="width:180px;" id="west">
  <div id="left-menus" border="false" fit="true">
    <jsp:include page="/admin/left.jsp"></jsp:include>
  </div>
</div>
<div id="mainPanle" region="center" border="true" style="background:#f7f7f7; padding:5px;">
  <table width="100%">
    <thead>
      <tr>
        <td colspan="5" align="center" style="padding:5px;">公文审批</td>
      </tr>
      <tr class="thead">
        <td align="center">序号</td>
        <td align="center">公文标题</td>
        <td align="center">审批类型</td>
        <td align="center">提交日期</td>
        <td align="center">操作</td>
      </tr>
    </thead>
    <tbody class="tbody">
    <%
    if(examine != null && examine.size() != 0){
	    for(int i = 0;i < examine.size();i++){
	    	ArrayList alRow = (ArrayList)examine.get(i);
	    	ArrayList document = array.getDocument(alRow.get(1).toString());
	    	String str = "";
	    	if(alRow.get(3).equals("1")){
	    		str = "已通过";
	    	}else if(alRow.get(3).equals("2")){
	    		str = "未通过";
	    	}
    %>
      <tr>
        <td align="center"><%=i+1 %></td>
        <%
        if(!document.get(8).equals("0")){
        %>
        <td align="center"><a href="<%=path %>/admin/document_show.jsp?id=<%=document.get(0) %>"><%=document.get(2) %></a></td>
        <%}else{ %>
        <td align="center"><a style="color:red" href="<%=path %>/admin/document_show.jsp?id=<%=document.get(0) %>"><%=document.get(2) %></a></td>
        <%} %>
        <td align="center"><%=document.get(4) %></td>
        <td align="center"><%=document.get(6).toString().substring(0,10) %></td>
        <td align="center"><%=alRow.get(3).equals("0")?"<a href='"+path+"/ExamineServlet?id="+alRow.get(0)+"&state=1'>已通过</a>&nbsp;&nbsp;<a href='"+path+"/ExamineServlet?id="+alRow.get(0)+"&state=2'>未通过</a>":str %></td>
      </tr>
      <%}} %>
    </tbody>
  </table>
</div>
</body>
</html>
