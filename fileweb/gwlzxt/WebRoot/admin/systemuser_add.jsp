<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<jsp:useBean id="array" scope="page" class="bean.AllBean"/>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
ArrayList adminlogin = (ArrayList)session.getAttribute("adminlogin");
boolean closed = false;
ArrayList admin = null;
if(adminlogin != null && adminlogin.size() != 0){
	closed = true;
	admin = array.getAllAdmin(adminlogin.get(0).toString());
}
String message = (String)request.getAttribute("message");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="<%=path %>/css/base.css" />
<link rel="stylesheet" type="text/css" href="<%=path %>/css/admin-css.css" />
<link rel="stylesheet" type="text/css" href="<%=path %>/css/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=path %>/css/themes/icon.css">
<script type="text/javascript" src="<%=path %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=path %>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path %>/js/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="<%=path %>/js/admin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('tbody tr:even').css({'background':'#ffffff'});
	$('tbody tr:odd').css({'background':'#eeeeff'});
	var message = <%=message %>;
	if(message != null && message != ""){
		alert(message);
	}
	login('<%=path %>',<%=closed %>);
	$("#click_logout").click(function(){
		logout('<%=path %>');
	});
	$("#editpass").click(function(){
		editpass('<%=path %>');
	});
	$("#admin_add").click(function(){
		adminAdd('<%=path %>');
	});
	$('#cc').combogrid('disable');
	$('input[name=type]').click(function(){
		var lives = $(this).attr('lives');
		$('#cc').combogrid(lives);
	})
})
function check(){
	if($('#form_systemuser_add').form('validate') == false){
		return false;
	}
	if($('#cc').combobox('getValue') == "0" && $(':radio[name=type][checked]').val() == "3"){
		$.messager.alert('系统消息','请选择学院部门！','info');
		return false;
	}
}
</script>
</head>
<body class="easyui-layout" style="overflow-y: hidden"  scroll="no" resizable="false">
<div id="north" region="north" split="false" border="false">
  <jsp:include page="/admin/head.jsp"></jsp:include>
</div>
<div region="west" split="false" border="false" title="导航菜单" style="width:180px;" id="west">
  <div id="left-menus" border="false" fit="true">
    <jsp:include page="/admin/left.jsp"></jsp:include>
  </div>
</div>
<div id="mainPanle" region="center" border="true" style="background:#f7f7f7; padding:5px;"><br/><br/><br/><br/><br/>
  <form id="form_systemuser_add" method="post" action="<%=path %>/AdminAddServlet" onSubmit="return check()">
    <table border="1" align="center" cellpadding="5">
      <tr>
        <td colspan="2" align="center">系统用户添加</td>
      </tr>
      <tr>
        <td align="right">用户名：</td>
        <td><input class="easyui-validatebox" type="text" name="name" required="true" validType="pattern" customize="Customize" /></td>
      </tr>
      <tr>
        <td align="right">密码：</td>
        <td><input class="easyui-validatebox" type="password" name="pwd" required="true" validType="pattern" id="systemusers_pwd" customize="Customize" /></td>
      </tr>
      <tr>
        <td align="right">重复密码：</td>
        <td><input class="easyui-validatebox" type="password" name="repeatpwd" required="true" validType="repeat'#systemusers_pwd'" customize="Customize" /></td>
      </tr>
      <tr>
        <td align="right">发布类型：</td>
        <td><input type="radio" name="type" value="1" lives="disable" checked>
          <span>管理员</span>&nbsp;
          <input type="radio" name="type" value="2" lives="disable">
          <span>拟稿人</span>&nbsp;
          <input type="radio" name="type" value="3" lives="enable">
          <span>学院部门</span></td>
      </tr>
      <tr>
        <td align="right">学院部门：</td>
        <td><select id="cc" class="easyui-combobox" name="status" editable="false" required="true" style="width:200px;">
        	<option value="0">-- 请选择学院部门  --</option>
            <%
            	ArrayList dep = array.getDep();
            	for(int i = 0;i < dep.size();i++){
            		ArrayList alRow = (ArrayList)dep.get(i);
            	%>
            <option value="<%=alRow.get(0) %>"><%=alRow.get(1) %></option>
            <%} %>
          </select></td>
      </tr>
      <tr>
        <td align="right">&nbsp;</td>
        <td><input type="submit" value="提交"></td>
      </tr>
    </table>
  </form>
</div>
</body>
</html>
