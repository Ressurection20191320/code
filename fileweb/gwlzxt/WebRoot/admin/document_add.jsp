<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<jsp:useBean id="array" scope="page" class="bean.AllBean"/>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
ArrayList adminlogin = (ArrayList)session.getAttribute("adminlogin");
boolean closed = false;
if(adminlogin != null && adminlogin.size() != 0){
	closed = true;
}
String message = (String)request.getAttribute("message");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="<%=path %>/css/base.css" />
<link rel="stylesheet" type="text/css" href="<%=path %>/css/admin-css.css" />
<link rel="stylesheet" type="text/css" href="<%=path %>/css/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=path %>/css/themes/icon.css">
<script type="text/javascript" src="<%=path %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=path %>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path %>/js/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="<%=path %>/js/admin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('tbody tr:even').css({'background':'#ffffff'});
	$('tbody tr:odd').css({'background':'#eeeeff'});
	var message = <%=message %>;
	if(message != null && message != ""){
		alert(message);
	}
	login('<%=path %>',<%=closed %>);
	$("#click_logout").click(function(){
		logout('<%=path %>');
	});
	$("#editpass").click(function(){
		editpass('<%=path %>');
	});
	$("#admin_add").click(function(){
		adminAdd('<%=path %>');
	});
})
</script>
</head>
<body class="easyui-layout" style="overflow-y: hidden"  scroll="no" resizable="false">
<div id="north" region="north" split="false" border="false">
  <jsp:include page="/admin/head.jsp"></jsp:include>
</div>
<div region="west" split="false" border="false" title="导航菜单" style="width:180px;" id="west">
  <div id="left-menus" border="false" fit="true">
    <jsp:include page="/admin/left.jsp"></jsp:include>
  </div>
</div>
<div id="mainPanle" region="center" border="true" style="background:#f7f7f7; padding:5px;"><br/><br/><br/><br/><br/>
  <form id="form_systemuser_add" method="post" action="<%=path %>/DocumentServlet" onSubmit="return $(this).form('validate')">
    <table border="1" align="center" cellpadding="5">
      <tr>
        <td colspan="2" align="center">添加新审批公文</td>
      </tr>
      <tr>
        <td align="right">公文标题：</td>
        <td><input class="easyui-validatebox" type="text" name="title" required="true" customize="Customize" style="width:350px;" /></td>
      </tr>
      <tr>
        <td align="right">公文内容：</td>
        <td><textarea name="content" cols="80" rows="20" class="easyui-validatebox" required="true"></textarea></td>
      </tr>
      <tr>
        <td align="right">审批类型：</td>
        <td><input type="radio" name="mode" value="普通审批" lives="disable" checked>
          <span>普通审批</span>&nbsp;
          <input type="radio" name="mode" value="会审审批" lives="disable">
          <span>会审审批</span></td>
      </tr>
      <tr>
        <td align="right">审批部门：</td>
        <td><select id="cc" class="easyui-combobox" name="examine" editable="false" required="true" multiple="true" style="width:200px;">
            <%
            	ArrayList dep = array.getDep();
            	for(int i = 0;i < dep.size();i++){
            		ArrayList alRow = (ArrayList)dep.get(i);
            	%>
            <option value="<%=alRow.get(0) %>"><%=alRow.get(1) %></option>
            <%} %>
        </select></td>
      </tr>
      <tr>
        <td align="right">下发部门：</td>
        <td><select id="dd" class="easyui-combobox" name="dep" editable="false" required="true" style="width:200px;">
            <%
            	for(int i = 0;i < dep.size();i++){
            		ArrayList alRow = (ArrayList)dep.get(i);
            	%>
            <option value="<%=alRow.get(0) %>"><%=alRow.get(1) %></option>
            <%} %>
          </select></td>
      </tr>
      <tr>
        <td align="right">&nbsp;</td>
        <td><input type="submit" value="提交"></td>
      </tr>
    </table>
  </form>
</div>
</body>
</html>
