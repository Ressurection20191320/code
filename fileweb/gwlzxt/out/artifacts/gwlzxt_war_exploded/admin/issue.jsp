<%@ page language="java" import="java.util.*" pageEncoding="gb2312"%>
<jsp:useBean id="array" scope="page" class="bean.AllBean"/>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
ArrayList adminlogin = (ArrayList)session.getAttribute("adminlogin");
boolean closed = false;
ArrayList document = null;
if(adminlogin != null && adminlogin.size() != 0){
	closed = true;
	document = array.getDocumentDep(adminlogin.get(4).toString());
}
String message = (String)request.getAttribute("message");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="<%=path %>/css/base.css" />
<link rel="stylesheet" type="text/css" href="<%=path %>/css/admin-css.css" />
<link rel="stylesheet" type="text/css" href="<%=path %>/css/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=path %>/css/themes/icon.css">
<script type="text/javascript" src="<%=path %>/js/jquery.js"></script>
<script type="text/javascript" src="<%=path %>/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%=path %>/js/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="<%=path %>/js/admin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('tbody tr:even').css({'background':'#ffffff'});
	$('tbody tr:odd').css({'background':'#eeeeff'});
	var message = <%=message %>;
	if(message != null && message != ""){
		alert(message);
	}
	login('<%=path %>',<%=closed %>);
	$("#click_logout").click(function(){
		logout('<%=path %>');
	});
	$("#editpass").click(function(){
		editpass('<%=path %>');
	});
})
</script>
</head>
<body class="easyui-layout" style="overflow-y: hidden"  scroll="no" resizable="false">
<div id="north" region="north" split="false" border="false">
  <jsp:include page="/admin/head.jsp"></jsp:include>
</div>
<div region="west" split="false" border="false" title="导航菜单" style="width:180px;" id="west">
  <div id="left-menus" border="false" fit="true">
    <jsp:include page="/admin/left.jsp"></jsp:include>
  </div>
</div>
<div id="mainPanle" region="center" border="true" style="background:#f7f7f7; padding:5px;">
  <table width="100%">
    <thead>
      <tr>
        <td colspan="4" align="center" style="padding:5px;">可下发公文</td>
      </tr>
      <tr class="thead">
        <td align="center">序号</td>
        <td align="center">公文标题</td>
        <td align="center">审批类型</td>
        <td align="center">提交日期</td>
      </tr>
    </thead>
    <tbody class="tbody">
    <%
    if(document != null && document.size() != 0){
	    for(int i = 0;i < document.size();i++){
	    	ArrayList alRow = (ArrayList)document.get(i);
	    	if(!alRow.get(7).equals("1")){
	    		continue;
	    	}
	    	ArrayList examine = array.getExamineDocument(alRow.get(0).toString());
	    	int flag = 0;
	    	for(int n = 0;n < examine.size();n++){
	    		ArrayList alRow1 = (ArrayList)examine.get(n);
	    		if(!alRow1.get(3).equals("1")){
	    			flag = 1;
	    			break;
	    		}
	    	}
	    	if(flag == 1){
	    		continue;
	    	}
    %>
      <tr>
        <td align="center"><%=i+1 %></td>
        <%
        if(!alRow.get(8).equals("0")){
        %>
        <td align="center"><a href="<%=path %>/admin/document_show.jsp?id=<%=alRow.get(0) %>"><%=alRow.get(2) %></a></td>
        <%}else{ %>
        <td align="center"><a style="color:red" href="<%=path %>/admin/document_show.jsp?id=<%=alRow.get(0) %>"><%=alRow.get(2) %></a></td>
        <%} %>
        <td align="center"><%=alRow.get(4) %></td>
        <td align="center"><%=alRow.get(6).toString().substring(0,10) %></td>
      </tr>
      <%}} %>
    </tbody>
  </table>
</div>
</body>
</html>
