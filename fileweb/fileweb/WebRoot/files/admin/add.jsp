<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 
功能介绍：添加

 -->
<%@ include file="/files/db/conn.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<link rel="stylesheet" rev="stylesheet" href="css/style.css" type="text/css" media="all" />
<style type="text/css">
<!--
.atten {font-size:12px;font-weight:normal;color:#F00;}
-->
</style>
<%
if(request.getParameter("flag")!=null&&"success".equals(request.getParameter("flag"))){
	response.getWriter().println("<script>alert('添加成功!');</script>");
}
%>
<script type="text/javascript">
		function checkValue(){
				if(document.form.username.value==""||document.form.username.value==null)
				{
					alert("不能为空！");
					document.form.username.focus();
					return false;
				}
				if(document.form.realname.value==""||document.form.realname.value==null)
				{
					alert("不能为空！");
					document.form.realname.focus();
					return false;
				}
				if(document.form.password.value==""||document.form.password.value==null)
				{
					alert("不能为空！");
					document.form.password.focus();
					return false;
				}
				if(document.form.zhiwei.value==""||document.form.zhiwei.value==null)
				{
					alert("不能为空！");
					document.form.zhiwei.focus();
					return false;
				}
				if(document.form.tel.value==""||document.form.tel.value==null)
				{
					alert("不能为空！");
					document.form.tel.focus();
					return false;
				}
				if(document.form.email.value==""||document.form.email.value==null)
				{
					alert("不能为空！");
					document.form.email.focus();
					return false;
				}
				return true;
		}
</script>
</head>

<body class="ContentBody">
  <form action="files/admin/addDao.jsp" method="post"  name="form"  onSubmit="return checkValue()">
<div class="MainDiv">
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
   <tr>
      <td height="10">&nbsp;</td>
  </tr>
  <tr>
      <td align="left" ><font color="blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单位管理员信息>>添加</font></td>
  </tr>
  <tr>
    <td class="CPanel">		
		<table border="0" cellpadding="0" cellspacing="0" style="width:100%">		
		<tr>
			<td width="100%">
				<fieldset style="height:100%;">
				<legend>单位管理员信息</legend>
					  <table border="0" cellpadding="2" cellspacing="1" style="width:100%">
					  <tr>
					    <td align="right" width="19%">登录账户:</td>
					    <td width="35%"><span class="red">
				        <input name='username' type="text" class="text" style="width:354px" value=""/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">姓名:</td>
					    <td width="35%"><span class="red">
				        <input name='realname' type="text" class="text" style="width:354px" value=""/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">登录密码:</td>
					    <td width="35%"><span class="red">
				        <input name='password' type="text" class="text" style="width:354px" value=""/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">性别:</td>
					    <td width="35%"><span class="red">
					    <select name="sex">
				        <option value="男" >男</option>
				        <option value="女" >女</option>
				        </select>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">单位名称:</td>
					    <td width="35%"><span class="red">
					    <select name='departmentid' >
						<%
						ResultSet rs = null;
							String sql = "select * from t_department";
							rs = stmt.executeQuery(sql);
						while(rs.next()) {
						%>
						<option value="<%=rs.getString("id") %>" ><%=rs.getString("departmentname") %> </option>
					     	<%
						} 
						%>   
						</select>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">组织名称:</td>
					    <td width="35%"><span class="red">
					    <select name='zuzhiid' >
						<%
						stmt1 = conn.createStatement();
						ResultSet rs1 = null;
							String sql1 = "select * from t_zuzhi";
							rs1 = stmt1.executeQuery(sql1);
						while(rs1.next()) {
						%>
						<option value="<%=rs1.getString("id") %>" ><%=rs1.getString("zuzhiname") %> </option>
					     	<%
						} 
						%>   
						</select>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">职位:</td>
					    <td width="35%"><span class="red">
				        <input name='zhiwei' type="text" class="text" style="width:354px" value=""/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">电话:</td>
					    <td width="35%"><span class="red">
				        <input name='tel' type="text" class="text" style="width:354px" value=""/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">邮件:</td>
					    <td width="35%"><span class="red">
				        <input name='email' type="text" class="text" style="width:354px" value=""/>
				        *</span>
				        </td>
					  </tr>
					  
					  </table>
			  <br />
				</fieldset>			</td>
			
		</TR>
		
		
		
		</TABLE>
	
	
	 </td>
  </tr>
		
		<TR>
			<TD colspan="2" align="center" height="50px">
			<input type="submit" name="submitbut" value="保存" class="button" />　
			
			<input type="reset" name="reset" value="重置" class="button"  /></TD>
		</TR>
		</TABLE>
</div>
</form>
</body>
</html>
