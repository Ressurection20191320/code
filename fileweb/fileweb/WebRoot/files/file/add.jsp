<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 
功能介绍：模版添加

 -->
<%@ include file="/files/db/conn.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<link rel="stylesheet" rev="stylesheet" href="css/style.css" type="text/css" media="all" />
<style type="text/css">
<!--
.atten {font-size:12px;font-weight:normal;color:#F00;}
-->
</style>
<%
if(request.getParameter("flag")!=null&&"success".equals(request.getParameter("flag"))){
	response.getWriter().println("<script>alert('添加成功!');</script>");
}
%>
<script type="text/javascript">
		function checkValue(){
				if(document.form.title.value==""||document.form.title.value==null)
				{
					alert("不能为空！");
					document.form.title.focus();
					return false;
				}
				if(document.form2.file.value.replace(/[ ]/g,"")==""){
					 alert("请选择文件!");
					 return false;
				}
					document.form.submit();
		}
		function Upload(){
			if(document.form2.file.value.replace(/[ ]/g,"")==""){
			 alert("请选择文件!");
			}else{
				document.form2.action="/fileweb/files/file/upload.jsp";
				document.form2.target="_blank";
				document.form2.submit();
			}
	   }
</script>
</head>

<body class="ContentBody">
  <form action="files/file/addDao.jsp" method="post"  name="form" >
<div class="MainDiv">
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
   <tr>
      <td height="10">&nbsp;</td>
  </tr>
  <tr>
      <td align="left" ><font color="blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;发送文件>>添加</font></td>
  </tr>
  <tr>
    <td class="CPanel">		
		<table border="0" cellpadding="0" cellspacing="0" style="width:100%">		
		<tr>
			<td width="100%">
				<fieldset style="height:100%;">
				<legend>文件</legend>
					  <table border="0" cellpadding="2" cellspacing="1" style="width:100%">
					  <tr>
					    <td align="right" width="19%">标题:</td>
					    <td width="35%"><span class="red">
				        <input name='title' type="text" class="text" style="width:354px" value=""/>
				        <input name='fileName' id="fileName" type="hidden" />
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">备注:</td>
					    <td width="35%"><span class="red">
					    <textarea name='remark' rows="4" cols="60"></textarea>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">收件人:</td>
					    <td width="35%"><span class="red">
					    <select name='touser' >
						<%
						ResultSet rs = null;
							String sql = "select * from t_user where role =2";
							rs = stmt.executeQuery(sql);
						while(rs.next()) {
						%>
						<option value="<%=rs.getString("id") %>" ><%=rs.getString("realname") %> </option>
					     	<%
						} 
						%>   
						</select>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">文件类型:</td>
					    <td width="35%"><span class="red">
					    <select name='typeid' >
						<%
						stmt1 = conn.createStatement();
						ResultSet rs1 = null;
							String sql1 = "select * from t_filetype";
							rs1 = stmt1.executeQuery(sql1);
						while(rs1.next()) {
						%>
						<option value="<%=rs1.getString("id") %>" ><%=rs1.getString("typename") %> </option>
					     	<%
						} 
						%>   
						</select>
				        *</span>
				        </td>
					  </tr>
					  </form>
					 <form method="post" name="form2" id="form2" enctype="multipart/form-data" >
					  <tr>
					    <td align="right" width="19%">上传附件:</td>
					    <td width="35%"><span class="red">
				        
				        <input name="file" type="file" class="Style_upload" id="file">
				        <input type="button" name="Submit2" value="上传" onClick="Upload()">
				        </span>
				        </td>
					  </tr>
					  </form>
					  </table>
			  <br />
				</fieldset>			</td>
			
		</TR>
		
		
		
		</TABLE>
	
	
	 </td>
  </tr>
		
		<TR>
			<TD colspan="2" align="center" height="50px">
			<input type="button" name="submitbut" value="保存" class="button"  onClick="return checkValue()" />　
			
			<input type="reset" name="reset" value="重置" class="button"  /></TD>
		</TR>
		</TABLE>
</div>

</body>
</html>
