<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!-- 
功能介绍：修改

 -->
<%@ include file="/files/db/conn.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<link rel="stylesheet" rev="stylesheet" href="css/style.css" type="text/css" media="all" />
<style type="text/css">
<!--
.atten {font-size:12px;font-weight:normal;color:#F00;}
-->
</style>

<script type="text/javascript">
		function checkValue(){
				if(document.form.username.value==""||document.form.username.value==null)
				{
					alert("不能为空！");
					document.form.username.focus();
					return false;
				}
				if(document.form.realname.value==""||document.form.realname.value==null)
				{
					alert("不能为空！");
					document.form.realname.focus();
					return false;
				}
				if(document.form.password.value==""||document.form.password.value==null)
				{
					alert("不能为空！");
					document.form.password.focus();
					return false;
				}
				if(document.form.zhiwei.value==""||document.form.zhiwei.value==null)
				{
					alert("不能为空！");
					document.form.zhiwei.focus();
					return false;
				}
				if(document.form.tel.value==""||document.form.tel.value==null)
				{
					alert("不能为空！");
					document.form.tel.focus();
					return false;
				}
				if(document.form.email.value==""||document.form.email.value==null)
				{
					alert("不能为空！");
					document.form.email.focus();
					return false;
				}
				return true;
		}
</script>
</head>
<%
ResultSet rs = null;
try {
	
	String sql = "select * from t_user where  id = '"+request.getParameter("ID")+"' ";
	rs = stmt.executeQuery(sql);
	if(rs.next()){
%>
<body class="ContentBody">
  <form action="files/user/updateDao.jsp" method="post"  name="form" onSubmit="return checkValue()" >
<div class="MainDiv">
<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
   <tr>
      <td height="10">&nbsp;</td>
  </tr>
  <tr>
      <td align="left" ><font color="blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单位员工信息>>修改</font></td>
  </tr>
  <tr>
    <td class="CPanel">		
		<table border="0" cellpadding="0" cellspacing="0" style="width:100%">	
		<input name="id" value="<%=rs.getString("id")%>"  type="hidden" />
		<tr>
			<td width="100%">
				<fieldset style="height:100%;">
				<legend>单位员工信息</legend>
					  <table border="0" cellpadding="2" cellspacing="1" style="width:100%">
					  <tr>
					    <td align="right" width="19%">登录帐号:</td>
					    <td width="35%"><span class="red">
				        <input name='username' readonly="readonly" type="text" class="text" style="width:354px" value="<%=rs.getString("username")%>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">姓名:</td>
					    <td width="35%"><span class="red">
				        <input name='realname' type="text" class="text" style="width:354px" value="<%=rs.getString("realname")%>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">登录密码:</td>
					    <td width="35%"><span class="red">
				        <input name='password' type="text" class="text" style="width:354px" value="<%=rs.getString("password")%>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">性别:</td>
					    <td width="35%"><span class="red">
					    <select name="sex">
				        <option value="男" <%if(rs.getString("sex").equals("男")){ %> selected="selected" <%} %> >男</option>
				        <option value="女" <%if(rs.getString("sex").equals("女")){ %> selected="selected" <%} %> >女</option>
				        </select>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">组织名称:</td>
					    <td width="35%"><span class="red">
					    <select name='zuzhiid' >
						<%
						stmt1 = conn.createStatement();
						ResultSet rs1 = null;
							String sql1 = "select * from t_zuzhi";
							rs1 = stmt1.executeQuery(sql1);
						while(rs1.next()) {
						%>
						<option value="<%=rs1.getString("id") %>" <%if(rs1.getString("id").equals(rs.getString("zuzhiid"))){ %> selected="selected" <%} %>  ><%=rs1.getString("zuzhiname") %> </option>
					     	<%
						} 
						%>   
						</select>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">职位:</td>
					    <td width="35%"><span class="red">
				        <input name='zhiwei' type="text" class="text" style="width:354px" value="<%=rs.getString("zhiwei")%>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">电话:</td>
					    <td width="35%"><span class="red">
				        <input name='tel' type="text" class="text" style="width:354px" value="<%=rs.getString("tel")%>"/>
				        *</span>
				        </td>
					  </tr>
					  <tr>
					    <td align="right" width="19%">邮件:</td>
					    <td width="35%"><span class="red">
				        <input name='email' type="text" class="text" style="width:354px" value="<%=rs.getString("email")%>"/>
				        *</span>
				        </td>
					  </tr>
					  </table>
			  <br />
				</fieldset>			</td>
			
		</TR>
		
		
		
		</TABLE>
	
	
	 </td>
  </tr>
		
		<TR>
			<TD colspan="2" align="center" height="50px">
			<input type="submit" name="submitbut" value="保存" class="button" />　
			
			<input type="reset" name="reset" value="重置" class="button"  /></TD>
		</TR>
		</TABLE>
	
<%
	}
} catch (Exception e) {
	out.print(e.getMessage());
} finally {
	if (rs != null) {
		try {
	rs.close();
		} catch (Exception e) {
	e.printStackTrace();
		}
	}
	if (stmt != null) {
		try {
			stmt.close();
		} catch (Exception e) {
	e.printStackTrace();
		}
	}
	if (conn != null) {
		try {
	conn.close();
		} catch (Exception e) {
	e.printStackTrace();
		}
	}
}
%>
</div>
</form>
</body>
</html>
